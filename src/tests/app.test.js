import React from 'react';
import { shallow, mount, render } from 'enzyme';
import App from '../components/App';

describe('render page', () => {
    it("Deve renderizar a página", () => {
        const wrapper = render(<App />);
        expect(wrapper.length).toEqual(1);
    });
});