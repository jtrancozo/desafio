import { required as fieldRequired, number, positive, lessThanPriceFrom, minLength, minLength8 } from '../components/formValidations';

describe('Form Validations Tests', () => {
    it('Required', () => {
        const testValue = 'test';
        expect(fieldRequired(testValue)).toEqual(undefined); // redux-form return undefined when its ok
        expect(fieldRequired()).toBe('Campo necessário');
    });

    it('Number', () => {
        expect(number('4')).toBe('Somente números');
        expect(number(4)).toBe(undefined);
    });

    it('Positive', () => {
        const msg = 'O número precisa ser maior que 0';
        const floatValue = 1.1;
        expect(positive(-1)).toBe(msg);
        expect(positive('-1')).toBe(msg);
        expect(positive(1.1)).toBe(undefined);
        expect(positive(1)).toBe(undefined);
    });

    it('Less than', () => {
        const priceFrom = {price_from: 10};
        expect(lessThanPriceFrom(5, priceFrom)).toBe(undefined);
        expect(lessThanPriceFrom(15, priceFrom)).toBe('O preço promocional precisa ser menor que o preço normal.');
        expect(lessThanPriceFrom('numero', priceFrom)).toBe('O preço promocional precisa ser menor que o preço normal.');
    });

    it('Min Length', () => {
        expect(minLength(6)('123456')).toBe(undefined);
    });

    it('Min Length 8', () => {
        expect(minLength8('12345678')).toBe(undefined);
    });
});



