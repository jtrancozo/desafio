import React, { Component } from 'react';
import { fetchProduct, fetchProductSuccess, fetchProductFailure,  } from '../actions/products';
import { updateProduct, updateProductSuccess, updateProductFailure,  } from '../actions/products';
import { deleteProduct, deleteProductSuccess, deleteProductFailure,  } from '../actions/products';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import ProductForm from './productForm';

class Edit extends Component {

    constructor(props){
        super(props);

        this.state =  {
            sending: '',
            success: ''
        };

        this.submit = this.submit.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    }

    submit(values) {
        this.setState({sending: ' sending'});

        return this.props.updateProduct(values, (response) => {
            if (!response.error) {
                this.setState({success: ' success'});
            } else {
                this.setState({success: ' fail'});
            }

            setTimeout(()=> {
                this.setState({sending: '', success: ''});
                this.props.history.push('/');
            },1000);
        });
    } 

    handleDelete() {
        const {sku} = this.props.product;
        this.setState({sending: ' sending'});

        return this.props.deleteProduct(sku, (response) => {
            if (!response.error) {
                this.setState({success: ' success'});
            } else {
                this.setState({success: ' fail'});
            }

            setTimeout(()=> {
                this.setState({sending: '', success: ''});
                this.props.history.push('/');
            },1000);
        });
    }

    componentDidMount() {
        let match = this.props.match;
        let sku = match.params.sku;
        this.props.fetchProduct(sku);        
    }

    render() {
        const product = this.props.product;

        return (
            <section className="section">
                <div className={'form-bar ' + this.state.sending + this.state.success }></div>
                <header className="section-header">
                    <h1 className="section-title">Editar Produto</h1>
                    <div>
                        <button className="btn btn-danger" onClick={this.handleDelete}>Deletar</button>
                    </div>
                </header>

                { product ? <ProductForm onSubmit={this.submit} product={product} edit={true}/> : '' }

            </section>
        );
    }
}

Edit.propTypes = {
    fetchProduct: PropTypes.func,
    updateProduct: PropTypes.func,
    deleteProduct: PropTypes.func,
    match: PropTypes.object,
    product: PropTypes.object,
    history: PropTypes.object
};

const mapStateToProps = (state) => {
    return {
        product: state.products.activeProduct.product 
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchProduct: (sku) => {
            dispatch(fetchProduct(sku)).then((response)=> {
                !response.error ? dispatch(fetchProductSuccess(response.payload.data)) : dispatch(fetchProductFailure(response.payload.data));
            });
        },

        updateProduct: (values, callback) => {
            dispatch(updateProduct(values)).then((response)=> {
                if(!response.error) { 
                    dispatch(updateProductSuccess(response.payload.data)); 
                } else { 
                    dispatch(updateProductFailure(response.payload.data));
                }

                callback(response);
            });
        },

        deleteProduct: (sku, callback) => {
            dispatch(deleteProduct(sku)).then((response)=> {
                if(!response.error) { 
                    dispatch(deleteProductSuccess(response.payload.data)); 
                } else { 
                    dispatch(deleteProductFailure(response.payload.data));
                }

                callback(response);
            });
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Edit);