import React from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
// validations
import { required, number, positive, lessThanPriceFrom, minLength8 } from './formValidations';


// renderField
const renderField = ({ input, label, type, textarea, disabled, meta: { error, submitFailed } }) => {
    const textareaType = <textarea {...input} placeholder={label} id={input.id ? input.id : ''} disabled={disabled} className={`textarea ${submitFailed && error ? 'error' : ''}`}/>;
    const inputType = <input {...input} placeholder={label} id={input.id ? input.id : ''} type={type} disabled={disabled} className={`input-${type} ${submitFailed && error ? 'error' : ''}`}/>;

    return (
        <div className="form-control">
            <label htmlFor={input.id ? input.id : input.name}>{label}</label>
            <div classNme="input-control">
                {textarea ? textareaType : inputType}
                {submitFailed && (error && <span className="error-message">{error}</span>)}
            </div>
        </div>
    );
};

let ProductForm = props => {
    const { handleSubmit, edit } = props;

    return (
        <form className="form" onSubmit={handleSubmit}>

            <div className="form-row">
                <Field name="sku" id="sku" label="SKU" component={renderField} type="text" placeholder="SKU" className="input-text" disabled={edit} validate={[ required ]}/>
                <Field name="ean" id="ean" label="EAN" component={renderField} type="text" placeholder="EAN" className="input-text" disabled={edit} validate={[ required, number, minLength8, positive ]}/>
                <Field name="qty" label="Quantidade" component={renderField} type="number" placeholder="Quantidade" className="input-text" validate={[required, number, positive]}/>  
            </div>

            <div className="form-row">
                <Field name="price_from" component={renderField} type="text" label="Preço normal" placeholder="Preço normal" className="input-text" validate={[required, number, positive]}/>
                <Field name="price_to" component={renderField} type="text" label="Preço promocional" placeholder="Preço promocional" className="input-text" validate={[ number, positive, lessThanPriceFrom ]}/>
            </div>

            <div className="form-row">
                <Field name="description" component={renderField} textarea={true} label="Descrição" placeholder="Descrição do produto" className="textarea" validate={[ required ]}/>
            </div>

            <div className="button-wrapper">   
                {/* <button  className="btn btn-primary" type="submit" disabled={pristine || submitting}> */}
                <button  className="btn btn-primary" type="submit">
                    Enviar
                </button>
            </div> 
        </form>
    ); 
};

renderField.propTypes = {
    type: PropTypes.text,
    input: PropTypes.object,
    label: PropTypes.text,
    meta: PropTypes.object,
    textarea: PropTypes.bool,
    disabled: PropTypes.bool
};

ProductForm.propTypes = {
    handleSubmit: PropTypes.func,
    product: PropTypes.object,
    pristine: PropTypes.bool,
    //submitting: PropTypes.bool,
    edit: PropTypes.bool
};

ProductForm = reduxForm({
    form: 'productForm'
})(ProductForm);

ProductForm = connect(
    (state,props) => {
        return {
            initialValues: props.product // pull initial values from account reducer
        };
    }
)(ProductForm);

export default ProductForm;

