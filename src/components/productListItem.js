import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

class ProductListItem extends Component {
    render() {
        return (
            <div className="tr tr-product" key={this.props.product.sku}>
                <div className="td product-ean">{this.props.product.sku}</div>
                <div className="td product-description"><Link to={`/edit/${this.props.product.sku}`}>{this.props.product.description}</Link></div>
                <div className="td price-from">R$ {this.props.product.price_from}</div>
                <div className="td price-to">R$ {this.props.product.price_to}</div>
                <div className="td product-qty">{this.props.product.qty}</div>
                <div className="td product-ean">{this.props.product.ean}</div>
            </div>
        );
    }
}

ProductListItem.propTypes = {
    product: PropTypes.object
};

export default ProductListItem;