import React from 'react';
import {BrowserRouter, Switch, Route } from 'react-router-dom';
import Products from './products';
import Edit from './edit';
import AddNew from './add_new';

const Main = () => (
    <main>
        <BrowserRouter>
            <Switch>
                <Route exact path='/' component={Products}/>
                <Route path='/edit/:sku' component={Edit}/>
                <Route path='/add-new' component={AddNew}/>
            </Switch>
        </BrowserRouter>
    </main>
);

export default Main;