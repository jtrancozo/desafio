import React, { Component } from 'react';
import { fetchProducts, fetchProductsSuccess, fetchProductsFailure } from '../actions/products';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import ProductListItem from './productListItem';
import InfiniteScroll from 'react-infinite-scroll-component';

class Products extends Component {
    constructor(props) {
        super(props);

        this.state = {
            page: 1,
            per_page: 10,
            next_url: null,
            hasMore: false,
            products: [],
            nextProducts: []
        };

        this.loadMoreProducts = this.loadMoreProducts.bind(this);
        this.hasMore = this.hasMore.bind(this);
    }

    loadMoreProducts() {
        let next_url = this.state.next_url;

        this.props.fetchProducts(next_url, (response) => {
            if(!response.error) {
                this.setState({
                    products: this.state.products.concat(response.payload.data.products),
                    nextProducts: response.payload.data.products,
                    next_url: response.payload.data.next_url,
                });

                this.hasMore(response.payload.data.products.length);

                return this.state.products;
            }
        });
    }

    hasMore(qty) {
        this.setState({hasMore: qty >= this.state.per_page ? true : false });
    }

    concatProducts(oldList, newList){
        return newList !== oldList ? oldList.concat(newList) : oldList; 
    }

    componentDidMount() {
        this.props.fetchProducts('', (response) => {
            if(!response.error) {
                this.setState({
                    products: this.concatProducts(this.state.products, response.payload.data.products),
                    next_url: response.payload.data.next_url,
                });

                this.hasMore(response.payload.data.products.length);
            }
        });
    }

    mountProduct() {
        const products = this.state.products;

        return (
            <section className="section">
                <header className="section-header">
                    <h1 className="section-title">Produtos</h1>
                    <div>
                        <Link to="/add-new"><button className="btn btn-primary">Novo produto</button></Link>
                    </div>
                    
                </header>    

                <div className="infinite-loader">
                    {products 
                        ? 
                        <InfiniteScroll
                            dataLength={this.state.products.length}
                            next={this.loadMoreProducts}
                            hasMore={this.state.hasMore}
                            loader={<h4>Carregando...</h4>}
                            endMessage={
                                <p style={{textAlign: 'center'}}>
                                    <b>Não temos mais produtos!</b>
                                </p>
                            }
                        > 
                            <div className="table">
                                <div className="thead">
                                    <div className="tr">
                                        <div className="th" style={{'width': '10%'}}>SKU</div>
                                        <div className="th" style={{'width': '30%'}}>Descrição</div>
                                        <div className="th">Preço</div>
                                        <div className="th">Preço promocional</div>
                                        <div className="th">Quantidade</div>
                                        <div className="th">EAN</div>
                                    </div>
                                </div>

                                <div className="tbody">
                                    {products.map((product) => <ProductListItem product={product} key={product.updated_at} />)}
                                </div>
                            </div>    
                        </InfiniteScroll>
                        : 
                        null
                    }
                </div>    
                
            </section>
        );
    }

    render() {
        return (
            <div className='container'>                         
                {this.mountProduct()}    
            </div>
        );
    }
}

Products.propTypes = {
    fetchProducts: PropTypes.func,
    products: PropTypes.object
};

const mapStateToProps = (state) => {
    return {
        products: state.products
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchProducts: (nextUrl, callback) => {
            dispatch(fetchProducts(nextUrl)).then((response)=> {
                !response.error ? dispatch(fetchProductsSuccess(response.payload.data)) : dispatch(fetchProductsFailure(response.payload.data));

                callback(response);
            });
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Products);