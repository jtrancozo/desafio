import React from 'react';
import Main from './main';

const App = () => (
    <div className="container">
        <Main />
    </div>
);
  
export default App;  