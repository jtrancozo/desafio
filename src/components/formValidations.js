export const required = value => (value ? undefined : 'Campo necessário');
//export const number = value => (value && /[0-9]/.test(value) ? undefined : 'Somente números');
export const number = value => (value && typeof value === 'number' ? undefined : 'Somente números');
export const positive = value => (value && /[0-9]/.test(value) && value > 0 ? undefined : 'O número precisa ser maior que 0');
export const lessThanPriceFrom = (value, allValues) => ( value < allValues.price_from ? undefined : 'O preço promocional precisa ser menor que o preço normal.');
export const minLength = min => value => (value && value.length >= min ? undefined : `Mínimo de ${min} números`);
export const minLength8 = minLength(8);