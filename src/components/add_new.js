import React from 'react';
import ProductForm from './productForm';
import { createProduct, createProductSuccess, createProductFailure } from '../actions/products';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

class AddNew extends React.Component {

    constructor(props){
        super(props);

        this.state =  {
            sending: '',
            success: ''
        };

        this.submit = this.submit.bind(this);
    }

    submit(values) {
        this.setState({sending: ' sending'});

        return this.props.createProduct(values, (response) => {
            if (!response.error) {
                this.setState({success: ' success'});
            } else {
                this.setState({success: ' fail'});
            }

            setTimeout(()=> {
                this.setState({sending: '', success: ''});
                this.props.history.push('/');
            },1000);
        });
    }

    render() {
        return (
            <section className="section">
                <div className={'form-bar ' + this.state.sending + this.state.success }></div>
                <header className="section-header">
                    <h1 className="section-title">Adicionar Produto</h1>
                </header>

                <ProductForm onSubmit={this.submit} edit={false}/>
            </section>
        );
    }
}

AddNew.propTypes = {
    createProduct: PropTypes.func,
    history: PropTypes.object
};

const mapStateToProps = (state) => {
    return {
        product: state.products.activeProduct.product 
    };
};

const mapDispatchToProps = (dispatch) => {

    return {
        createProduct: (values, callback) => {
            dispatch(createProduct(values)).then((response)=> {
                if(!response.error) { 
                    dispatch(createProductSuccess(response.payload.data)); 
                } else { 
                    dispatch(createProductFailure(response.payload.data));
                }

                callback(response);
            });
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(AddNew);