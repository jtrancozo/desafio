import axios from 'axios';

//Product list
export const FETCH_PRODUCTS = 'FETCH_PRODUCTS';
export const FETCH_PRODUCTS_SUCCESS = 'FETCH_PRODUCTS_SUCCESS';
export const FETCH_PRODUCTS_FAILURE = 'FETCH_PRODUCTS_FAILURE';
export const FETCH_COMBINE_PRODUCTS = 'FETCH_COMBINE_PRODUCTS';
export const RESET_PRODUCTS = 'RESET_PRODUCTS';

//Create new product
export const CREATE_PRODUCT = 'CREATE_PRODUCT';
export const CREATE_PRODUCT_SUCCESS = 'CREATE_PRODUCT_SUCCESS';
export const CREATE_PRODUCT_FAILURE = 'CREATE_PRODUCT_FAILURE';
export const RESET_NEW_PRODUCT = 'RESET_NEW_PRODUCT';

//Update product
export const UPDATE_PRODUCT = 'UPDATE_PRODUCT';
export const UPDATE_PRODUCT_SUCCESS = 'UPDATE_PRODUCT_SUCCESS';
export const UPDATE_PRODUCT_FAILURE = 'UPDATE_PRODUCT_FAILURE';


//Fetch PRODUCT
export const FETCH_PRODUCT = 'FETCH_PRODUCT';
export const FETCH_PRODUCT_SUCCESS = 'FETCH_PRODUCT_SUCCESS';
export const FETCH_PRODUCT_FAILURE = 'FETCH_PRODUCT_FAILURE';
export const RESET_ACTIVE_PRODUCT = 'RESET_ACTIVE_PRODUCT';

//Delete PRODUCT
export const DELETE_PRODUCT = 'DELETE_PRODUCT';
export const DELETE_PRODUCT_SUCCESS = 'DELETE_PRODUCT_SUCCESS';
export const DELETE_PRODUCT_FAILURE = 'DELETE_PRODUCT_FAILURE';
export const RESET_DELETED_PRODUCT = 'RESET_DELETED_PRODUCT';



const ROOT_URL = 'https://skyhub-product-warehouse.herokuapp.com/api';
const headers = {
    'api-key': 'j01',
    'Content-Type': 'application/json',
    'Accept': 'application/json',
    'Cache-Control' : 'no-cache'
};

export function fetchProducts(nextUrl) {
    let url = `${ROOT_URL}/products`;

    if (nextUrl) {
        url = nextUrl;
    }
    
    const request = axios({
        method: 'get',
        url: url,
        headers: headers
    });

    return {
        type: FETCH_PRODUCTS,
        payload: request
    };
}

export function fetchProductsSuccess(products) {
    return {
        type: FETCH_PRODUCTS_SUCCESS,
        payload: products
    };
}

export function fetchCombineProducts(products) {
    return {
        type: FETCH_COMBINE_PRODUCTS,
        payload: products
    };
}

export function fetchProductsFailure(error) {
    return {
        type: FETCH_PRODUCTS_FAILURE,
        payload: error
    };
}

export function createProduct(props) {
    const request = axios({
        method: 'post',
        data: props,
        url: `${ROOT_URL}/products`,
        headers: headers
    });

    return {
        type: CREATE_PRODUCT,
        payload: request
    };
}

export function createProductSuccess(newProduct) {
    return {
        type: CREATE_PRODUCT_SUCCESS,
        payload: newProduct
    };
}

export function createProductFailure(error) {
    return {
        type: CREATE_PRODUCT_FAILURE,
        payload: error
    };
}


export function updateProduct(data) {
    const request = axios({
        method: 'PUT',
        data: data,
        url: `${ROOT_URL}/products/${data.sku}`,
        headers: headers
    });

    return {
        type: UPDATE_PRODUCT,
        payload: request
    };
}

export function updateProductSuccess(newProduct) {
    return {
        type: UPDATE_PRODUCT_SUCCESS,
        payload: newProduct
    };
}

export function updateProductFailure(error) {
    return {
        type: UPDATE_PRODUCT_FAILURE,
        payload: error
    };
}


export function fetchProduct(sku) {
    //const request = axios.get(`${ROOT_URL}/products/${sku}`);

    const request = axios({
        method: 'get',
        url: `${ROOT_URL}/products/${sku}`,
        headers: headers
    });

    return {
        type: FETCH_PRODUCT,
        payload: request
    };
}

export function fetchProductSuccess(activeProduct) {
    return {
        type: FETCH_PRODUCT_SUCCESS,
        payload: activeProduct
    };
}

export function fetchProductFailure(error) {
    return {
        type: FETCH_PRODUCT_FAILURE,
        payload: error
    };
}


export function resetActiveProduct() {
    return {
        type: RESET_ACTIVE_PRODUCT
    };
}


export function deleteProduct(sku) {
    const request = axios({
        method: 'DELETE',
        url: `${ROOT_URL}/products/${sku}`,
        headers: headers
    });
    return {
        type: DELETE_PRODUCT,
        payload: request
    };
}

export function deleteProductSuccess(deletedProduct) {
    return {
        type: DELETE_PRODUCT_SUCCESS,
        payload: deletedProduct
    };
}

export function deleteProductFailure(response) {
    return {
        type: DELETE_PRODUCT_FAILURE,
        payload: response
    };
}