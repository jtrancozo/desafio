import { combineReducers } from 'redux';
import ProductsReducer from './reducer_product';
//import ValidateUserFieldsReducer from './reducer_validateUserFields';
import { reducer as formReducer } from 'redux-form';

const rootReducer = combineReducers({
    //validateFields: ValidateUserFieldsReducer,
    products: ProductsReducer, //<-- Posts
    form: formReducer, // <-- redux-form
});

export default rootReducer;
