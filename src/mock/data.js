const data = [{
    'description': 'Diomedea irrorata',
    'sku': 70892171,
    'ean': 9635258764,
    'price_to': 562.83,
    'price_from': 902.04,
    'qty': 158
}, {
    'description': 'Macropus fuliginosus',
    'sku': 29865122,
    'ean': 2871626951,
    'price_to': 162.35,
    'price_from': 468.54,
    'qty': 164
},
{
    'description': 'Anas bahamensis',
    'sku': 21562398,
    'ean': 5750940285,
    'price_to': 758.44,
    'price_from': 1227.39,
    'qty': 223
},
{
    'description': 'Fratercula corniculata',
    'sku': 36709885,
    'ean': 8076572903,
    'price_to': 363.53,
    'price_from': 555.52,
    'qty': 52
},
{
    'description': 'Corvus albicollis',
    'sku': 20186566,
    'ean': 3893190712,
    'price_to': 563.83,
    'price_from': 1534.48,
    'qty': 234
},
{
    'description': 'Pteropus rufus',
    'sku': 84188661,
    'ean': 8196627475,
    'price_to': 131.2,
    'price_from': 1117.1,
    'qty': 416
},
{
    'description': 'Dasyurus maculatus',
    'sku': 29742177,
    'ean': 5051139714,
    'price_to': 147.98,
    'price_from': 1043.36,
    'qty': 168
},
{
    'description': 'Falco peregrinus',
    'sku': 97840308,
    'ean': 4963637888,
    'price_to': 856.43,
    'price_from': 910.37,
    'qty': 382
},
{
    'description': 'Recurvirostra avosetta',
    'sku': 62533691,
    'ean': 9531241654,
    'price_to': 15.48,
    'price_from': 354.4,
    'qty': 258
},
{
    'description': 'Castor fiber',
    'sku': 12247572,
    'ean': 2950830724,
    'price_to': 210.24,
    'price_from': 333.84,
    'qty': 442
},
{
    'description': 'Corallus hortulanus cooki',
    'sku': 95139547,
    'ean': 1819064861,
    'price_to': 650.38,
    'price_from': 1540.9,
    'qty': 192
},
{
    'description': 'Phalacrocorax carbo',
    'sku': 78621389,
    'ean': 6937447207,
    'price_to': 487.32,
    'price_from': 660.65,
    'qty': 296
},
{
    'description': 'Otaria flavescens',
    'sku': 73969355,
    'ean': 9013669582,
    'price_to': 730.35,
    'price_from': 1493.61,
    'qty': 160
},
{
    'description': 'Calyptorhynchus magnificus',
    'sku': 41690255,
    'ean': 7322872995,
    'price_to': 996.78,
    'price_from': 1505.87,
    'qty': 337
},
{
    'description': 'Acrantophis madagascariensis',
    'sku': 21724228,
    'ean': 5627120338,
    'price_to': 77.94,
    'price_from': 554.6,
    'qty': 127
},
{
    'description': 'Melophus lathami',
    'sku': 13355740,
    'ean': 3254408323,
    'price_to': 398.38,
    'price_from': 446.28,
    'qty': 282
},
{
    'description': 'Petaurus norfolcensis',
    'sku': 89241863,
    'ean': 2356780041,
    'price_to': 349.86,
    'price_from': 964.84,
    'qty': 478
},
{
    'description': 'Loxodonta africana',
    'sku': 29255911,
    'ean': 6241125464,
    'price_to': 626.62,
    'price_from': 1514.41,
    'qty': 127
},
{
    'description': 'Hippotragus equinus',
    'sku': 47309542,
    'ean': 8740984082,
    'price_to': 433.84,
    'price_from': 583.53,
    'qty': 263
},
{
    'description': 'Acridotheres tristis',
    'sku': 55403015,
    'ean': 9833894353,
    'price_to': 510.75,
    'price_from': 1071.04,
    'qty': 484
},
{
    'description': 'Psophia viridis',
    'sku': 55111788,
    'ean': 3498353530,
    'price_to': 6.77,
    'price_from': 289.71,
    'qty': 117
},
{
    'description': 'Zenaida asiatica',
    'sku': 90195082,
    'ean': 7599523387,
    'price_to': 246.2,
    'price_from': 786.69,
    'qty': 438
},
{
    'description': 'Trichosurus vulpecula',
    'sku': 75886090,
    'ean': 2603377384,
    'price_to': 986.38,
    'price_from': 1908.77,
    'qty': 83
},
{
    'description': 'Semnopithecus entellus',
    'sku': 42434387,
    'ean': 3535399377,
    'price_to': 380.82,
    'price_from': 916.78,
    'qty': 115
},
{
    'description': 'unavailable',
    'sku': 98989612,
    'ean': 3922824350,
    'price_to': 759.03,
    'price_from': 983.18,
    'qty': 267
},
{
    'description': 'Mazama americana',
    'sku': 30549960,
    'ean': 2474367102,
    'price_to': 132.42,
    'price_from': 161.64,
    'qty': 409
},
{
    'description': 'Macropus fuliginosus',
    'sku': 83985447,
    'ean': 2284714337,
    'price_to': 692.58,
    'price_from': 761.74,
    'qty': 149
},
{
    'description': 'Pycnonotus nigricans',
    'sku': 12325044,
    'ean': 7920522257,
    'price_to': 342.33,
    'price_from': 887.21,
    'qty': 319
},
{
    'description': 'Varanus salvator',
    'sku': 89643453,
    'ean': 7600203564,
    'price_to': 196.56,
    'price_from': 545.35,
    'qty': 15
},
{
    'description': 'Procyon cancrivorus',
    'sku': 93946863,
    'ean': 7319077787,
    'price_to': 169.06,
    'price_from': 943.82,
    'qty': 271
},
{
    'description': 'Spermophilus richardsonii',
    'sku': 53219419,
    'ean': 4939719210,
    'price_to': 405.37,
    'price_from': 474.96,
    'qty': 341
},
{
    'description': 'Haliaeetus leucoryphus',
    'sku': 40784015,
    'ean': 1378281832,
    'price_to': 280.45,
    'price_from': 536.12,
    'qty': 38
},
{
    'description': 'Myotis lucifugus',
    'sku': 20288592,
    'ean': 7668860913,
    'price_to': 90.1,
    'price_from': 722.23,
    'qty': 22
},
{
    'description': 'Paraxerus cepapi',
    'sku': 89553911,
    'ean': 3382816858,
    'price_to': 960.91,
    'price_from': 1092.41,
    'qty': 271
},
{
    'description': 'Axis axis',
    'sku': 78597719,
    'ean': 7581890614,
    'price_to': 485.72,
    'price_from': 1104.85,
    'qty': 13
},
{
    'description': 'Tadorna tadorna',
    'sku': 48861350,
    'ean': 1561856987,
    'price_to': 28.06,
    'price_from': 896.62,
    'qty': 417
},
{
    'description': 'Spermophilus parryii',
    'sku': 52057048,
    'ean': 5577750766,
    'price_to': 917.03,
    'price_from': 1355.94,
    'qty': 164
},
{
    'description': 'Larus fuliginosus',
    'sku': 87344245,
    'ean': 5940091325,
    'price_to': 682.83,
    'price_from': 790.2,
    'qty': 290
},
{
    'description': 'Physignathus cocincinus',
    'sku': 14368581,
    'ean': 4600006714,
    'price_to': 669.01,
    'price_from': 819.74,
    'qty': 434
},
{
    'description': 'Varanus sp.',
    'sku': 78443022,
    'ean': 7941069646,
    'price_to': 527.45,
    'price_from': 1198.06,
    'qty': 339
},
{
    'description': 'Choloepus hoffmani',
    'sku': 60193447,
    'ean': 3942333840,
    'price_to': 360.2,
    'price_from': 1291.73,
    'qty': 95
},
{
    'description': 'Ardea golieth',
    'sku': 88056386,
    'ean': 4658416402,
    'price_to': 412.81,
    'price_from': 695.19,
    'qty': 468
},
{
    'description': 'Carduelis pinus',
    'sku': 39857828,
    'ean': 2236412788,
    'price_to': 175.74,
    'price_from': 956.99,
    'qty': 126
},
{
    'description': 'Castor fiber',
    'sku': 91801568,
    'ean': 5728076570,
    'price_to': 752.05,
    'price_from': 1328.46,
    'qty': 448
},
{
    'description': 'Odocoilenaus virginianus',
    'sku': 14814104,
    'ean': 2872166416,
    'price_to': 294.12,
    'price_from': 1238.62,
    'qty': 478
},
{
    'description': 'Dasyurus maculatus',
    'sku': 35478780,
    'ean': 5143975385,
    'price_to': 567.81,
    'price_from': 829.27,
    'qty': 357
},
{
    'description': 'Delphinus delphis',
    'sku': 63449720,
    'ean': 4188080107,
    'price_to': 489.56,
    'price_from': 607.62,
    'qty': 298
},
{
    'description': 'Ammospermophilus nelsoni',
    'sku': 96283502,
    'ean': 5790905471,
    'price_to': 794.59,
    'price_from': 841.77,
    'qty': 84
},
{
    'description': 'Butorides striatus',
    'sku': 32728188,
    'ean': 8699026127,
    'price_to': 288.43,
    'price_from': 348.16,
    'qty': 16
},
{
    'description': 'Colobus guerza',
    'sku': 68925140,
    'ean': 7316660371,
    'price_to': 285.27,
    'price_from': 423.33,
    'qty': 325
},
{
    'description': 'Coendou prehensilis',
    'sku': 77391148,
    'ean': 6409523815,
    'price_to': 435.72,
    'price_from': 1234.95,
    'qty': 348
},
{
    'description': 'Marmota caligata',
    'sku': 99403900,
    'ean': 5647793076,
    'price_to': 116.36,
    'price_from': 775.78,
    'qty': 109
},
{
    'description': 'Dolichitus patagonum',
    'sku': 35527365,
    'ean': 7660121232,
    'price_to': 82.05,
    'price_from': 576.22,
    'qty': 69
},
{
    'description': 'Zenaida galapagoensis',
    'sku': 77484412,
    'ean': 4424418430,
    'price_to': 845.75,
    'price_from': 1769.73,
    'qty': 418
},
{
    'description': 'Gorilla gorilla',
    'sku': 21774837,
    'ean': 4424846691,
    'price_to': 944.98,
    'price_from': 1355.14,
    'qty': 271
},
{
    'description': 'Spilogale gracilis',
    'sku': 48250523,
    'ean': 8988002415,
    'price_to': 27.9,
    'price_from': 585.46,
    'qty': 232
},
{
    'description': 'Canis latrans',
    'sku': 91325016,
    'ean': 3833661190,
    'price_to': 977.21,
    'price_from': 1437.53,
    'qty': 82
},
{
    'description': 'Panthera pardus',
    'sku': 25681588,
    'ean': 9497807636,
    'price_to': 769.93,
    'price_from': 1165.2,
    'qty': 420
},
{
    'description': 'Threskionis aethiopicus',
    'sku': 68482956,
    'ean': 8140535976,
    'price_to': 855.34,
    'price_from': 1125.6,
    'qty': 390
},
{
    'description': 'Manouria emys',
    'sku': 37090961,
    'ean': 7116331422,
    'price_to': 361.37,
    'price_from': 409.54,
    'qty': 379
},
{
    'description': 'Xerus sp.',
    'sku': 63866094,
    'ean': 7255381384,
    'price_to': 543.98,
    'price_from': 1048.3,
    'qty': 417
},
{
    'description': 'Aonyx cinerea',
    'sku': 46578706,
    'ean': 8278515633,
    'price_to': 864.09,
    'price_from': 1341.67,
    'qty': 14
},
{
    'description': 'Threskionis aethiopicus',
    'sku': 81378305,
    'ean': 5661751632,
    'price_to': 945.86,
    'price_from': 1231.59,
    'qty': 65
},
{
    'description': 'Platalea leucordia',
    'sku': 15894539,
    'ean': 1956831403,
    'price_to': 109.91,
    'price_from': 486.12,
    'qty': 493
},
{
    'description': 'Snycerus caffer',
    'sku': 89453491,
    'ean': 3748499589,
    'price_to': 549.73,
    'price_from': 894.19,
    'qty': 241
},
{
    'description': 'Pandon haliaetus',
    'sku': 98830077,
    'ean': 9137443755,
    'price_to': 479.86,
    'price_from': 1433.85,
    'qty': 193
},
{
    'description': 'Pseudoleistes virescens',
    'sku': 27174621,
    'ean': 7581904901,
    'price_to': 711.78,
    'price_from': 1619.04,
    'qty': 380
},
{
    'description': 'Taurotagus oryx',
    'sku': 44830733,
    'ean': 1285214379,
    'price_to': 484.87,
    'price_from': 1270.22,
    'qty': 330
},
{
    'description': 'Chlidonias leucopterus',
    'sku': 80846791,
    'ean': 9464157146,
    'price_to': 151.94,
    'price_from': 265.19,
    'qty': 462
},
{
    'description': 'Anthropoides paradisea',
    'sku': 13619168,
    'ean': 4663511170,
    'price_to': 928.66,
    'price_from': 1531.21,
    'qty': 134
},
{
    'description': 'Graspus graspus',
    'sku': 86919723,
    'ean': 1154317786,
    'price_to': 990.04,
    'price_from': 1746.92,
    'qty': 109
},
{
    'description': 'Buteo regalis',
    'sku': 90152714,
    'ean': 2822457225,
    'price_to': 539.53,
    'price_from': 1100.13,
    'qty': 264
},
{
    'description': 'Camelus dromedarius',
    'sku': 93788607,
    'ean': 6222463816,
    'price_to': 261.3,
    'price_from': 560.97,
    'qty': 375
},
{
    'description': 'Speothos vanaticus',
    'sku': 34804787,
    'ean': 1726919691,
    'price_to': 189.45,
    'price_from': 1134.15,
    'qty': 353
},
{
    'description': 'Phalacrocorax niger',
    'sku': 69106185,
    'ean': 3126160558,
    'price_to': 289.98,
    'price_from': 1274.42,
    'qty': 436
},
{
    'description': 'Semnopithecus entellus',
    'sku': 61331239,
    'ean': 5995586417,
    'price_to': 33.62,
    'price_from': 38.45,
    'qty': 100
},
{
    'description': 'Pseudocheirus peregrinus',
    'sku': 15743044,
    'ean': 5225045630,
    'price_to': 134.42,
    'price_from': 137.46,
    'qty': 276
},
{
    'description': 'Lycaon pictus',
    'sku': 73649071,
    'ean': 5149197071,
    'price_to': 989.21,
    'price_from': 1666.96,
    'qty': 45
},
{
    'description': 'Choriotis kori',
    'sku': 39812371,
    'ean': 3341510002,
    'price_to': 699.25,
    'price_from': 1021.62,
    'qty': 83
},
{
    'description': 'Delphinus delphis',
    'sku': 65439283,
    'ean': 5419949752,
    'price_to': 943.4,
    'price_from': 1740.96,
    'qty': 244
},
{
    'description': 'Phalacrocorax brasilianus',
    'sku': 73469020,
    'ean': 8293605407,
    'price_to': 410.44,
    'price_from': 1215.92,
    'qty': 175
},
{
    'description': 'Crotalus cerastes',
    'sku': 26710248,
    'ean': 6060389938,
    'price_to': 927.14,
    'price_from': 1306.01,
    'qty': 399
},
{
    'description': 'Phalacrocorax niger',
    'sku': 30304117,
    'ean': 5717172159,
    'price_to': 415.03,
    'price_from': 478.01,
    'qty': 322
},
{
    'description': 'Hippotragus niger',
    'sku': 91886760,
    'ean': 1429894821,
    'price_to': 90.18,
    'price_from': 1054.57,
    'qty': 407
},
{
    'description': 'Grus rubicundus',
    'sku': 97339680,
    'ean': 6043665318,
    'price_to': 345.2,
    'price_from': 826.81,
    'qty': 204
},
{
    'description': 'Speotyte cuniculata',
    'sku': 47749006,
    'ean': 5053456375,
    'price_to': 209.91,
    'price_from': 461.24,
    'qty': 333
},
{
    'description': 'Colobus guerza',
    'sku': 18624100,
    'ean': 6331179870,
    'price_to': 18.3,
    'price_from': 550.08,
    'qty': 265
},
{
    'description': 'Streptopelia senegalensis',
    'sku': 38787284,
    'ean': 2874919186,
    'price_to': 337.29,
    'price_from': 522.44,
    'qty': 350
},
{
    'description': 'Tamandua tetradactyla',
    'sku': 89432680,
    'ean': 3201180012,
    'price_to': 740.38,
    'price_from': 1298.66,
    'qty': 215
},
{
    'description': 'Trichechus inunguis',
    'sku': 43123044,
    'ean': 5705939289,
    'price_to': 209.08,
    'price_from': 798.59,
    'qty': 107
},
{
    'description': 'Sylvicapra grimma',
    'sku': 60158650,
    'ean': 6442682965,
    'price_to': 879.07,
    'price_from': 1803.83,
    'qty': 181
},
{
    'description': 'Canis mesomelas',
    'sku': 88065428,
    'ean': 4887196356,
    'price_to': 762.05,
    'price_from': 1386.35,
    'qty': 281
},
{
    'description': 'Oryx gazella',
    'sku': 30210431,
    'ean': 4065091005,
    'price_to': 251.06,
    'price_from': 1185.9,
    'qty': 475
},
{
    'description': 'Dasypus septemcincus',
    'sku': 39194504,
    'ean': 4151582980,
    'price_to': 182.45,
    'price_from': 197.4,
    'qty': 228
},
{
    'description': 'Felis chaus',
    'sku': 31779676,
    'ean': 1647769685,
    'price_to': 416.44,
    'price_from': 919.57,
    'qty': 361
},
{
    'description': 'Crotalus adamanteus',
    'sku': 22097373,
    'ean': 4038519034,
    'price_to': 251.56,
    'price_from': 796.86,
    'qty': 186
},
{
    'description': 'Eolophus roseicapillus',
    'sku': 21364062,
    'ean': 9758640834,
    'price_to': 493.47,
    'price_from': 580.2,
    'qty': 493
},
{
    'description': 'Pseudalopex gymnocercus',
    'sku': 53494540,
    'ean': 2792565111,
    'price_to': 728.52,
    'price_from': 1229.66,
    'qty': 55
},
{
    'description': 'Varanus sp.',
    'sku': 19112466,
    'ean': 2260893044,
    'price_to': 955.43,
    'price_from': 1235.03,
    'qty': 262
},
{
    'description': 'Eremophila alpestris',
    'sku': 51374897,
    'ean': 2538093895,
    'price_to': 287.46,
    'price_from': 700.35,
    'qty': 203
},
{
    'description': 'Cebus apella',
    'sku': 71800578,
    'ean': 5818682837,
    'price_to': 372.36,
    'price_from': 753.99,
    'qty': 320
},
{
    'description': 'Geochelone radiata',
    'sku': 87805072,
    'ean': 8675090412,
    'price_to': 913.69,
    'price_from': 1551.87,
    'qty': 39
},
{
    'description': 'Marmota monax',
    'sku': 37379011,
    'ean': 6340890411,
    'price_to': 730.97,
    'price_from': 1362.36,
    'qty': 449
},
{
    'description': 'Tachybaptus ruficollis',
    'sku': 92997703,
    'ean': 7799216850,
    'price_to': 872.42,
    'price_from': 1299.85,
    'qty': 370
},
{
    'description': 'Alopex lagopus',
    'sku': 57333543,
    'ean': 2967227183,
    'price_to': 959.33,
    'price_from': 1120.75,
    'qty': 221
},
{
    'description': 'Vanellus chilensis',
    'sku': 57771041,
    'ean': 8542475033,
    'price_to': 319.79,
    'price_from': 1202.73,
    'qty': 448
},
{
    'description': 'Sarkidornis melanotos',
    'sku': 23716156,
    'ean': 3063113765,
    'price_to': 482.9,
    'price_from': 1163.54,
    'qty': 495
},
{
    'description': 'Vanellus armatus',
    'sku': 46531397,
    'ean': 6270150893,
    'price_to': 416.93,
    'price_from': 675.21,
    'qty': 234
},
{
    'description': 'Passer domesticus',
    'sku': 90768155,
    'ean': 3699286654,
    'price_to': 393.93,
    'price_from': 604.81,
    'qty': 13
},
{
    'description': 'Psittacula krameri',
    'sku': 28084276,
    'ean': 2835052637,
    'price_to': 345.0,
    'price_from': 379.87,
    'qty': 264
},
{
    'description': 'Morelia spilotes variegata',
    'sku': 54806672,
    'ean': 6052547447,
    'price_to': 259.5,
    'price_from': 759.97,
    'qty': 401
},
{
    'description': 'Macropus parryi',
    'sku': 38629169,
    'ean': 6748303048,
    'price_to': 439.54,
    'price_from': 522.76,
    'qty': 415
},
{
    'description': 'Trichoglossus haematodus moluccanus',
    'sku': 37995313,
    'ean': 6717980487,
    'price_to': 809.48,
    'price_from': 1020.3,
    'qty': 470
},
{
    'description': 'Stercorarius longicausus',
    'sku': 61714650,
    'ean': 2858401989,
    'price_to': 547.98,
    'price_from': 1283.03,
    'qty': 189
},
{
    'description': 'Semnopithecus entellus',
    'sku': 23700348,
    'ean': 7215891601,
    'price_to': 220.31,
    'price_from': 554.55,
    'qty': 61
},
{
    'description': 'Crotalus cerastes',
    'sku': 74003769,
    'ean': 9058240212,
    'price_to': 415.83,
    'price_from': 1231.26,
    'qty': 103
},
{
    'description': 'Sarkidornis melanotos',
    'sku': 83177970,
    'ean': 6882425655,
    'price_to': 243.85,
    'price_from': 792.35,
    'qty': 305
},
{
    'description': 'Varanus sp.',
    'sku': 65833176,
    'ean': 6680053104,
    'price_to': 138.85,
    'price_from': 140.4,
    'qty': 302
},
{
    'description': 'Ardea cinerea',
    'sku': 70522921,
    'ean': 6667992729,
    'price_to': 24.41,
    'price_from': 413.54,
    'qty': 249
},
{
    'description': 'Dusicyon thous',
    'sku': 44652185,
    'ean': 9586548592,
    'price_to': 702.7,
    'price_from': 1565.14,
    'qty': 119
},
{
    'description': 'Anastomus oscitans',
    'sku': 18364115,
    'ean': 4169013993,
    'price_to': 450.13,
    'price_from': 1444.87,
    'qty': 374
},
{
    'description': 'Myiarchus tuberculifer',
    'sku': 11673745,
    'ean': 2454960302,
    'price_to': 812.22,
    'price_from': 1393.44,
    'qty': 395
},
{
    'description': 'Colaptes campestroides',
    'sku': 93258834,
    'ean': 9808864549,
    'price_to': 346.99,
    'price_from': 476.99,
    'qty': 47
},
{
    'description': 'Paraxerus cepapi',
    'sku': 17691621,
    'ean': 1725909866,
    'price_to': 371.76,
    'price_from': 375.02,
    'qty': 350
},
{
    'description': 'Aquila chrysaetos',
    'sku': 70727874,
    'ean': 4435691533,
    'price_to': 693.37,
    'price_from': 1249.56,
    'qty': 321
},
{
    'description': 'Mungos mungo',
    'sku': 92292934,
    'ean': 6345511464,
    'price_to': 585.39,
    'price_from': 1476.79,
    'qty': 421
},
{
    'description': 'Semnopithecus entellus',
    'sku': 99784208,
    'ean': 5025665326,
    'price_to': 291.23,
    'price_from': 637.96,
    'qty': 287
},
{
    'description': 'Mycteria leucocephala',
    'sku': 20723213,
    'ean': 2600081927,
    'price_to': 243.86,
    'price_from': 619.6,
    'qty': 143
},
{
    'description': 'Tetracerus quadricornis',
    'sku': 25253342,
    'ean': 3504433925,
    'price_to': 915.68,
    'price_from': 1048.44,
    'qty': 211
},
{
    'description': 'Sula nebouxii',
    'sku': 29606774,
    'ean': 3889816851,
    'price_to': 810.77,
    'price_from': 874.17,
    'qty': 134
},
{
    'description': 'Paraxerus cepapi',
    'sku': 90958741,
    'ean': 9413554697,
    'price_to': 451.05,
    'price_from': 1060.01,
    'qty': 147
},
{
    'description': 'Chelodina longicollis',
    'sku': 33618636,
    'ean': 8466326471,
    'price_to': 726.99,
    'price_from': 1555.29,
    'qty': 460
},
{
    'description': 'Erinaceus frontalis',
    'sku': 67188218,
    'ean': 4653713898,
    'price_to': 849.31,
    'price_from': 1534.31,
    'qty': 58
},
{
    'description': 'Xerus sp.',
    'sku': 81856418,
    'ean': 6342062299,
    'price_to': 171.29,
    'price_from': 1138.49,
    'qty': 61
},
{
    'description': 'Dicrostonyx groenlandicus',
    'sku': 35155235,
    'ean': 1899372317,
    'price_to': 203.53,
    'price_from': 1048.15,
    'qty': 159
},
{
    'description': 'Dicrostonyx groenlandicus',
    'sku': 49272480,
    'ean': 7172795702,
    'price_to': 360.97,
    'price_from': 1026.14,
    'qty': 446
},
{
    'description': 'Oryx gazella callotis',
    'sku': 61646529,
    'ean': 1900539040,
    'price_to': 679.11,
    'price_from': 1609.98,
    'qty': 355
},
{
    'description': 'Bucorvus leadbeateri',
    'sku': 54096601,
    'ean': 4447991280,
    'price_to': 648.57,
    'price_from': 1033.03,
    'qty': 237
},
{
    'description': 'Bucorvus leadbeateri',
    'sku': 40192233,
    'ean': 7381479373,
    'price_to': 772.45,
    'price_from': 1203.33,
    'qty': 314
},
{
    'description': 'Ovibos moschatus',
    'sku': 57944333,
    'ean': 4540847534,
    'price_to': 755.86,
    'price_from': 1636.22,
    'qty': 411
},
{
    'description': 'Pseudocheirus peregrinus',
    'sku': 21915792,
    'ean': 8613675607,
    'price_to': 972.97,
    'price_from': 1006.18,
    'qty': 44
},
{
    'description': 'Ephipplorhynchus senegalensis',
    'sku': 49532203,
    'ean': 5714002606,
    'price_to': 550.5,
    'price_from': 887.07,
    'qty': 371
},
{
    'description': 'Gopherus agassizii',
    'sku': 13059321,
    'ean': 8995896922,
    'price_to': 151.78,
    'price_from': 770.69,
    'qty': 216
},
{
    'description': 'Butorides striatus',
    'sku': 77640193,
    'ean': 9470900371,
    'price_to': 890.95,
    'price_from': 1215.42,
    'qty': 39
},
{
    'description': 'Choloepus hoffmani',
    'sku': 68517773,
    'ean': 9240599590,
    'price_to': 944.87,
    'price_from': 1012.99,
    'qty': 478
},
{
    'description': 'Falco mexicanus',
    'sku': 52111776,
    'ean': 9382324376,
    'price_to': 748.1,
    'price_from': 1244.2,
    'qty': 256
},
{
    'description': 'Carduelis uropygialis',
    'sku': 31493185,
    'ean': 7343534054,
    'price_to': 19.29,
    'price_from': 31.46,
    'qty': 175
},
{
    'description': 'Tockus flavirostris',
    'sku': 77970803,
    'ean': 1528682413,
    'price_to': 124.15,
    'price_from': 374.5,
    'qty': 332
},
{
    'description': 'Varanus albigularis',
    'sku': 30572066,
    'ean': 1719125546,
    'price_to': 188.61,
    'price_from': 799.33,
    'qty': 271
},
{
    'description': 'unavailable',
    'sku': 89157883,
    'ean': 7707283703,
    'price_to': 252.23,
    'price_from': 827.86,
    'qty': 405
},
{
    'description': 'Lamprotornis sp.',
    'sku': 57369783,
    'ean': 7228986120,
    'price_to': 565.78,
    'price_from': 779.29,
    'qty': 303
},
{
    'description': 'Pseudocheirus peregrinus',
    'sku': 56575123,
    'ean': 3978769383,
    'price_to': 301.49,
    'price_from': 387.11,
    'qty': 443
},
{
    'description': 'Larus fuliginosus',
    'sku': 35347823,
    'ean': 3188408642,
    'price_to': 550.19,
    'price_from': 1424.31,
    'qty': 134
},
{
    'description': 'Macaca mulatta',
    'sku': 83408948,
    'ean': 6404060486,
    'price_to': 811.02,
    'price_from': 985.71,
    'qty': 280
},
{
    'description': 'Macropus agilis',
    'sku': 30716777,
    'ean': 2694936353,
    'price_to': 854.26,
    'price_from': 1148.15,
    'qty': 361
},
{
    'description': 'Alouatta seniculus',
    'sku': 68530166,
    'ean': 2189473125,
    'price_to': 881.4,
    'price_from': 1444.54,
    'qty': 265
},
{
    'description': 'Pterocles gutturalis',
    'sku': 94145271,
    'ean': 7240143662,
    'price_to': 43.15,
    'price_from': 246.79,
    'qty': 445
},
{
    'description': 'Cacatua tenuirostris',
    'sku': 63241841,
    'ean': 9171457799,
    'price_to': 441.27,
    'price_from': 979.13,
    'qty': 342
},
{
    'description': 'Uraeginthus angolensis',
    'sku': 31611005,
    'ean': 1883071557,
    'price_to': 572.43,
    'price_from': 1218.36,
    'qty': 64
},
{
    'description': 'Macaca mulatta',
    'sku': 62883732,
    'ean': 2918603981,
    'price_to': 731.16,
    'price_from': 826.11,
    'qty': 435
},
{
    'description': 'Cathartes aura',
    'sku': 94871197,
    'ean': 7798613912,
    'price_to': 474.96,
    'price_from': 741.08,
    'qty': 82
},
{
    'description': 'Stenella coeruleoalba',
    'sku': 44157000,
    'ean': 9898834687,
    'price_to': 869.45,
    'price_from': 1705.91,
    'qty': 54
},
{
    'description': 'Choriotis kori',
    'sku': 51790480,
    'ean': 8158252227,
    'price_to': 261.88,
    'price_from': 1053.69,
    'qty': 179
},
{
    'description': 'Dusicyon thous',
    'sku': 53598304,
    'ean': 8235079896,
    'price_to': 657.86,
    'price_from': 961.38,
    'qty': 7
},
{
    'description': 'Gazella thompsonii',
    'sku': 37610855,
    'ean': 8854683684,
    'price_to': 678.98,
    'price_from': 856.15,
    'qty': 188
},
{
    'description': 'Galictis vittata',
    'sku': 44859711,
    'ean': 1820048143,
    'price_to': 727.99,
    'price_from': 1006.94,
    'qty': 97
},
{
    'description': 'Eolophus roseicapillus',
    'sku': 30745673,
    'ean': 1970337560,
    'price_to': 316.84,
    'price_from': 848.49,
    'qty': 290
},
{
    'description': 'Cereopsis novaehollandiae',
    'sku': 85757869,
    'ean': 3091563335,
    'price_to': 133.86,
    'price_from': 825.56,
    'qty': 181
},
{
    'description': 'Bubalornis niger',
    'sku': 32820287,
    'ean': 2594775474,
    'price_to': 24.03,
    'price_from': 652.09,
    'qty': 196
},
{
    'description': 'Ardea cinerea',
    'sku': 32085135,
    'ean': 3975223424,
    'price_to': 108.28,
    'price_from': 1066.44,
    'qty': 453
},
{
    'description': 'Dendrocitta vagabunda',
    'sku': 81501299,
    'ean': 6178792194,
    'price_to': 892.81,
    'price_from': 1004.88,
    'qty': 250
},
{
    'description': 'Fulica cristata',
    'sku': 81517885,
    'ean': 7851096059,
    'price_to': 575.65,
    'price_from': 1321.88,
    'qty': 222
},
{
    'description': 'Meleagris gallopavo',
    'sku': 57361511,
    'ean': 6194131095,
    'price_to': 818.54,
    'price_from': 1369.89,
    'qty': 107
},
{
    'description': 'Coluber constrictor foxii',
    'sku': 12820349,
    'ean': 4932204322,
    'price_to': 7.1,
    'price_from': 292.6,
    'qty': 22
},
{
    'description': 'Ara chloroptera',
    'sku': 48602286,
    'ean': 3343069167,
    'price_to': 632.03,
    'price_from': 1317.88,
    'qty': 262
},
{
    'description': 'Ciconia ciconia',
    'sku': 88938690,
    'ean': 1598452986,
    'price_to': 989.39,
    'price_from': 1145.42,
    'qty': 475
},
{
    'description': 'Podargus strigoides',
    'sku': 29192838,
    'ean': 6568950737,
    'price_to': 474.92,
    'price_from': 1342.55,
    'qty': 83
},
{
    'description': 'Vanessa indica',
    'sku': 35255065,
    'ean': 1242418775,
    'price_to': 261.95,
    'price_from': 859.28,
    'qty': 202
},
{
    'description': 'Nycticorax nycticorax',
    'sku': 41168029,
    'ean': 1381891581,
    'price_to': 718.43,
    'price_from': 797.21,
    'qty': 241
},
{
    'description': 'Melursus ursinus',
    'sku': 11836004,
    'ean': 5029303577,
    'price_to': 939.05,
    'price_from': 1349.75,
    'qty': 149
},
{
    'description': 'Acrantophis madagascariensis',
    'sku': 38304065,
    'ean': 4405116679,
    'price_to': 229.5,
    'price_from': 723.76,
    'qty': 394
},
{
    'description': 'Pavo cristatus',
    'sku': 59361906,
    'ean': 2233496280,
    'price_to': 952.82,
    'price_from': 1659.44,
    'qty': 219
},
{
    'description': 'Laniaurius atrococcineus',
    'sku': 83955803,
    'ean': 3316010847,
    'price_to': 424.87,
    'price_from': 788.07,
    'qty': 113
},
{
    'description': 'Taurotagus oryx',
    'sku': 24274840,
    'ean': 5754008016,
    'price_to': 314.25,
    'price_from': 1257.37,
    'qty': 103
},
{
    'description': 'Trichechus inunguis',
    'sku': 93968585,
    'ean': 5582083422,
    'price_to': 919.02,
    'price_from': 1171.85,
    'qty': 450
},
{
    'description': 'Alopex lagopus',
    'sku': 35334997,
    'ean': 2558244289,
    'price_to': 888.68,
    'price_from': 1212.7,
    'qty': 465
},
{
    'description': 'Bubo virginianus',
    'sku': 41193588,
    'ean': 3727233574,
    'price_to': 144.44,
    'price_from': 594.9,
    'qty': 491
},
{
    'description': 'Eubalaena australis',
    'sku': 87564622,
    'ean': 4396033945,
    'price_to': 753.3,
    'price_from': 1400.8,
    'qty': 356
},
{
    'description': 'Semnopithecus entellus',
    'sku': 62214573,
    'ean': 1716974298,
    'price_to': 435.48,
    'price_from': 1302.35,
    'qty': 180
},
{
    'description': 'Irania gutteralis',
    'sku': 35037389,
    'ean': 6317476074,
    'price_to': 873.69,
    'price_from': 1829.25,
    'qty': 494
},
{
    'description': 'Alcelaphus buselaphus cokii',
    'sku': 98296732,
    'ean': 6161947054,
    'price_to': 634.39,
    'price_from': 1401.26,
    'qty': 57
},
{
    'description': 'Butorides striatus',
    'sku': 26876616,
    'ean': 7955908240,
    'price_to': 399.5,
    'price_from': 1135.3,
    'qty': 446
},
{
    'description': 'Canis aureus',
    'sku': 11634362,
    'ean': 6913737338,
    'price_to': 421.76,
    'price_from': 1330.85,
    'qty': 385
},
{
    'description': 'Kobus defassa',
    'sku': 80731493,
    'ean': 6233074403,
    'price_to': 182.92,
    'price_from': 865.91,
    'qty': 245
},
{
    'description': 'Martes americana',
    'sku': 86171675,
    'ean': 6972719828,
    'price_to': 696.48,
    'price_from': 1168.76,
    'qty': 158
},
{
    'description': 'Felis silvestris lybica',
    'sku': 38974617,
    'ean': 9753242354,
    'price_to': 447.68,
    'price_from': 1284.3,
    'qty': 347
},
{
    'description': 'Delphinus delphis',
    'sku': 39623697,
    'ean': 5186896666,
    'price_to': 515.12,
    'price_from': 1354.0,
    'qty': 338
},
{
    'description': 'Felis concolor',
    'sku': 37519450,
    'ean': 5589619070,
    'price_to': 518.44,
    'price_from': 1181.2,
    'qty': 149
},
{
    'description': 'Ciconia episcopus',
    'sku': 16921722,
    'ean': 9465023966,
    'price_to': 492.39,
    'price_from': 865.82,
    'qty': 245
},
{
    'description': 'Chauna torquata',
    'sku': 76810171,
    'ean': 7491352743,
    'price_to': 967.78,
    'price_from': 1132.92,
    'qty': 151
},
{
    'description': 'Deroptyus accipitrinus',
    'sku': 79605960,
    'ean': 3135127238,
    'price_to': 895.67,
    'price_from': 1278.2,
    'qty': 66
},
{
    'description': 'Stenella coeruleoalba',
    'sku': 85148225,
    'ean': 3826891224,
    'price_to': 487.88,
    'price_from': 663.84,
    'qty': 210
},
{
    'description': 'unavailable',
    'sku': 56650326,
    'ean': 2787027135,
    'price_to': 840.02,
    'price_from': 1558.33,
    'qty': 287
},
{
    'description': 'Nesomimus trifasciatus',
    'sku': 33917714,
    'ean': 3642842018,
    'price_to': 30.89,
    'price_from': 496.78,
    'qty': 165
},
{
    'description': 'Macropus giganteus',
    'sku': 57610820,
    'ean': 5697500413,
    'price_to': 747.45,
    'price_from': 1104.57,
    'qty': 307
},
{
    'description': 'Tamiasciurus hudsonicus',
    'sku': 83279456,
    'ean': 9587684995,
    'price_to': 40.78,
    'price_from': 531.24,
    'qty': 496
},
{
    'description': 'Phoeniconaias minor',
    'sku': 36568872,
    'ean': 5003197656,
    'price_to': 952.26,
    'price_from': 1924.88,
    'qty': 395
},
{
    'description': 'Papio cynocephalus',
    'sku': 65113292,
    'ean': 7076160005,
    'price_to': 567.28,
    'price_from': 1378.9,
    'qty': 280
},
{
    'description': 'Ephippiorhynchus mycteria',
    'sku': 50030618,
    'ean': 3673972988,
    'price_to': 832.19,
    'price_from': 1796.77,
    'qty': 32
},
{
    'description': 'Dicrurus adsimilis',
    'sku': 58228038,
    'ean': 6519840894,
    'price_to': 311.39,
    'price_from': 777.91,
    'qty': 329
},
{
    'description': 'Lepus townsendii',
    'sku': 63506870,
    'ean': 7073109974,
    'price_to': 711.33,
    'price_from': 1060.64,
    'qty': 69
},
{
    'description': 'Eurocephalus anguitimens',
    'sku': 88127876,
    'ean': 4254582644,
    'price_to': 76.91,
    'price_from': 871.66,
    'qty': 123
},
{
    'description': 'Anitibyx armatus',
    'sku': 67127108,
    'ean': 8092370191,
    'price_to': 283.23,
    'price_from': 1218.86,
    'qty': 459
},
{
    'description': 'Cacatua tenuirostris',
    'sku': 28130918,
    'ean': 9347072348,
    'price_to': 866.06,
    'price_from': 1021.32,
    'qty': 12
},
{
    'description': 'unavailable',
    'sku': 45008929,
    'ean': 7485101910,
    'price_to': 243.45,
    'price_from': 1009.14,
    'qty': 436
},
{
    'description': 'Petaurus breviceps',
    'sku': 23193568,
    'ean': 4637231855,
    'price_to': 238.89,
    'price_from': 957.11,
    'qty': 354
},
{
    'description': 'Lamprotornis nitens',
    'sku': 94654586,
    'ean': 4384452623,
    'price_to': 199.62,
    'price_from': 1165.79,
    'qty': 484
},
{
    'description': 'Panthera leo persica',
    'sku': 43052462,
    'ean': 3055952461,
    'price_to': 810.62,
    'price_from': 938.16,
    'qty': 100
},
{
    'description': 'Crotalus cerastes',
    'sku': 93543350,
    'ean': 3651231709,
    'price_to': 37.1,
    'price_from': 783.82,
    'qty': 77
},
{
    'description': 'Loxodonta africana',
    'sku': 98364210,
    'ean': 8784720447,
    'price_to': 663.0,
    'price_from': 835.51,
    'qty': 175
},
{
    'description': 'Rhea americana',
    'sku': 20762339,
    'ean': 7936559973,
    'price_to': 545.96,
    'price_from': 1295.17,
    'qty': 201
},
{
    'description': 'Oryx gazella',
    'sku': 87701530,
    'ean': 7955012000,
    'price_to': 355.28,
    'price_from': 890.16,
    'qty': 320
},
{
    'description': 'Trichosurus vulpecula',
    'sku': 81819255,
    'ean': 6416737898,
    'price_to': 727.27,
    'price_from': 1668.22,
    'qty': 276
},
{
    'description': 'Alopex lagopus',
    'sku': 72424212,
    'ean': 4572366239,
    'price_to': 629.43,
    'price_from': 1616.2,
    'qty': 189
},
{
    'description': 'Stercorarius longicausus',
    'sku': 36271995,
    'ean': 7730441086,
    'price_to': 807.07,
    'price_from': 944.89,
    'qty': 241
},
{
    'description': 'Chordeiles minor',
    'sku': 28084703,
    'ean': 2169151044,
    'price_to': 549.29,
    'price_from': 1160.76,
    'qty': 64
},
{
    'description': 'Eudyptula minor',
    'sku': 30219225,
    'ean': 7406365587,
    'price_to': 74.65,
    'price_from': 677.79,
    'qty': 108
},
{
    'description': 'Gyps bengalensis',
    'sku': 93447580,
    'ean': 2015491588,
    'price_to': 854.49,
    'price_from': 1836.48,
    'qty': 155
},
{
    'description': 'Pycnonotus nigricans',
    'sku': 12845670,
    'ean': 5972539585,
    'price_to': 61.27,
    'price_from': 927.54,
    'qty': 374
},
{
    'description': 'Ciconia ciconia',
    'sku': 82812071,
    'ean': 3450484795,
    'price_to': 441.7,
    'price_from': 733.98,
    'qty': 64
},
{
    'description': 'Cacatua tenuirostris',
    'sku': 29695694,
    'ean': 2535942172,
    'price_to': 980.79,
    'price_from': 1621.95,
    'qty': 252
},
{
    'description': 'Macropus rufus',
    'sku': 20203873,
    'ean': 9560350266,
    'price_to': 502.13,
    'price_from': 1069.25,
    'qty': 201
},
{
    'description': 'Alligator mississippiensis',
    'sku': 76682806,
    'ean': 1603269144,
    'price_to': 690.05,
    'price_from': 981.95,
    'qty': 221
},
{
    'description': 'Phasianus colchicus',
    'sku': 16228954,
    'ean': 8059171592,
    'price_to': 502.89,
    'price_from': 1120.17,
    'qty': 298
},
{
    'description': 'Certotrichas paena',
    'sku': 73958350,
    'ean': 2814270273,
    'price_to': 415.34,
    'price_from': 885.39,
    'qty': 183
},
{
    'description': 'Choriotis kori',
    'sku': 24613687,
    'ean': 4009213113,
    'price_to': 699.84,
    'price_from': 785.98,
    'qty': 322
},
{
    'description': 'Castor canadensis',
    'sku': 37475761,
    'ean': 4795014576,
    'price_to': 157.75,
    'price_from': 715.74,
    'qty': 321
},
{
    'description': 'Petaurus norfolcensis',
    'sku': 46108226,
    'ean': 6457020298,
    'price_to': 410.42,
    'price_from': 1282.16,
    'qty': 479
},
{
    'description': 'Ovis musimon',
    'sku': 74158278,
    'ean': 4343160294,
    'price_to': 231.82,
    'price_from': 284.05,
    'qty': 265
},
{
    'description': 'Cervus elaphus',
    'sku': 48868844,
    'ean': 9282851585,
    'price_to': 709.92,
    'price_from': 1685.24,
    'qty': 209
},
{
    'description': 'Neotis denhami',
    'sku': 25913204,
    'ean': 1970993809,
    'price_to': 995.04,
    'price_from': 1228.92,
    'qty': 307
},
{
    'description': 'Mirounga angustirostris',
    'sku': 38404686,
    'ean': 2828932444,
    'price_to': 157.89,
    'price_from': 1144.55,
    'qty': 317
},
{
    'description': 'Otaria flavescens',
    'sku': 19149345,
    'ean': 4797683408,
    'price_to': 57.38,
    'price_from': 968.17,
    'qty': 477
},
{
    'description': 'Anhinga rufa',
    'sku': 22923072,
    'ean': 7847098803,
    'price_to': 28.52,
    'price_from': 368.07,
    'qty': 33
},
{
    'description': 'Lorythaixoides concolor',
    'sku': 39992647,
    'ean': 8677264473,
    'price_to': 297.86,
    'price_from': 766.59,
    'qty': 53
},
{
    'description': 'Plegadis falcinellus',
    'sku': 55900397,
    'ean': 3779499590,
    'price_to': 453.4,
    'price_from': 499.62,
    'qty': 2
},
{
    'description': 'Cochlearius cochlearius',
    'sku': 65924522,
    'ean': 5498962796,
    'price_to': 244.95,
    'price_from': 1147.63,
    'qty': 457
},
{
    'description': 'Tiliqua scincoides',
    'sku': 85667330,
    'ean': 6038333169,
    'price_to': 16.68,
    'price_from': 794.62,
    'qty': 200
},
{
    'description': 'Columba palumbus',
    'sku': 79565696,
    'ean': 1481306968,
    'price_to': 56.27,
    'price_from': 966.73,
    'qty': 292
},
{
    'description': 'Porphyrio porphyrio',
    'sku': 54179084,
    'ean': 7340484756,
    'price_to': 333.0,
    'price_from': 524.01,
    'qty': 307
},
{
    'description': 'Tadorna tadorna',
    'sku': 41867007,
    'ean': 5925745655,
    'price_to': 722.17,
    'price_from': 728.31,
    'qty': 310
},
{
    'description': 'Varanus sp.',
    'sku': 68937960,
    'ean': 3879002583,
    'price_to': 453.97,
    'price_from': 1226.6,
    'qty': 51
},
{
    'description': 'Equus burchelli',
    'sku': 19041504,
    'ean': 5941870223,
    'price_to': 694.23,
    'price_from': 1602.94,
    'qty': 327
},
{
    'description': 'Fulica cristata',
    'sku': 70986230,
    'ean': 3126552720,
    'price_to': 84.89,
    'price_from': 563.45,
    'qty': 463
},
{
    'description': 'Nesomimus trifasciatus',
    'sku': 94942065,
    'ean': 1686816286,
    'price_to': 236.09,
    'price_from': 329.88,
    'qty': 412
},
{
    'description': 'Rhea americana',
    'sku': 75299060,
    'ean': 9893683669,
    'price_to': 769.4,
    'price_from': 809.09,
    'qty': 430
},
{
    'description': 'Catharacta skua',
    'sku': 86749028,
    'ean': 2145523457,
    'price_to': 644.37,
    'price_from': 1547.62,
    'qty': 358
},
{
    'description': 'Estrilda erythronotos',
    'sku': 73471020,
    'ean': 9972484543,
    'price_to': 918.28,
    'price_from': 919.65,
    'qty': 217
},
{
    'description': 'Ara ararauna',
    'sku': 77423974,
    'ean': 6860883694,
    'price_to': 619.04,
    'price_from': 718.12,
    'qty': 93
},
{
    'description': 'Nyctanassa violacea',
    'sku': 98186656,
    'ean': 2631448949,
    'price_to': 315.77,
    'price_from': 679.47,
    'qty': 354
},
{
    'description': 'Butorides striatus',
    'sku': 46648395,
    'ean': 8533023402,
    'price_to': 868.31,
    'price_from': 1823.21,
    'qty': 172
},
{
    'description': 'Toxostoma curvirostre',
    'sku': 38964512,
    'ean': 7714823784,
    'price_to': 536.28,
    'price_from': 932.61,
    'qty': 146
},
{
    'description': 'Galictis vittata',
    'sku': 64353441,
    'ean': 4961679933,
    'price_to': 622.96,
    'price_from': 914.33,
    'qty': 176
},
{
    'description': 'Tayassu tajacu',
    'sku': 87320907,
    'ean': 6990085654,
    'price_to': 820.32,
    'price_from': 1690.07,
    'qty': 283
},
{
    'description': 'Acrobates pygmaeus',
    'sku': 99971150,
    'ean': 6015611738,
    'price_to': 940.96,
    'price_from': 1050.35,
    'qty': 51
},
{
    'description': 'Naja sp.',
    'sku': 42941520,
    'ean': 1343451158,
    'price_to': 80.44,
    'price_from': 107.3,
    'qty': 62
},
{
    'description': 'Streptopelia decipiens',
    'sku': 64250787,
    'ean': 4458721183,
    'price_to': 505.87,
    'price_from': 621.7,
    'qty': 488
},
{
    'description': 'Gopherus agassizii',
    'sku': 82833112,
    'ean': 1250222945,
    'price_to': 189.21,
    'price_from': 202.78,
    'qty': 11
},
{
    'description': 'Laniaurius atrococcineus',
    'sku': 16456567,
    'ean': 2904889542,
    'price_to': 890.39,
    'price_from': 1023.85,
    'qty': 290
},
{
    'description': 'Coluber constrictor',
    'sku': 59673386,
    'ean': 9059763737,
    'price_to': 328.94,
    'price_from': 564.08,
    'qty': 79
},
{
    'description': 'Felis serval',
    'sku': 41397803,
    'ean': 7884589570,
    'price_to': 708.92,
    'price_from': 1378.44,
    'qty': 302
},
{
    'description': 'Lutra canadensis',
    'sku': 19304388,
    'ean': 3883584555,
    'price_to': 343.02,
    'price_from': 898.4,
    'qty': 224
},
{
    'description': 'Felis libyca',
    'sku': 39561017,
    'ean': 9366739728,
    'price_to': 928.69,
    'price_from': 1400.92,
    'qty': 110
},
{
    'description': 'Lamprotornis nitens',
    'sku': 41333936,
    'ean': 5418498851,
    'price_to': 240.59,
    'price_from': 947.26,
    'qty': 208
},
{
    'description': 'Felis wiedi or Leopardus weidi',
    'sku': 26338330,
    'ean': 6975344193,
    'price_to': 626.8,
    'price_from': 1235.42,
    'qty': 285
},
{
    'description': 'Tamandua tetradactyla',
    'sku': 64887857,
    'ean': 3280349815,
    'price_to': 179.11,
    'price_from': 257.58,
    'qty': 337
},
{
    'description': 'Prionace glauca',
    'sku': 21624718,
    'ean': 6780021346,
    'price_to': 403.41,
    'price_from': 682.6,
    'qty': 136
},
{
    'description': 'Ciconia episcopus',
    'sku': 14404476,
    'ean': 3272903927,
    'price_to': 703.69,
    'price_from': 1600.75,
    'qty': 415
},
{
    'description': 'Vanellus chilensis',
    'sku': 40663708,
    'ean': 4181802386,
    'price_to': 518.62,
    'price_from': 724.29,
    'qty': 15
},
{
    'description': 'Dasyurus viverrinus',
    'sku': 79838358,
    'ean': 9777956349,
    'price_to': 417.49,
    'price_from': 1168.5,
    'qty': 393
},
{
    'description': 'Chelodina longicollis',
    'sku': 86370447,
    'ean': 4333100886,
    'price_to': 152.65,
    'price_from': 513.89,
    'qty': 359
},
{
    'description': 'Nycticorax nycticorax',
    'sku': 67946124,
    'ean': 3576089204,
    'price_to': 651.15,
    'price_from': 922.26,
    'qty': 61
},
{
    'description': 'Canis mesomelas',
    'sku': 96818287,
    'ean': 4292130616,
    'price_to': 511.56,
    'price_from': 1276.6,
    'qty': 226
},
{
    'description': 'Nannopterum harrisi',
    'sku': 57982498,
    'ean': 5601883818,
    'price_to': 943.25,
    'price_from': 1156.66,
    'qty': 102
},
{
    'description': 'Canis mesomelas',
    'sku': 68490256,
    'ean': 2119067699,
    'price_to': 365.6,
    'price_from': 1112.58,
    'qty': 267
},
{
    'description': 'Vulpes cinereoargenteus',
    'sku': 81845638,
    'ean': 4551014829,
    'price_to': 740.84,
    'price_from': 1456.46,
    'qty': 10
},
{
    'description': 'Axis axis',
    'sku': 81085301,
    'ean': 2111675971,
    'price_to': 88.72,
    'price_from': 631.38,
    'qty': 313
},
{
    'description': 'Stenella coeruleoalba',
    'sku': 74563144,
    'ean': 3209456641,
    'price_to': 241.93,
    'price_from': 354.07,
    'qty': 305
},
{
    'description': 'Alectura lathami',
    'sku': 19738936,
    'ean': 1991427019,
    'price_to': 724.38,
    'price_from': 1245.23,
    'qty': 88
},
{
    'description': 'Cordylus giganteus',
    'sku': 43546020,
    'ean': 8667932259,
    'price_to': 554.39,
    'price_from': 1180.56,
    'qty': 358
},
{
    'description': 'Eurocephalus anguitimens',
    'sku': 92526174,
    'ean': 1984376351,
    'price_to': 297.73,
    'price_from': 859.32,
    'qty': 129
},
{
    'description': 'Ammospermophilus nelsoni',
    'sku': 76166049,
    'ean': 9995352744,
    'price_to': 159.93,
    'price_from': 725.26,
    'qty': 169
},
{
    'description': 'Paraxerus cepapi',
    'sku': 57139402,
    'ean': 5820622246,
    'price_to': 15.63,
    'price_from': 97.1,
    'qty': 500
},
{
    'description': 'Corythornis cristata',
    'sku': 72878797,
    'ean': 5768751672,
    'price_to': 397.55,
    'price_from': 574.09,
    'qty': 67
},
{
    'description': 'Aegypius tracheliotus',
    'sku': 21559561,
    'ean': 8624721481,
    'price_to': 138.17,
    'price_from': 708.26,
    'qty': 87
},
{
    'description': 'Theropithecus gelada',
    'sku': 61988434,
    'ean': 6706828073,
    'price_to': 133.98,
    'price_from': 1009.67,
    'qty': 201
},
{
    'description': 'Haematopus ater',
    'sku': 93046914,
    'ean': 6919462702,
    'price_to': 272.1,
    'price_from': 986.36,
    'qty': 189
},
{
    'description': 'Xerus sp.',
    'sku': 87194201,
    'ean': 7288120314,
    'price_to': 282.6,
    'price_from': 528.34,
    'qty': 425
},
{
    'description': 'Tockus erythrorhyncus',
    'sku': 97758334,
    'ean': 7124839247,
    'price_to': 49.97,
    'price_from': 907.7,
    'qty': 421
},
{
    'description': 'Helogale undulata',
    'sku': 41786139,
    'ean': 6141627932,
    'price_to': 789.99,
    'price_from': 1346.09,
    'qty': 244
},
{
    'description': 'Chlidonias leucopterus',
    'sku': 55679457,
    'ean': 9495114162,
    'price_to': 448.62,
    'price_from': 746.54,
    'qty': 354
},
{
    'description': 'Egretta thula',
    'sku': 99153294,
    'ean': 9920147992,
    'price_to': 924.2,
    'price_from': 1375.43,
    'qty': 195
},
{
    'description': 'Mazama americana',
    'sku': 21011285,
    'ean': 5319800090,
    'price_to': 119.41,
    'price_from': 1039.49,
    'qty': 183
},
{
    'description': 'Rhea americana',
    'sku': 29534453,
    'ean': 9801457524,
    'price_to': 660.03,
    'price_from': 928.93,
    'qty': 348
},
{
    'description': 'Melanerpes erythrocephalus',
    'sku': 38372888,
    'ean': 5879356627,
    'price_to': 985.51,
    'price_from': 1241.7,
    'qty': 16
},
{
    'description': 'Eumetopias jubatus',
    'sku': 60884574,
    'ean': 1542908490,
    'price_to': 783.95,
    'price_from': 1083.95,
    'qty': 222
},
{
    'description': 'Otocyon megalotis',
    'sku': 67908901,
    'ean': 8016763976,
    'price_to': 817.34,
    'price_from': 888.71,
    'qty': 84
},
{
    'description': 'Stercorarius longicausus',
    'sku': 77534898,
    'ean': 7437685518,
    'price_to': 48.63,
    'price_from': 510.5,
    'qty': 122
},
{
    'description': 'Propithecus verreauxi',
    'sku': 11834815,
    'ean': 6709635538,
    'price_to': 117.65,
    'price_from': 1094.99,
    'qty': 89
},
{
    'description': 'Tachybaptus ruficollis',
    'sku': 41267863,
    'ean': 5019842916,
    'price_to': 596.98,
    'price_from': 1347.72,
    'qty': 241
},
{
    'description': 'Macaca radiata',
    'sku': 64067532,
    'ean': 4018793301,
    'price_to': 6.6,
    'price_from': 926.25,
    'qty': 326
},
{
    'description': 'Bucorvus leadbeateri',
    'sku': 15872835,
    'ean': 8405069177,
    'price_to': 959.93,
    'price_from': 1152.28,
    'qty': 262
},
{
    'description': 'Pseudoleistes virescens',
    'sku': 28124843,
    'ean': 5371522953,
    'price_to': 451.95,
    'price_from': 550.97,
    'qty': 212
},
{
    'description': 'Meleagris gallopavo',
    'sku': 76902649,
    'ean': 2050735871,
    'price_to': 440.07,
    'price_from': 481.36,
    'qty': 179
},
{
    'description': 'Macaca radiata',
    'sku': 68215861,
    'ean': 1644314767,
    'price_to': 306.54,
    'price_from': 673.25,
    'qty': 464
},
{
    'description': 'Semnopithecus entellus',
    'sku': 80380881,
    'ean': 6881192619,
    'price_to': 902.4,
    'price_from': 1687.49,
    'qty': 399
},
{
    'description': 'Lycaon pictus',
    'sku': 66162487,
    'ean': 2337684632,
    'price_to': 429.9,
    'price_from': 1165.77,
    'qty': 447
},
{
    'description': 'Crotaphytus collaris',
    'sku': 68051323,
    'ean': 6867579445,
    'price_to': 708.63,
    'price_from': 1337.02,
    'qty': 200
},
{
    'description': 'Mycteria leucocephala',
    'sku': 36178885,
    'ean': 6721575623,
    'price_to': 777.31,
    'price_from': 966.78,
    'qty': 196
},
{
    'description': 'Eutamias minimus',
    'sku': 40156081,
    'ean': 3617347555,
    'price_to': 291.55,
    'price_from': 329.59,
    'qty': 476
},
{
    'description': 'Ovis musimon',
    'sku': 91381226,
    'ean': 8145505152,
    'price_to': 152.89,
    'price_from': 590.42,
    'qty': 459
},
{
    'description': 'Panthera leo persica',
    'sku': 57248329,
    'ean': 5843416430,
    'price_to': 189.63,
    'price_from': 722.16,
    'qty': 172
},
{
    'description': 'Madoqua kirkii',
    'sku': 98675604,
    'ean': 6185717725,
    'price_to': 266.39,
    'price_from': 286.29,
    'qty': 58
},
{
    'description': 'Trichoglossus chlorolepidotus',
    'sku': 35882953,
    'ean': 7246466531,
    'price_to': 945.02,
    'price_from': 1302.24,
    'qty': 420
},
{
    'description': 'Cervus unicolor',
    'sku': 42398479,
    'ean': 6848782626,
    'price_to': 293.97,
    'price_from': 881.72,
    'qty': 340
},
{
    'description': 'Haliaeetus leucocephalus',
    'sku': 59116537,
    'ean': 1957690858,
    'price_to': 41.21,
    'price_from': 168.35,
    'qty': 333
},
{
    'description': 'Anthropoides paradisea',
    'sku': 48638781,
    'ean': 8817863888,
    'price_to': 670.13,
    'price_from': 1225.59,
    'qty': 458
},
{
    'description': 'Chlamydosaurus kingii',
    'sku': 68456620,
    'ean': 2233034508,
    'price_to': 766.17,
    'price_from': 1632.68,
    'qty': 252
},
{
    'description': 'Chionis alba',
    'sku': 21827008,
    'ean': 5089717490,
    'price_to': 360.46,
    'price_from': 1118.74,
    'qty': 145
},
{
    'description': 'Tamiasciurus hudsonicus',
    'sku': 88926018,
    'ean': 3865162710,
    'price_to': 894.54,
    'price_from': 1815.4,
    'qty': 364
},
{
    'description': 'Ephippiorhynchus mycteria',
    'sku': 58427632,
    'ean': 8698819568,
    'price_to': 977.56,
    'price_from': 1197.45,
    'qty': 289
},
{
    'description': 'Varanus sp.',
    'sku': 68057822,
    'ean': 7025961543,
    'price_to': 639.19,
    'price_from': 1431.22,
    'qty': 79
},
{
    'description': 'Cordylus giganteus',
    'sku': 60609989,
    'ean': 5313789337,
    'price_to': 111.64,
    'price_from': 136.3,
    'qty': 98
},
{
    'description': 'Phoenicopterus chilensis',
    'sku': 45140653,
    'ean': 3904737729,
    'price_to': 375.59,
    'price_from': 717.85,
    'qty': 447
},
{
    'description': 'Chordeiles minor',
    'sku': 96818992,
    'ean': 4119859492,
    'price_to': 633.5,
    'price_from': 1123.12,
    'qty': 387
},
{
    'description': 'Sitta canadensis',
    'sku': 40964472,
    'ean': 9240144455,
    'price_to': 588.52,
    'price_from': 1182.05,
    'qty': 131
},
{
    'description': 'Ardea cinerea',
    'sku': 13487089,
    'ean': 5204055156,
    'price_to': 975.58,
    'price_from': 1108.96,
    'qty': 342
},
{
    'description': 'Haliaetus leucogaster',
    'sku': 59546441,
    'ean': 6409245775,
    'price_to': 188.14,
    'price_from': 931.94,
    'qty': 111
},
{
    'description': 'Zalophus californicus',
    'sku': 75799725,
    'ean': 8025844925,
    'price_to': 158.24,
    'price_from': 874.49,
    'qty': 433
},
{
    'description': 'Passer domesticus',
    'sku': 43438043,
    'ean': 5857247309,
    'price_to': 728.58,
    'price_from': 1504.23,
    'qty': 200
},
{
    'description': 'Choloepus hoffmani',
    'sku': 63770091,
    'ean': 5099474239,
    'price_to': 679.74,
    'price_from': 1341.64,
    'qty': 321
},
{
    'description': 'Corvus albus',
    'sku': 86344759,
    'ean': 9360880114,
    'price_to': 395.32,
    'price_from': 1299.3,
    'qty': 487
},
{
    'description': 'Geospiza sp.',
    'sku': 53202373,
    'ean': 8167454828,
    'price_to': 9.99,
    'price_from': 866.47,
    'qty': 365
},
{
    'description': 'Acrantophis madagascariensis',
    'sku': 75816303,
    'ean': 6061063006,
    'price_to': 109.34,
    'price_from': 353.2,
    'qty': 193
},
{
    'description': 'Lama glama',
    'sku': 88981731,
    'ean': 1981871709,
    'price_to': 19.5,
    'price_from': 232.08,
    'qty': 187
},
{
    'description': 'Streptopelia senegalensis',
    'sku': 13516415,
    'ean': 8694102105,
    'price_to': 415.09,
    'price_from': 969.01,
    'qty': 397
},
{
    'description': 'Haliaetus vocifer',
    'sku': 84536182,
    'ean': 1537330701,
    'price_to': 566.32,
    'price_from': 1128.6,
    'qty': 147
},
{
    'description': 'Butorides striatus',
    'sku': 86262541,
    'ean': 4542180293,
    'price_to': 602.05,
    'price_from': 672.01,
    'qty': 115
},
{
    'description': 'Giraffe camelopardalis',
    'sku': 98559343,
    'ean': 3267908129,
    'price_to': 152.57,
    'price_from': 273.34,
    'qty': 80
},
{
    'description': 'Choriotis kori',
    'sku': 16795298,
    'ean': 8979765861,
    'price_to': 456.6,
    'price_from': 1108.04,
    'qty': 99
},
{
    'description': 'Tadorna tadorna',
    'sku': 62364350,
    'ean': 9357908511,
    'price_to': 465.83,
    'price_from': 948.21,
    'qty': 308
},
{
    'description': 'Panthera pardus',
    'sku': 42836731,
    'ean': 8527974085,
    'price_to': 342.21,
    'price_from': 609.0,
    'qty': 194
},
{
    'description': 'Orcinus orca',
    'sku': 69377953,
    'ean': 5405228492,
    'price_to': 224.01,
    'price_from': 324.05,
    'qty': 14
},
{
    'description': 'Columba palumbus',
    'sku': 91167630,
    'ean': 8806318636,
    'price_to': 178.62,
    'price_from': 743.48,
    'qty': 468
},
{
    'description': 'Bison bison',
    'sku': 20893705,
    'ean': 7959234131,
    'price_to': 1.21,
    'price_from': 791.76,
    'qty': 141
},
{
    'description': 'Theropithecus gelada',
    'sku': 13873152,
    'ean': 8864217778,
    'price_to': 570.83,
    'price_from': 860.44,
    'qty': 334
},
{
    'description': 'Aquila chrysaetos',
    'sku': 93927949,
    'ean': 4318314333,
    'price_to': 623.1,
    'price_from': 1344.36,
    'qty': 246
},
{
    'description': 'Lutra canadensis',
    'sku': 93115543,
    'ean': 4812707077,
    'price_to': 191.35,
    'price_from': 1163.27,
    'qty': 391
},
{
    'description': 'Varanus sp.',
    'sku': 45013201,
    'ean': 4126819153,
    'price_to': 410.14,
    'price_from': 695.92,
    'qty': 171
},
{
    'description': 'Bubulcus ibis',
    'sku': 88199672,
    'ean': 1322392062,
    'price_to': 282.38,
    'price_from': 636.14,
    'qty': 469
},
{
    'description': 'Gymnorhina tibicen',
    'sku': 69985369,
    'ean': 9510698681,
    'price_to': 262.06,
    'price_from': 1082.37,
    'qty': 363
},
{
    'description': 'Bugeranus caruncalatus',
    'sku': 47987192,
    'ean': 7619451856,
    'price_to': 819.11,
    'price_from': 1092.8,
    'qty': 120
},
{
    'description': 'Acanthaster planci',
    'sku': 20439499,
    'ean': 4241656315,
    'price_to': 24.68,
    'price_from': 454.16,
    'qty': 208
},
{
    'description': 'Paradoxurus hermaphroditus',
    'sku': 21600719,
    'ean': 4047829032,
    'price_to': 70.12,
    'price_from': 194.22,
    'qty': 329
},
{
    'description': 'Diomedea irrorata',
    'sku': 28220818,
    'ean': 9103486631,
    'price_to': 445.42,
    'price_from': 701.42,
    'qty': 349
},
{
    'description': 'Ovis ammon',
    'sku': 91631130,
    'ean': 4847861866,
    'price_to': 773.94,
    'price_from': 1283.15,
    'qty': 296
},
{
    'description': 'Priodontes maximus',
    'sku': 42928969,
    'ean': 7719608836,
    'price_to': 206.67,
    'price_from': 1197.16,
    'qty': 94
},
{
    'description': 'Ara ararauna',
    'sku': 36179803,
    'ean': 8052501981,
    'price_to': 702.41,
    'price_from': 793.1,
    'qty': 251
},
{
    'description': 'Genetta genetta',
    'sku': 53330792,
    'ean': 6223127812,
    'price_to': 564.44,
    'price_from': 1441.21,
    'qty': 267
},
{
    'description': 'Stercorarius longicausus',
    'sku': 84056739,
    'ean': 4775292161,
    'price_to': 764.15,
    'price_from': 1326.35,
    'qty': 15
},
{
    'description': 'Castor canadensis',
    'sku': 94828487,
    'ean': 9407087309,
    'price_to': 110.14,
    'price_from': 276.53,
    'qty': 26
},
{
    'description': 'Paraxerus cepapi',
    'sku': 17854177,
    'ean': 1332874579,
    'price_to': 991.21,
    'price_from': 1935.69,
    'qty': 252
},
{
    'description': 'Gorilla gorilla',
    'sku': 62264421,
    'ean': 8720930442,
    'price_to': 323.78,
    'price_from': 484.54,
    'qty': 318
},
{
    'description': 'Xerus sp.',
    'sku': 28186620,
    'ean': 7318527800,
    'price_to': 421.42,
    'price_from': 1360.1,
    'qty': 84
},
{
    'description': 'Tiliqua scincoides',
    'sku': 43164342,
    'ean': 2764332125,
    'price_to': 415.48,
    'price_from': 770.84,
    'qty': 308
},
{
    'description': 'Catharacta skua',
    'sku': 54348781,
    'ean': 5583421507,
    'price_to': 758.45,
    'price_from': 784.74,
    'qty': 448
},
{
    'description': 'Oxybelis fulgidus',
    'sku': 58908245,
    'ean': 2175861321,
    'price_to': 956.47,
    'price_from': 1890.72,
    'qty': 273
},
{
    'description': 'Leprocaulinus vipera',
    'sku': 46491991,
    'ean': 4483260276,
    'price_to': 6.25,
    'price_from': 18.86,
    'qty': 317
},
{
    'description': 'Vanellus chilensis',
    'sku': 14507888,
    'ean': 3751438526,
    'price_to': 930.66,
    'price_from': 1301.67,
    'qty': 285
},
{
    'description': 'Lybius torquatus',
    'sku': 59905779,
    'ean': 4199024550,
    'price_to': 146.87,
    'price_from': 194.8,
    'qty': 316
},
{
    'description': 'Mazama gouazoubira',
    'sku': 29569050,
    'ean': 1863933558,
    'price_to': 686.02,
    'price_from': 1423.96,
    'qty': 247
},
{
    'description': 'Acinynox jubatus',
    'sku': 17784080,
    'ean': 7736713901,
    'price_to': 596.16,
    'price_from': 1453.98,
    'qty': 301
},
{
    'description': 'Hystrix indica',
    'sku': 99829973,
    'ean': 7733878476,
    'price_to': 485.32,
    'price_from': 1353.9,
    'qty': 120
},
{
    'description': 'Trichoglossus chlorolepidotus',
    'sku': 14031060,
    'ean': 6507665670,
    'price_to': 652.99,
    'price_from': 1501.24,
    'qty': 303
},
{
    'description': 'Ursus arctos',
    'sku': 77683359,
    'ean': 7483662615,
    'price_to': 290.55,
    'price_from': 1201.76,
    'qty': 174
},
{
    'description': 'Papio cynocephalus',
    'sku': 71513141,
    'ean': 8023697201,
    'price_to': 602.06,
    'price_from': 1417.95,
    'qty': 65
},
{
    'description': 'Pterocles gutturalis',
    'sku': 93147705,
    'ean': 4456321998,
    'price_to': 32.49,
    'price_from': 402.95,
    'qty': 477
},
{
    'description': 'Mazama gouazoubira',
    'sku': 68971655,
    'ean': 7340219785,
    'price_to': 337.18,
    'price_from': 1063.88,
    'qty': 35
},
{
    'description': 'Aegypius occipitalis',
    'sku': 24561337,
    'ean': 4563877320,
    'price_to': 269.13,
    'price_from': 957.2,
    'qty': 366
},
{
    'description': 'Rhabdomys pumilio',
    'sku': 49575879,
    'ean': 5074267913,
    'price_to': 284.3,
    'price_from': 771.3,
    'qty': 297
},
{
    'description': 'Gazella granti',
    'sku': 20316807,
    'ean': 6466653561,
    'price_to': 11.28,
    'price_from': 244.21,
    'qty': 432
},
{
    'description': 'Redunca redunca',
    'sku': 16915766,
    'ean': 5721710832,
    'price_to': 637.38,
    'price_from': 791.65,
    'qty': 16
},
{
    'description': 'Acrantophis madagascariensis',
    'sku': 42174565,
    'ean': 7791181114,
    'price_to': 987.02,
    'price_from': 1325.1,
    'qty': 405
},
{
    'description': 'Anthropoides paradisea',
    'sku': 84064367,
    'ean': 2999729540,
    'price_to': 321.08,
    'price_from': 1111.63,
    'qty': 154
},
{
    'description': 'Papio ursinus',
    'sku': 30343175,
    'ean': 5853177985,
    'price_to': 259.63,
    'price_from': 819.56,
    'qty': 47
},
{
    'description': 'Neotis denhami',
    'sku': 72043948,
    'ean': 3178144695,
    'price_to': 401.89,
    'price_from': 1146.69,
    'qty': 255
},
{
    'description': 'Nyctanassa violacea',
    'sku': 20667790,
    'ean': 4209451773,
    'price_to': 419.56,
    'price_from': 529.71,
    'qty': 470
},
{
    'description': 'Lamprotornis nitens',
    'sku': 52838032,
    'ean': 3250000836,
    'price_to': 869.1,
    'price_from': 1795.05,
    'qty': 283
},
{
    'description': 'Pterocles gutturalis',
    'sku': 16099016,
    'ean': 7262575466,
    'price_to': 944.31,
    'price_from': 1477.65,
    'qty': 66
},
{
    'description': 'Marmota caligata',
    'sku': 59319141,
    'ean': 5719660287,
    'price_to': 674.86,
    'price_from': 1620.47,
    'qty': 175
},
{
    'description': 'Morelia spilotes variegata',
    'sku': 68920290,
    'ean': 2906386962,
    'price_to': 200.97,
    'price_from': 318.3,
    'qty': 34
},
{
    'description': 'Odocoileus hemionus',
    'sku': 66081496,
    'ean': 8243447851,
    'price_to': 139.79,
    'price_from': 578.51,
    'qty': 164
},
{
    'description': 'Casmerodius albus',
    'sku': 42131845,
    'ean': 4290496330,
    'price_to': 976.78,
    'price_from': 1720.1,
    'qty': 158
},
{
    'description': 'Ardea golieth',
    'sku': 12246094,
    'ean': 3098955315,
    'price_to': 464.87,
    'price_from': 624.48,
    'qty': 472
},
{
    'description': 'Ursus arctos',
    'sku': 32862801,
    'ean': 9036030015,
    'price_to': 555.44,
    'price_from': 1349.16,
    'qty': 429
},
{
    'description': 'Antidorcas marsupialis',
    'sku': 63156486,
    'ean': 9814682227,
    'price_to': 220.9,
    'price_from': 883.41,
    'qty': 258
},
{
    'description': 'Ammospermophilus nelsoni',
    'sku': 98286294,
    'ean': 3880127363,
    'price_to': 321.5,
    'price_from': 1017.92,
    'qty': 305
},
{
    'description': 'Macropus eugenii',
    'sku': 54514351,
    'ean': 3880361507,
    'price_to': 419.84,
    'price_from': 480.8,
    'qty': 78
},
{
    'description': 'Phoenicopterus chilensis',
    'sku': 66839420,
    'ean': 1543611850,
    'price_to': 980.83,
    'price_from': 1684.73,
    'qty': 404
},
{
    'description': 'Phalaropus lobatus',
    'sku': 71253556,
    'ean': 9911996473,
    'price_to': 772.96,
    'price_from': 1447.78,
    'qty': 444
},
{
    'description': 'Leipoa ocellata',
    'sku': 35941314,
    'ean': 5982573904,
    'price_to': 201.17,
    'price_from': 627.94,
    'qty': 370
},
{
    'description': 'Cordylus giganteus',
    'sku': 20794930,
    'ean': 3458928752,
    'price_to': 70.18,
    'price_from': 184.59,
    'qty': 345
},
{
    'description': 'Laniaurius atrococcineus',
    'sku': 62476071,
    'ean': 8892983846,
    'price_to': 420.89,
    'price_from': 1117.41,
    'qty': 200
},
{
    'description': 'Ursus americanus',
    'sku': 23253048,
    'ean': 3177628850,
    'price_to': 781.5,
    'price_from': 1362.5,
    'qty': 22
},
{
    'description': 'Aonyx capensis',
    'sku': 77949941,
    'ean': 1905108144,
    'price_to': 203.97,
    'price_from': 927.83,
    'qty': 401
},
{
    'description': 'Macaca mulatta',
    'sku': 57064371,
    'ean': 9924026482,
    'price_to': 68.81,
    'price_from': 758.64,
    'qty': 485
},
{
    'description': 'Crotalus adamanteus',
    'sku': 98377206,
    'ean': 6485138337,
    'price_to': 568.6,
    'price_from': 1318.22,
    'qty': 323
},
{
    'description': 'Meles meles',
    'sku': 65031250,
    'ean': 1346740565,
    'price_to': 614.92,
    'price_from': 1345.65,
    'qty': 65
},
{
    'description': 'Pseudalopex gymnocercus',
    'sku': 74260893,
    'ean': 9287472877,
    'price_to': 108.52,
    'price_from': 354.84,
    'qty': 417
},
{
    'description': 'Lycaon pictus',
    'sku': 54099211,
    'ean': 7790015401,
    'price_to': 407.45,
    'price_from': 791.09,
    'qty': 326
},
{
    'description': 'Lepus townsendii',
    'sku': 42383005,
    'ean': 6702202070,
    'price_to': 524.58,
    'price_from': 829.53,
    'qty': 351
},
{
    'description': 'Lorythaixoides concolor',
    'sku': 84109054,
    'ean': 8605007884,
    'price_to': 154.54,
    'price_from': 1047.27,
    'qty': 51
},
{
    'description': 'Alouatta seniculus',
    'sku': 48944726,
    'ean': 7356082952,
    'price_to': 930.36,
    'price_from': 1763.97,
    'qty': 383
},
{
    'description': 'Fratercula corniculata',
    'sku': 29503851,
    'ean': 7446181552,
    'price_to': 105.79,
    'price_from': 997.69,
    'qty': 351
},
{
    'description': 'Laniarius ferrugineus',
    'sku': 21586121,
    'ean': 9048937781,
    'price_to': 149.59,
    'price_from': 543.71,
    'qty': 408
},
{
    'description': 'Butorides striatus',
    'sku': 73666947,
    'ean': 9779348216,
    'price_to': 60.72,
    'price_from': 704.0,
    'qty': 357
},
{
    'description': 'Libellula quadrimaculata',
    'sku': 39025241,
    'ean': 3737635090,
    'price_to': 611.54,
    'price_from': 668.18,
    'qty': 1
},
{
    'description': 'Gymnorhina tibicen',
    'sku': 89942636,
    'ean': 5823881749,
    'price_to': 330.83,
    'price_from': 794.06,
    'qty': 113
},
{
    'description': 'Kobus defassa',
    'sku': 21989504,
    'ean': 5716463181,
    'price_to': 655.33,
    'price_from': 1576.42,
    'qty': 290
},
{
    'description': 'Ursus americanus',
    'sku': 19798041,
    'ean': 6484580623,
    'price_to': 951.79,
    'price_from': 1192.3,
    'qty': 144
},
{
    'description': 'Nyctereutes procyonoides',
    'sku': 18419259,
    'ean': 6157700838,
    'price_to': 527.26,
    'price_from': 771.62,
    'qty': 217
},
{
    'description': 'Haliaetus vocifer',
    'sku': 64565080,
    'ean': 9485124936,
    'price_to': 745.96,
    'price_from': 1601.74,
    'qty': 429
},
{
    'description': 'Limosa haemastica',
    'sku': 26408689,
    'ean': 1224874290,
    'price_to': 43.47,
    'price_from': 449.2,
    'qty': 295
},
{
    'description': 'Diomedea irrorata',
    'sku': 91942876,
    'ean': 4071112564,
    'price_to': 165.94,
    'price_from': 479.8,
    'qty': 232
},
{
    'description': 'Propithecus verreauxi',
    'sku': 81530137,
    'ean': 4758172729,
    'price_to': 820.54,
    'price_from': 1687.29,
    'qty': 472
},
{
    'description': 'Alouatta seniculus',
    'sku': 92494249,
    'ean': 4090579431,
    'price_to': 164.08,
    'price_from': 425.85,
    'qty': 279
},
{
    'description': 'Tayassu tajacu',
    'sku': 34081451,
    'ean': 2824195786,
    'price_to': 52.87,
    'price_from': 569.91,
    'qty': 62
},
{
    'description': 'Vulpes vulpes',
    'sku': 41414133,
    'ean': 6108456917,
    'price_to': 892.04,
    'price_from': 1370.8,
    'qty': 308
},
{
    'description': 'Galictis vittata',
    'sku': 84690430,
    'ean': 1993707585,
    'price_to': 195.92,
    'price_from': 777.87,
    'qty': 385
},
{
    'description': 'Balearica pavonina',
    'sku': 83829444,
    'ean': 8631124327,
    'price_to': 533.53,
    'price_from': 587.7,
    'qty': 196
},
{
    'description': 'Macropus agilis',
    'sku': 40915002,
    'ean': 1270459359,
    'price_to': 901.83,
    'price_from': 1545.47,
    'qty': 202
},
{
    'description': 'Bucorvus leadbeateri',
    'sku': 88544135,
    'ean': 5053498207,
    'price_to': 346.45,
    'price_from': 838.58,
    'qty': 353
},
{
    'description': 'Phalacrocorax niger',
    'sku': 49463476,
    'ean': 5208278622,
    'price_to': 4.5,
    'price_from': 49.36,
    'qty': 83
},
{
    'description': 'Felis caracal',
    'sku': 86235864,
    'ean': 7765677509,
    'price_to': 494.5,
    'price_from': 526.72,
    'qty': 204
},
{
    'description': 'Microcavia australis',
    'sku': 50886250,
    'ean': 7837114588,
    'price_to': 443.48,
    'price_from': 942.76,
    'qty': 421
},
{
    'description': 'Myrmecobius fasciatus',
    'sku': 24472723,
    'ean': 5394249967,
    'price_to': 129.1,
    'price_from': 582.71,
    'qty': 117
},
{
    'description': 'Ovis dalli stonei',
    'sku': 38013088,
    'ean': 5593630348,
    'price_to': 695.95,
    'price_from': 784.41,
    'qty': 11
},
{
    'description': 'Gekko gecko',
    'sku': 95734758,
    'ean': 3924689374,
    'price_to': 292.85,
    'price_from': 777.05,
    'qty': 303
},
{
    'description': 'Terrapene carolina',
    'sku': 47669990,
    'ean': 5799971616,
    'price_to': 340.57,
    'price_from': 726.43,
    'qty': 437
},
{
    'description': 'Paradoxurus hermaphroditus',
    'sku': 71909636,
    'ean': 3041834901,
    'price_to': 196.59,
    'price_from': 569.51,
    'qty': 184
},
{
    'description': 'Gyps bengalensis',
    'sku': 57752668,
    'ean': 4853800433,
    'price_to': 34.97,
    'price_from': 817.33,
    'qty': 218
},
{
    'description': 'Pavo cristatus',
    'sku': 75501611,
    'ean': 9803147186,
    'price_to': 123.96,
    'price_from': 924.65,
    'qty': 478
},
{
    'description': 'Hystrix cristata',
    'sku': 38068082,
    'ean': 9866802264,
    'price_to': 671.52,
    'price_from': 1662.17,
    'qty': 157
},
{
    'description': 'Sylvicapra grimma',
    'sku': 37719550,
    'ean': 1973217044,
    'price_to': 617.91,
    'price_from': 1415.66,
    'qty': 158
},
{
    'description': 'Cervus duvauceli',
    'sku': 69894027,
    'ean': 9796502450,
    'price_to': 908.16,
    'price_from': 1298.24,
    'qty': 388
},
{
    'description': 'Odocoileus hemionus',
    'sku': 78412032,
    'ean': 8421106870,
    'price_to': 540.81,
    'price_from': 739.99,
    'qty': 217
},
{
    'description': 'Climacteris melanura',
    'sku': 83838189,
    'ean': 7924388207,
    'price_to': 107.8,
    'price_from': 964.37,
    'qty': 46
},
{
    'description': 'Crotalus triseriatus',
    'sku': 29844844,
    'ean': 7511259833,
    'price_to': 834.75,
    'price_from': 1511.77,
    'qty': 483
},
{
    'description': 'Anser caerulescens',
    'sku': 71776739,
    'ean': 2017043116,
    'price_to': 54.05,
    'price_from': 617.92,
    'qty': 461
},
{
    'description': 'Fratercula corniculata',
    'sku': 98362619,
    'ean': 4580785683,
    'price_to': 631.44,
    'price_from': 1373.02,
    'qty': 78
},
{
    'description': 'Felis wiedi or Leopardus weidi',
    'sku': 64176700,
    'ean': 7469649386,
    'price_to': 209.03,
    'price_from': 498.24,
    'qty': 65
},
{
    'description': 'Uraeginthus granatina',
    'sku': 81060509,
    'ean': 5979633896,
    'price_to': 94.89,
    'price_from': 205.02,
    'qty': 151
},
{
    'description': 'Macropus parryi',
    'sku': 95654753,
    'ean': 2250350419,
    'price_to': 601.84,
    'price_from': 878.55,
    'qty': 497
},
{
    'description': 'Varanus sp.',
    'sku': 34619541,
    'ean': 6733339146,
    'price_to': 721.88,
    'price_from': 1269.37,
    'qty': 79
},
{
    'description': 'Ara chloroptera',
    'sku': 30251240,
    'ean': 3127020541,
    'price_to': 673.41,
    'price_from': 1186.34,
    'qty': 354
},
{
    'description': 'Eumetopias jubatus',
    'sku': 70213878,
    'ean': 7421428178,
    'price_to': 914.37,
    'price_from': 1238.48,
    'qty': 179
},
{
    'description': 'Alopex lagopus',
    'sku': 32559178,
    'ean': 4427061285,
    'price_to': 552.53,
    'price_from': 941.13,
    'qty': 372
},
{
    'description': 'Phalacrocorax albiventer',
    'sku': 14928587,
    'ean': 9743266369,
    'price_to': 451.22,
    'price_from': 546.38,
    'qty': 177
},
{
    'description': 'Tachybaptus ruficollis',
    'sku': 43513200,
    'ean': 4527409589,
    'price_to': 848.81,
    'price_from': 1612.68,
    'qty': 249
},
{
    'description': 'Boa caninus',
    'sku': 66382896,
    'ean': 5140657970,
    'price_to': 370.24,
    'price_from': 1358.68,
    'qty': 409
},
{
    'description': 'Cyrtodactylus louisiadensis',
    'sku': 81835913,
    'ean': 3117886893,
    'price_to': 61.4,
    'price_from': 421.03,
    'qty': 491
},
{
    'description': 'Diomedea irrorata',
    'sku': 40587314,
    'ean': 5649742998,
    'price_to': 489.51,
    'price_from': 1363.87,
    'qty': 382
},
{
    'description': 'Libellula quadrimaculata',
    'sku': 94850567,
    'ean': 1533734608,
    'price_to': 430.8,
    'price_from': 678.86,
    'qty': 481
},
{
    'description': 'Taurotagus oryx',
    'sku': 61962893,
    'ean': 8368098413,
    'price_to': 109.23,
    'price_from': 489.68,
    'qty': 78
},
{
    'description': 'Tragelaphus scriptus',
    'sku': 93425631,
    'ean': 6413372431,
    'price_to': 402.8,
    'price_from': 1319.02,
    'qty': 315
},
{
    'description': 'Bubo virginianus',
    'sku': 53769375,
    'ean': 5285679135,
    'price_to': 969.3,
    'price_from': 1070.8,
    'qty': 402
},
{
    'description': 'Choriotis kori',
    'sku': 87673764,
    'ean': 7168439778,
    'price_to': 317.19,
    'price_from': 434.82,
    'qty': 148
},
{
    'description': 'Raphicerus campestris',
    'sku': 18757595,
    'ean': 9859184478,
    'price_to': 485.86,
    'price_from': 521.25,
    'qty': 196
},
{
    'description': 'Streptopelia decipiens',
    'sku': 89942344,
    'ean': 3175095220,
    'price_to': 469.06,
    'price_from': 1245.62,
    'qty': 414
},
{
    'description': 'Laniarius ferrugineus',
    'sku': 45879666,
    'ean': 3477025100,
    'price_to': 653.78,
    'price_from': 1635.63,
    'qty': 314
},
{
    'description': 'Melophus lathami',
    'sku': 98915482,
    'ean': 3687975620,
    'price_to': 721.08,
    'price_from': 1015.71,
    'qty': 118
},
{
    'description': 'Pseudalopex gymnocercus',
    'sku': 53204005,
    'ean': 3187641681,
    'price_to': 513.6,
    'price_from': 631.96,
    'qty': 353
},
{
    'description': 'Bucephala clangula',
    'sku': 28357396,
    'ean': 3774779762,
    'price_to': 385.4,
    'price_from': 1092.91,
    'qty': 313
},
{
    'description': 'Coluber constrictor',
    'sku': 73573850,
    'ean': 4257126808,
    'price_to': 613.31,
    'price_from': 1378.93,
    'qty': 241
},
{
    'description': 'Geococcyx californianus',
    'sku': 60674453,
    'ean': 5467662315,
    'price_to': 304.51,
    'price_from': 312.99,
    'qty': 147
},
{
    'description': 'Colaptes campestroides',
    'sku': 53696622,
    'ean': 1572150157,
    'price_to': 788.13,
    'price_from': 1522.41,
    'qty': 376
},
{
    'description': 'Bugeranus caruncalatus',
    'sku': 41546696,
    'ean': 5054139705,
    'price_to': 289.25,
    'price_from': 1195.77,
    'qty': 428
},
{
    'description': 'Cracticus nigroagularis',
    'sku': 81647871,
    'ean': 3512410130,
    'price_to': 727.91,
    'price_from': 1252.34,
    'qty': 371
},
{
    'description': 'Lepilemur rufescens',
    'sku': 89654397,
    'ean': 6161814769,
    'price_to': 709.19,
    'price_from': 869.94,
    'qty': 193
},
{
    'description': 'unavailable',
    'sku': 86375801,
    'ean': 2129742332,
    'price_to': 109.88,
    'price_from': 515.16,
    'qty': 237
},
{
    'description': 'Phoeniconaias minor',
    'sku': 95735298,
    'ean': 7158346336,
    'price_to': 868.42,
    'price_from': 1045.57,
    'qty': 253
},
{
    'description': 'Eudyptula minor',
    'sku': 25523530,
    'ean': 8551115581,
    'price_to': 777.53,
    'price_from': 1203.26,
    'qty': 91
},
{
    'description': 'Macropus parryi',
    'sku': 11447529,
    'ean': 9241749844,
    'price_to': 512.67,
    'price_from': 802.35,
    'qty': 227
},
{
    'description': 'Amblyrhynchus cristatus',
    'sku': 28214016,
    'ean': 8445115483,
    'price_to': 359.67,
    'price_from': 1072.14,
    'qty': 26
},
{
    'description': 'Tiliqua scincoides',
    'sku': 33230316,
    'ean': 1583744523,
    'price_to': 496.18,
    'price_from': 1150.27,
    'qty': 26
},
{
    'description': 'Cathartes aura',
    'sku': 62847342,
    'ean': 7674730255,
    'price_to': 963.64,
    'price_from': 1616.06,
    'qty': 480
},
{
    'description': 'Anitibyx armatus',
    'sku': 93221991,
    'ean': 7140456070,
    'price_to': 173.61,
    'price_from': 473.15,
    'qty': 405
},
{
    'description': 'Sagittarius serpentarius',
    'sku': 41757606,
    'ean': 5339229093,
    'price_to': 607.37,
    'price_from': 1502.16,
    'qty': 415
},
{
    'description': 'Colobus guerza',
    'sku': 53362880,
    'ean': 9346500891,
    'price_to': 706.17,
    'price_from': 821.95,
    'qty': 238
},
{
    'description': 'Alcelaphus buselaphus cokii',
    'sku': 96147191,
    'ean': 5627841384,
    'price_to': 145.12,
    'price_from': 730.89,
    'qty': 142
},
{
    'description': 'Trachyphonus vaillantii',
    'sku': 98069584,
    'ean': 4493208721,
    'price_to': 219.28,
    'price_from': 669.2,
    'qty': 391
},
{
    'description': 'Sitta canadensis',
    'sku': 19462713,
    'ean': 7699561248,
    'price_to': 347.77,
    'price_from': 1119.71,
    'qty': 96
},
{
    'description': 'Lutra canadensis',
    'sku': 71887061,
    'ean': 7866362750,
    'price_to': 920.45,
    'price_from': 1111.65,
    'qty': 349
},
{
    'description': 'Martes americana',
    'sku': 30553058,
    'ean': 7056887365,
    'price_to': 159.41,
    'price_from': 262.92,
    'qty': 435
},
{
    'description': 'Motacilla aguimp',
    'sku': 67671375,
    'ean': 2627468959,
    'price_to': 669.31,
    'price_from': 958.17,
    'qty': 143
},
{
    'description': 'Paraxerus cepapi',
    'sku': 70149431,
    'ean': 9029818845,
    'price_to': 341.15,
    'price_from': 580.66,
    'qty': 208
},
{
    'description': 'Haliaeetus leucocephalus',
    'sku': 21240349,
    'ean': 4081536471,
    'price_to': 664.4,
    'price_from': 1586.45,
    'qty': 132
},
{
    'description': 'Anas punctata',
    'sku': 46131155,
    'ean': 9470773017,
    'price_to': 535.97,
    'price_from': 1405.52,
    'qty': 350
},
{
    'description': 'Leprocaulinus vipera',
    'sku': 14772861,
    'ean': 3526082976,
    'price_to': 970.31,
    'price_from': 1268.27,
    'qty': 412
},
{
    'description': 'Theropithecus gelada',
    'sku': 44936744,
    'ean': 7563614229,
    'price_to': 494.77,
    'price_from': 1446.77,
    'qty': 239
},
{
    'description': 'Dusicyon thous',
    'sku': 88475420,
    'ean': 2157080847,
    'price_to': 929.17,
    'price_from': 1491.48,
    'qty': 168
},
{
    'description': 'Zalophus californicus',
    'sku': 26402334,
    'ean': 6828281109,
    'price_to': 491.76,
    'price_from': 682.67,
    'qty': 62
},
{
    'description': 'Pteropus rufus',
    'sku': 15586285,
    'ean': 2116384778,
    'price_to': 503.41,
    'price_from': 1103.58,
    'qty': 232
},
{
    'description': 'Dendrocitta vagabunda',
    'sku': 87273114,
    'ean': 7212117531,
    'price_to': 573.4,
    'price_from': 999.02,
    'qty': 196
},
{
    'description': 'Eubalaena australis',
    'sku': 44884563,
    'ean': 6517884388,
    'price_to': 161.36,
    'price_from': 559.48,
    'qty': 458
},
{
    'description': 'Lasiorhinus latifrons',
    'sku': 88876132,
    'ean': 3767635654,
    'price_to': 744.61,
    'price_from': 1371.63,
    'qty': 160
},
{
    'description': 'Suricata suricatta',
    'sku': 19993116,
    'ean': 1940709484,
    'price_to': 606.16,
    'price_from': 617.21,
    'qty': 123
},
{
    'description': 'Larus sp.',
    'sku': 95398790,
    'ean': 1484344114,
    'price_to': 895.34,
    'price_from': 1733.62,
    'qty': 422
},
{
    'description': 'Ramphastos tucanus',
    'sku': 30683166,
    'ean': 5168398961,
    'price_to': 695.68,
    'price_from': 1549.95,
    'qty': 238
},
{
    'description': 'Sarcorhamphus papa',
    'sku': 85031286,
    'ean': 5413658152,
    'price_to': 506.69,
    'price_from': 815.34,
    'qty': 171
},
{
    'description': 'Tadorna tadorna',
    'sku': 56560513,
    'ean': 7797281017,
    'price_to': 225.92,
    'price_from': 927.5,
    'qty': 376
},
{
    'description': 'Toxostoma curvirostre',
    'sku': 70623050,
    'ean': 5644284624,
    'price_to': 606.14,
    'price_from': 691.82,
    'qty': 400
},
{
    'description': 'Megaderma spasma',
    'sku': 24633857,
    'ean': 3097960028,
    'price_to': 276.43,
    'price_from': 319.15,
    'qty': 241
},
{
    'description': 'Cyrtodactylus louisiadensis',
    'sku': 79148428,
    'ean': 7005009669,
    'price_to': 201.99,
    'price_from': 205.81,
    'qty': 421
},
{
    'description': 'Macropus fuliginosus',
    'sku': 65999889,
    'ean': 3441109110,
    'price_to': 541.05,
    'price_from': 807.15,
    'qty': 291
},
{
    'description': 'Lophoaetus occipitalis',
    'sku': 61012458,
    'ean': 5190625162,
    'price_to': 719.87,
    'price_from': 1246.87,
    'qty': 250
},
{
    'description': 'Climacteris melanura',
    'sku': 31378862,
    'ean': 9606165736,
    'price_to': 903.75,
    'price_from': 1297.68,
    'qty': 445
},
{
    'description': 'Spizaetus coronatus',
    'sku': 37089899,
    'ean': 6251285880,
    'price_to': 673.8,
    'price_from': 754.15,
    'qty': 328
},
{
    'description': 'Lamprotornis sp.',
    'sku': 30434147,
    'ean': 3707492311,
    'price_to': 324.42,
    'price_from': 970.91,
    'qty': 178
},
{
    'description': 'Phalacrocorax niger',
    'sku': 44009478,
    'ean': 9923195301,
    'price_to': 478.21,
    'price_from': 955.66,
    'qty': 160
},
{
    'description': 'Paradoxurus hermaphroditus',
    'sku': 65510393,
    'ean': 8180527062,
    'price_to': 4.67,
    'price_from': 304.8,
    'qty': 402
},
{
    'description': 'Redunca redunca',
    'sku': 99764871,
    'ean': 5138770456,
    'price_to': 57.1,
    'price_from': 180.11,
    'qty': 189
},
{
    'description': 'Megaderma spasma',
    'sku': 99913492,
    'ean': 8144366986,
    'price_to': 666.91,
    'price_from': 1207.97,
    'qty': 355
},
{
    'description': 'Rhea americana',
    'sku': 87962351,
    'ean': 1270174556,
    'price_to': 181.41,
    'price_from': 1144.82,
    'qty': 434
},
{
    'description': 'Porphyrio porphyrio',
    'sku': 76449802,
    'ean': 8112774953,
    'price_to': 126.58,
    'price_from': 251.52,
    'qty': 323
},
{
    'description': 'Heloderma horridum',
    'sku': 81746936,
    'ean': 6887817744,
    'price_to': 507.56,
    'price_from': 1485.65,
    'qty': 138
},
{
    'description': 'Vanellus sp.',
    'sku': 76337704,
    'ean': 2371943349,
    'price_to': 297.94,
    'price_from': 1019.19,
    'qty': 242
},
{
    'description': 'Egretta thula',
    'sku': 59217144,
    'ean': 9941260581,
    'price_to': 811.07,
    'price_from': 1284.32,
    'qty': 153
},
{
    'description': 'Equus burchelli',
    'sku': 68678354,
    'ean': 5427978961,
    'price_to': 153.85,
    'price_from': 850.95,
    'qty': 187
},
{
    'description': 'Equus hemionus',
    'sku': 54117198,
    'ean': 1388035005,
    'price_to': 691.65,
    'price_from': 1206.12,
    'qty': 395
},
{
    'description': 'Gekko gecko',
    'sku': 82447949,
    'ean': 2341445798,
    'price_to': 101.38,
    'price_from': 612.07,
    'qty': 363
},
{
    'description': 'Felis wiedi or Leopardus weidi',
    'sku': 38695801,
    'ean': 7099476706,
    'price_to': 667.87,
    'price_from': 1094.97,
    'qty': 234
},
{
    'description': 'Mabuya spilogaster',
    'sku': 97131939,
    'ean': 8900324993,
    'price_to': 35.15,
    'price_from': 290.46,
    'qty': 488
},
{
    'description': 'Theropithecus gelada',
    'sku': 66415895,
    'ean': 1645632995,
    'price_to': 340.12,
    'price_from': 1020.1,
    'qty': 451
},
{
    'description': 'Ninox superciliaris',
    'sku': 96517376,
    'ean': 1354309562,
    'price_to': 211.45,
    'price_from': 779.82,
    'qty': 141
},
{
    'description': 'Otocyon megalotis',
    'sku': 53937541,
    'ean': 6260659690,
    'price_to': 189.68,
    'price_from': 243.86,
    'qty': 7
},
{
    'description': 'Tragelaphus strepsiceros',
    'sku': 19943993,
    'ean': 9484968356,
    'price_to': 823.43,
    'price_from': 1453.99,
    'qty': 256
},
{
    'description': 'Spermophilus parryii',
    'sku': 29172955,
    'ean': 8533538474,
    'price_to': 674.76,
    'price_from': 769.25,
    'qty': 295
},
{
    'description': 'Cracticus nigroagularis',
    'sku': 83895228,
    'ean': 3942858105,
    'price_to': 46.01,
    'price_from': 496.4,
    'qty': 182
},
{
    'description': 'Marmota caligata',
    'sku': 43389470,
    'ean': 1899709325,
    'price_to': 698.17,
    'price_from': 883.05,
    'qty': 368
},
{
    'description': 'Ratufa indica',
    'sku': 58783447,
    'ean': 8736564999,
    'price_to': 283.42,
    'price_from': 1128.3,
    'qty': 323
},
{
    'description': 'Callorhinus ursinus',
    'sku': 83519425,
    'ean': 7724330807,
    'price_to': 160.28,
    'price_from': 411.36,
    'qty': 476
},
{
    'description': 'Tadorna tadorna',
    'sku': 44797791,
    'ean': 4479890513,
    'price_to': 562.77,
    'price_from': 653.75,
    'qty': 202
},
{
    'description': 'Odocoileus hemionus',
    'sku': 48121857,
    'ean': 6316141129,
    'price_to': 88.15,
    'price_from': 166.94,
    'qty': 390
},
{
    'description': 'Haliaeetus leucoryphus',
    'sku': 45733231,
    'ean': 9993186517,
    'price_to': 633.49,
    'price_from': 908.23,
    'qty': 251
},
{
    'description': 'Manouria emys',
    'sku': 56644571,
    'ean': 2198479313,
    'price_to': 460.06,
    'price_from': 837.19,
    'qty': 414
},
{
    'description': 'Phalaropus fulicarius',
    'sku': 48083120,
    'ean': 2243530867,
    'price_to': 802.16,
    'price_from': 1670.52,
    'qty': 159
},
{
    'description': 'Colaptes campestroides',
    'sku': 42939766,
    'ean': 8203883861,
    'price_to': 351.86,
    'price_from': 1107.58,
    'qty': 111
},
{
    'description': 'Picoides pubescens',
    'sku': 32935126,
    'ean': 1185838117,
    'price_to': 730.65,
    'price_from': 845.27,
    'qty': 332
},
{
    'description': 'Lasiodora parahybana',
    'sku': 40566850,
    'ean': 2960014189,
    'price_to': 525.22,
    'price_from': 609.73,
    'qty': 388
},
{
    'description': 'Gorilla gorilla',
    'sku': 18628257,
    'ean': 9285924024,
    'price_to': 760.44,
    'price_from': 1120.16,
    'qty': 479
},
{
    'description': 'Pelecans onocratalus',
    'sku': 73233036,
    'ean': 2071761366,
    'price_to': 973.09,
    'price_from': 1730.5,
    'qty': 351
},
{
    'description': 'Melursus ursinus',
    'sku': 52575858,
    'ean': 2276528037,
    'price_to': 497.51,
    'price_from': 946.63,
    'qty': 128
},
{
    'description': 'Mycteria leucocephala',
    'sku': 78144716,
    'ean': 7397113332,
    'price_to': 599.1,
    'price_from': 1364.88,
    'qty': 399
},
{
    'description': 'Spermophilus tridecemlineatus',
    'sku': 40693257,
    'ean': 5987657770,
    'price_to': 249.79,
    'price_from': 1144.37,
    'qty': 179
},
{
    'description': 'Dendrocitta vagabunda',
    'sku': 18408701,
    'ean': 5665994847,
    'price_to': 14.51,
    'price_from': 43.2,
    'qty': 372
},
{
    'description': 'Hydrochoerus hydrochaeris',
    'sku': 12065882,
    'ean': 6050038707,
    'price_to': 408.92,
    'price_from': 840.66,
    'qty': 255
},
{
    'description': 'Alligator mississippiensis',
    'sku': 62671138,
    'ean': 3322430104,
    'price_to': 172.34,
    'price_from': 885.33,
    'qty': 354
},
{
    'description': 'Coendou prehensilis',
    'sku': 72688968,
    'ean': 3218271993,
    'price_to': 349.08,
    'price_from': 1182.1,
    'qty': 124
},
{
    'description': 'Paroaria gularis',
    'sku': 13426307,
    'ean': 2459009783,
    'price_to': 9.65,
    'price_from': 465.72,
    'qty': 452
},
{
    'description': 'Varanus sp.',
    'sku': 98176694,
    'ean': 3198146699,
    'price_to': 790.17,
    'price_from': 944.5,
    'qty': 249
},
{
    'description': 'Didelphis virginiana',
    'sku': 54532026,
    'ean': 4619058278,
    'price_to': 537.5,
    'price_from': 793.8,
    'qty': 14
},
{
    'description': 'Coluber constrictor foxii',
    'sku': 79600754,
    'ean': 9213102034,
    'price_to': 235.67,
    'price_from': 614.64,
    'qty': 299
},
{
    'description': 'Sarkidornis melanotos',
    'sku': 24599513,
    'ean': 6274276162,
    'price_to': 121.6,
    'price_from': 343.48,
    'qty': 225
},
{
    'description': 'Centrocercus urophasianus',
    'sku': 84898633,
    'ean': 7034446067,
    'price_to': 441.37,
    'price_from': 689.57,
    'qty': 18
},
{
    'description': 'Acrobates pygmaeus',
    'sku': 24172948,
    'ean': 3260895151,
    'price_to': 957.68,
    'price_from': 1909.26,
    'qty': 339
},
{
    'description': 'Felis pardalis',
    'sku': 31512728,
    'ean': 8468118413,
    'price_to': 351.4,
    'price_from': 974.27,
    'qty': 56
},
{
    'description': 'Sitta canadensis',
    'sku': 18594342,
    'ean': 4011714104,
    'price_to': 843.65,
    'price_from': 1401.47,
    'qty': 349
},
{
    'description': 'Estrilda erythronotos',
    'sku': 45467642,
    'ean': 9973419133,
    'price_to': 163.35,
    'price_from': 504.47,
    'qty': 442
},
{
    'description': 'Lutra canadensis',
    'sku': 90348875,
    'ean': 4009380498,
    'price_to': 133.02,
    'price_from': 719.27,
    'qty': 96
},
{
    'description': 'Dasypus novemcinctus',
    'sku': 19401438,
    'ean': 8707339405,
    'price_to': 674.69,
    'price_from': 1060.12,
    'qty': 439
},
{
    'description': 'unavailable',
    'sku': 61665258,
    'ean': 5101449238,
    'price_to': 634.71,
    'price_from': 822.36,
    'qty': 182
},
{
    'description': 'Ovis ammon',
    'sku': 77440960,
    'ean': 8788021188,
    'price_to': 514.92,
    'price_from': 802.28,
    'qty': 279
},
{
    'description': 'Haliaetus vocifer',
    'sku': 83973228,
    'ean': 5867337318,
    'price_to': 331.12,
    'price_from': 759.78,
    'qty': 292
},
{
    'description': 'Numida meleagris',
    'sku': 38149347,
    'ean': 7961041042,
    'price_to': 253.71,
    'price_from': 1197.19,
    'qty': 421
},
{
    'description': 'Theropithecus gelada',
    'sku': 70617498,
    'ean': 3772385878,
    'price_to': 111.36,
    'price_from': 923.02,
    'qty': 467
},
{
    'description': 'Aonyx capensis',
    'sku': 59184534,
    'ean': 7618993072,
    'price_to': 349.75,
    'price_from': 537.97,
    'qty': 387
},
{
    'description': 'Felis concolor',
    'sku': 43960557,
    'ean': 1260906960,
    'price_to': 100.94,
    'price_from': 434.44,
    'qty': 429
},
{
    'description': 'Rhea americana',
    'sku': 69163234,
    'ean': 4668453835,
    'price_to': 944.67,
    'price_from': 994.76,
    'qty': 230
},
{
    'description': 'Isoodon obesulus',
    'sku': 45308742,
    'ean': 4168067696,
    'price_to': 591.54,
    'price_from': 1000.61,
    'qty': 323
},
{
    'description': 'Lasiodora parahybana',
    'sku': 75298518,
    'ean': 4242627313,
    'price_to': 274.54,
    'price_from': 908.14,
    'qty': 344
},
{
    'description': 'Kobus defassa',
    'sku': 65921830,
    'ean': 1920293927,
    'price_to': 943.05,
    'price_from': 1531.2,
    'qty': 369
},
{
    'description': 'Psophia viridis',
    'sku': 53473400,
    'ean': 2226344055,
    'price_to': 729.39,
    'price_from': 1483.18,
    'qty': 25
},
{
    'description': 'Graspus graspus',
    'sku': 49682949,
    'ean': 2341801531,
    'price_to': 21.15,
    'price_from': 1019.39,
    'qty': 256
},
{
    'description': 'Aonyx capensis',
    'sku': 91219489,
    'ean': 9746246929,
    'price_to': 41.83,
    'price_from': 932.95,
    'qty': 113
},
{
    'description': 'Falco peregrinus',
    'sku': 64038363,
    'ean': 5031371317,
    'price_to': 712.85,
    'price_from': 760.13,
    'qty': 282
},
{
    'description': 'Ovis canadensis',
    'sku': 43740601,
    'ean': 3457817458,
    'price_to': 405.59,
    'price_from': 1078.04,
    'qty': 239
},
{
    'description': 'Odocoileus hemionus',
    'sku': 83441591,
    'ean': 1789707469,
    'price_to': 507.58,
    'price_from': 1311.4,
    'qty': 362
},
{
    'description': 'Trichoglossus haematodus moluccanus',
    'sku': 69315937,
    'ean': 6378976702,
    'price_to': 405.96,
    'price_from': 1385.02,
    'qty': 193
},
{
    'description': 'Tayassu pecari',
    'sku': 68011562,
    'ean': 8540988086,
    'price_to': 881.08,
    'price_from': 1455.28,
    'qty': 348
},
{
    'description': 'Dacelo novaeguineae',
    'sku': 12836308,
    'ean': 1126824758,
    'price_to': 268.39,
    'price_from': 1242.03,
    'qty': 44
},
{
    'description': 'Macropus agilis',
    'sku': 12010463,
    'ean': 6290254283,
    'price_to': 554.1,
    'price_from': 1524.37,
    'qty': 81
},
{
    'description': 'Vanessa indica',
    'sku': 68350514,
    'ean': 2946925677,
    'price_to': 449.67,
    'price_from': 638.96,
    'qty': 53
},
{
    'description': 'Genetta genetta',
    'sku': 79379462,
    'ean': 5442724718,
    'price_to': 968.98,
    'price_from': 1168.46,
    'qty': 423
},
{
    'description': 'Cygnus buccinator',
    'sku': 59838716,
    'ean': 3542030193,
    'price_to': 474.75,
    'price_from': 890.26,
    'qty': 264
},
{
    'description': 'Potos flavus',
    'sku': 88340360,
    'ean': 2047695217,
    'price_to': 96.57,
    'price_from': 628.7,
    'qty': 124
},
{
    'description': 'Gymnorhina tibicen',
    'sku': 15124487,
    'ean': 4260911242,
    'price_to': 395.38,
    'price_from': 1127.91,
    'qty': 443
},
{
    'description': 'Tockus erythrorhyncus',
    'sku': 28329438,
    'ean': 2233990033,
    'price_to': 614.81,
    'price_from': 1610.26,
    'qty': 364
},
{
    'description': 'Panthera pardus',
    'sku': 71622322,
    'ean': 2655293100,
    'price_to': 822.9,
    'price_from': 1496.28,
    'qty': 70
},
{
    'description': 'Paroaria gularis',
    'sku': 18025655,
    'ean': 3078602185,
    'price_to': 891.94,
    'price_from': 1411.65,
    'qty': 319
},
{
    'description': 'Agouti paca',
    'sku': 70325260,
    'ean': 8345522050,
    'price_to': 702.93,
    'price_from': 1484.92,
    'qty': 272
},
{
    'description': 'Antidorcas marsupialis',
    'sku': 49788727,
    'ean': 4151616124,
    'price_to': 747.27,
    'price_from': 1158.34,
    'qty': 326
},
{
    'description': 'Dasypus novemcinctus',
    'sku': 65043249,
    'ean': 3412295413,
    'price_to': 376.06,
    'price_from': 1090.23,
    'qty': 483
},
{
    'description': 'Nesomimus trifasciatus',
    'sku': 16231076,
    'ean': 8455879579,
    'price_to': 915.83,
    'price_from': 1788.54,
    'qty': 51
},
{
    'description': 'Ara chloroptera',
    'sku': 85483613,
    'ean': 9554757852,
    'price_to': 147.37,
    'price_from': 610.44,
    'qty': 37
},
{
    'description': 'Bugeranus caruncalatus',
    'sku': 34071208,
    'ean': 7234185938,
    'price_to': 694.27,
    'price_from': 1420.23,
    'qty': 17
},
{
    'description': 'Columba palumbus',
    'sku': 87872148,
    'ean': 4027631777,
    'price_to': 449.72,
    'price_from': 613.66,
    'qty': 483
},
{
    'description': 'Mazama gouazoubira',
    'sku': 29492360,
    'ean': 3317619248,
    'price_to': 111.23,
    'price_from': 135.27,
    'qty': 191
},
{
    'description': 'Orcinus orca',
    'sku': 70826939,
    'ean': 5704908200,
    'price_to': 62.28,
    'price_from': 425.57,
    'qty': 315
},
{
    'description': 'Tiliqua scincoides',
    'sku': 98889124,
    'ean': 7554464068,
    'price_to': 446.06,
    'price_from': 651.78,
    'qty': 80
},
{
    'description': 'Cynomys ludovicianus',
    'sku': 40245669,
    'ean': 1894233959,
    'price_to': 411.81,
    'price_from': 468.31,
    'qty': 168
},
{
    'description': 'Isoodon obesulus',
    'sku': 71579270,
    'ean': 5205035028,
    'price_to': 848.01,
    'price_from': 1282.78,
    'qty': 349
},
{
    'description': 'Drymarchon corias couperi',
    'sku': 50417689,
    'ean': 7637524753,
    'price_to': 52.74,
    'price_from': 684.83,
    'qty': 285
},
{
    'description': 'Creagrus furcatus',
    'sku': 24387134,
    'ean': 1262958894,
    'price_to': 277.18,
    'price_from': 1212.73,
    'qty': 153
},
{
    'description': 'Acrobates pygmaeus',
    'sku': 81412036,
    'ean': 7519360387,
    'price_to': 246.56,
    'price_from': 768.34,
    'qty': 198
},
{
    'description': 'Drymarchon corias couperi',
    'sku': 62530150,
    'ean': 8479579856,
    'price_to': 595.02,
    'price_from': 1116.41,
    'qty': 465
},
{
    'description': 'Meleagris gallopavo',
    'sku': 57870327,
    'ean': 3062199465,
    'price_to': 435.71,
    'price_from': 1299.18,
    'qty': 303
},
{
    'description': 'Climacteris melanura',
    'sku': 27254237,
    'ean': 5255965596,
    'price_to': 811.53,
    'price_from': 1169.79,
    'qty': 404
},
{
    'description': 'Hystrix cristata',
    'sku': 62667273,
    'ean': 9592579815,
    'price_to': 310.88,
    'price_from': 359.35,
    'qty': 390
},
{
    'description': 'Snycerus caffer',
    'sku': 24016417,
    'ean': 8715443543,
    'price_to': 225.41,
    'price_from': 1069.79,
    'qty': 131
},
{
    'description': 'Dendrocygna viduata',
    'sku': 97212246,
    'ean': 5617448613,
    'price_to': 577.93,
    'price_from': 1280.4,
    'qty': 432
},
{
    'description': 'Tragelaphus scriptus',
    'sku': 89736590,
    'ean': 2927000054,
    'price_to': 691.64,
    'price_from': 1445.29,
    'qty': 185
},
{
    'description': 'Paradoxurus hermaphroditus',
    'sku': 23130189,
    'ean': 2194818101,
    'price_to': 448.94,
    'price_from': 727.01,
    'qty': 313
},
{
    'description': 'Madoqua kirkii',
    'sku': 41983167,
    'ean': 8956918703,
    'price_to': 416.28,
    'price_from': 662.64,
    'qty': 24
},
{
    'description': 'Notechis semmiannulatus',
    'sku': 26137946,
    'ean': 4928393177,
    'price_to': 526.75,
    'price_from': 835.46,
    'qty': 52
},
{
    'description': 'Canis aureus',
    'sku': 48919117,
    'ean': 4109216065,
    'price_to': 813.29,
    'price_from': 1165.39,
    'qty': 353
},
{
    'description': 'Terrapene carolina',
    'sku': 71832466,
    'ean': 8620610809,
    'price_to': 629.55,
    'price_from': 1136.88,
    'qty': 104
},
{
    'description': 'Phoca vitulina',
    'sku': 29469491,
    'ean': 2487820746,
    'price_to': 92.2,
    'price_from': 1032.33,
    'qty': 269
},
{
    'description': 'Zenaida asiatica',
    'sku': 63886432,
    'ean': 6384160747,
    'price_to': 682.72,
    'price_from': 771.95,
    'qty': 153
},
{
    'description': 'Hippopotamus amphibius',
    'sku': 42658138,
    'ean': 5091307508,
    'price_to': 392.67,
    'price_from': 1235.73,
    'qty': 367
},
{
    'description': 'Rhea americana',
    'sku': 90315613,
    'ean': 2280034402,
    'price_to': 360.45,
    'price_from': 489.65,
    'qty': 210
},
{
    'description': 'Anhinga rufa',
    'sku': 73115742,
    'ean': 9948061616,
    'price_to': 136.76,
    'price_from': 878.28,
    'qty': 23
},
{
    'description': 'Eira barbata',
    'sku': 27906892,
    'ean': 9192641351,
    'price_to': 567.04,
    'price_from': 1139.89,
    'qty': 427
},
{
    'description': 'Larus sp.',
    'sku': 44631494,
    'ean': 6154054049,
    'price_to': 212.84,
    'price_from': 815.46,
    'qty': 252
},
{
    'description': 'Neotis denhami',
    'sku': 11900481,
    'ean': 4848579414,
    'price_to': 971.69,
    'price_from': 1449.0,
    'qty': 394
},
{
    'description': 'Cynictis penicillata',
    'sku': 72142033,
    'ean': 7725602985,
    'price_to': 68.58,
    'price_from': 373.05,
    'qty': 485
},
{
    'description': 'Anastomus oscitans',
    'sku': 39082957,
    'ean': 8136470267,
    'price_to': 764.25,
    'price_from': 821.04,
    'qty': 362
},
{
    'description': 'Tragelaphus strepsiceros',
    'sku': 76181108,
    'ean': 7710822638,
    'price_to': 598.27,
    'price_from': 1003.92,
    'qty': 302
},
{
    'description': 'Nyctanassa violacea',
    'sku': 73969818,
    'ean': 6155376696,
    'price_to': 623.44,
    'price_from': 846.52,
    'qty': 223
},
{
    'description': 'Cacatua tenuirostris',
    'sku': 17340776,
    'ean': 9783417766,
    'price_to': 946.21,
    'price_from': 1854.37,
    'qty': 267
},
{
    'description': 'Egretta thula',
    'sku': 72421045,
    'ean': 4661422097,
    'price_to': 480.53,
    'price_from': 1089.33,
    'qty': 17
},
{
    'description': 'Thamnolaea cinnmomeiventris',
    'sku': 55492708,
    'ean': 6846474141,
    'price_to': 897.38,
    'price_from': 1016.2,
    'qty': 410
},
{
    'description': 'Equus burchelli',
    'sku': 92488598,
    'ean': 6076583230,
    'price_to': 570.03,
    'price_from': 719.67,
    'qty': 303
},
{
    'description': 'Colobus guerza',
    'sku': 63410006,
    'ean': 4401640994,
    'price_to': 911.65,
    'price_from': 1499.94,
    'qty': 115
},
{
    'description': 'Lepus townsendii',
    'sku': 37253744,
    'ean': 5492467020,
    'price_to': 953.45,
    'price_from': 1595.88,
    'qty': 89
},
{
    'description': 'Passer domesticus',
    'sku': 20150859,
    'ean': 6385213245,
    'price_to': 638.73,
    'price_from': 807.77,
    'qty': 289
},
{
    'description': 'Salvadora hexalepis',
    'sku': 54990909,
    'ean': 5782578034,
    'price_to': 24.29,
    'price_from': 408.67,
    'qty': 344
},
{
    'description': 'Hymenolaimus malacorhynchus',
    'sku': 43075287,
    'ean': 9663408541,
    'price_to': 473.74,
    'price_from': 1082.32,
    'qty': 269
},
{
    'description': 'Castor fiber',
    'sku': 29057689,
    'ean': 3017351494,
    'price_to': 220.97,
    'price_from': 297.32,
    'qty': 102
},
{
    'description': 'Genetta genetta',
    'sku': 39826000,
    'ean': 9989466049,
    'price_to': 479.05,
    'price_from': 1437.96,
    'qty': 500
},
{
    'description': 'Tachyglossus aculeatus',
    'sku': 19958505,
    'ean': 8488975004,
    'price_to': 16.25,
    'price_from': 54.85,
    'qty': 208
},
{
    'description': 'Phoenicopterus ruber',
    'sku': 64949565,
    'ean': 9880179470,
    'price_to': 642.48,
    'price_from': 1073.69,
    'qty': 409
},
{
    'description': 'Antidorcas marsupialis',
    'sku': 36651091,
    'ean': 6113471018,
    'price_to': 889.71,
    'price_from': 1561.46,
    'qty': 111
},
{
    'description': 'Coluber constrictor',
    'sku': 61038916,
    'ean': 6426131559,
    'price_to': 792.87,
    'price_from': 1646.05,
    'qty': 21
},
{
    'description': 'Isoodon obesulus',
    'sku': 64532726,
    'ean': 6479010989,
    'price_to': 709.55,
    'price_from': 1305.38,
    'qty': 305
},
{
    'description': 'Paraxerus cepapi',
    'sku': 67405301,
    'ean': 7000385418,
    'price_to': 761.6,
    'price_from': 1731.4,
    'qty': 140
},
{
    'description': 'Semnopithecus entellus',
    'sku': 77474022,
    'ean': 2830005895,
    'price_to': 829.15,
    'price_from': 918.89,
    'qty': 104
},
{
    'description': 'Nucifraga columbiana',
    'sku': 12550387,
    'ean': 8757647689,
    'price_to': 993.27,
    'price_from': 1948.19,
    'qty': 109
},
{
    'description': 'Butorides striatus',
    'sku': 20126332,
    'ean': 4151779658,
    'price_to': 570.58,
    'price_from': 1091.54,
    'qty': 414
},
{
    'description': 'Bison bison',
    'sku': 77779917,
    'ean': 4026465129,
    'price_to': 807.77,
    'price_from': 1575.39,
    'qty': 328
},
{
    'description': 'Egretta thula',
    'sku': 71456201,
    'ean': 8976608005,
    'price_to': 341.8,
    'price_from': 511.76,
    'qty': 105
},
{
    'description': 'Macropus eugenii',
    'sku': 84919617,
    'ean': 6305680243,
    'price_to': 699.94,
    'price_from': 1313.64,
    'qty': 428
},
{
    'description': 'Paraxerus cepapi',
    'sku': 49404168,
    'ean': 9844904520,
    'price_to': 811.6,
    'price_from': 1298.24,
    'qty': 261
},
{
    'description': 'Cebus apella',
    'sku': 96807722,
    'ean': 7405052687,
    'price_to': 899.7,
    'price_from': 1130.27,
    'qty': 267
},
{
    'description': 'Lepus arcticus',
    'sku': 21893573,
    'ean': 7704581927,
    'price_to': 989.7,
    'price_from': 1536.2,
    'qty': 65
},
{
    'description': 'Ceratotherium simum',
    'sku': 23020837,
    'ean': 4451208449,
    'price_to': 398.77,
    'price_from': 426.8,
    'qty': 373
},
{
    'description': 'Limnocorax flavirostra',
    'sku': 73559023,
    'ean': 9763523517,
    'price_to': 779.78,
    'price_from': 1106.88,
    'qty': 95
},
{
    'description': 'Oncorhynchus nerka',
    'sku': 46858185,
    'ean': 9960922570,
    'price_to': 678.86,
    'price_from': 1404.02,
    'qty': 420
},
{
    'description': 'Eudyptula minor',
    'sku': 60130909,
    'ean': 5001175512,
    'price_to': 755.16,
    'price_from': 1147.95,
    'qty': 118
},
{
    'description': 'Macropus giganteus',
    'sku': 23397524,
    'ean': 2065931077,
    'price_to': 875.14,
    'price_from': 905.97,
    'qty': 2
},
{
    'description': 'Haematopus ater',
    'sku': 19347836,
    'ean': 4818654706,
    'price_to': 796.58,
    'price_from': 1448.77,
    'qty': 14
},
{
    'description': 'Vulpes cinereoargenteus',
    'sku': 64966240,
    'ean': 2189438812,
    'price_to': 167.89,
    'price_from': 847.84,
    'qty': 301
},
{
    'description': 'Phalaropus fulicarius',
    'sku': 52238050,
    'ean': 1674864838,
    'price_to': 719.88,
    'price_from': 1420.98,
    'qty': 328
},
{
    'description': 'Eudyptula minor',
    'sku': 38800549,
    'ean': 8126443592,
    'price_to': 59.58,
    'price_from': 120.5,
    'qty': 7
},
{
    'description': 'Petaurus breviceps',
    'sku': 63599198,
    'ean': 3391053023,
    'price_to': 567.65,
    'price_from': 787.7,
    'qty': 167
},
{
    'description': 'Dasypus novemcinctus',
    'sku': 48701077,
    'ean': 2805250915,
    'price_to': 743.48,
    'price_from': 1387.92,
    'qty': 268
},
{
    'description': 'Crotalus cerastes',
    'sku': 15897891,
    'ean': 5011443775,
    'price_to': 774.64,
    'price_from': 1577.31,
    'qty': 270
},
{
    'description': 'Butorides striatus',
    'sku': 43888574,
    'ean': 6980944458,
    'price_to': 389.47,
    'price_from': 651.01,
    'qty': 175
},
{
    'description': 'Phalacrocorax niger',
    'sku': 52601674,
    'ean': 2399320383,
    'price_to': 721.29,
    'price_from': 1385.91,
    'qty': 395
},
{
    'description': 'Alcelaphus buselaphus caama',
    'sku': 50425623,
    'ean': 2282395021,
    'price_to': 494.28,
    'price_from': 1187.7,
    'qty': 19
},
{
    'description': 'Gazella thompsonii',
    'sku': 44486569,
    'ean': 3712787311,
    'price_to': 426.34,
    'price_from': 1420.8,
    'qty': 121
},
{
    'description': 'Macropus fuliginosus',
    'sku': 23047778,
    'ean': 3777975967,
    'price_to': 664.72,
    'price_from': 1265.87,
    'qty': 193
},
{
    'description': 'Anhinga rufa',
    'sku': 94113992,
    'ean': 9757037306,
    'price_to': 859.43,
    'price_from': 1138.1,
    'qty': 56
},
{
    'description': 'Capreolus capreolus',
    'sku': 43721391,
    'ean': 8228487559,
    'price_to': 557.12,
    'price_from': 1191.93,
    'qty': 255
},
{
    'description': 'Ammospermophilus nelsoni',
    'sku': 78428685,
    'ean': 5330571947,
    'price_to': 784.33,
    'price_from': 1321.03,
    'qty': 133
},
{
    'description': 'Eumetopias jubatus',
    'sku': 86162294,
    'ean': 9865168826,
    'price_to': 703.74,
    'price_from': 815.55,
    'qty': 14
},
{
    'description': 'Manouria emys',
    'sku': 42775254,
    'ean': 1717507543,
    'price_to': 908.48,
    'price_from': 1239.79,
    'qty': 263
},
{
    'description': 'Genetta genetta',
    'sku': 21491481,
    'ean': 7907950829,
    'price_to': 803.56,
    'price_from': 1474.03,
    'qty': 408
},
{
    'description': 'Ardea golieth',
    'sku': 79910885,
    'ean': 4961542098,
    'price_to': 536.71,
    'price_from': 1443.58,
    'qty': 284
},
{
    'description': 'Bassariscus astutus',
    'sku': 52080503,
    'ean': 4762550539,
    'price_to': 595.89,
    'price_from': 900.54,
    'qty': 189
},
{
    'description': 'Felis wiedi or Leopardus weidi',
    'sku': 57136927,
    'ean': 7391075931,
    'price_to': 876.89,
    'price_from': 1820.0,
    'qty': 139
},
{
    'description': 'Creagrus furcatus',
    'sku': 43674369,
    'ean': 4150526135,
    'price_to': 743.35,
    'price_from': 1160.59,
    'qty': 150
},
{
    'description': 'Sceloporus magister',
    'sku': 82351046,
    'ean': 4934074745,
    'price_to': 734.66,
    'price_from': 1038.08,
    'qty': 266
},
{
    'description': 'Capreolus capreolus',
    'sku': 74341086,
    'ean': 5664464903,
    'price_to': 889.42,
    'price_from': 1259.16,
    'qty': 178
},
{
    'description': 'Choloepus hoffmani',
    'sku': 91497307,
    'ean': 4153404474,
    'price_to': 38.17,
    'price_from': 314.6,
    'qty': 206
},
{
    'description': 'Priodontes maximus',
    'sku': 87777559,
    'ean': 4382122929,
    'price_to': 785.24,
    'price_from': 1556.45,
    'qty': 232
},
{
    'description': 'Kobus leche robertsi',
    'sku': 97932600,
    'ean': 9330865715,
    'price_to': 8.3,
    'price_from': 182.73,
    'qty': 269
},
{
    'description': 'Isoodon obesulus',
    'sku': 65840807,
    'ean': 8333462230,
    'price_to': 864.08,
    'price_from': 1458.11,
    'qty': 132
},
{
    'description': 'Cercopithecus aethiops',
    'sku': 40062602,
    'ean': 6334784829,
    'price_to': 333.27,
    'price_from': 1063.9,
    'qty': 184
},
{
    'description': 'Kobus defassa',
    'sku': 36813787,
    'ean': 8094349635,
    'price_to': 407.16,
    'price_from': 551.9,
    'qty': 32
},
{
    'description': 'Ardea golieth',
    'sku': 16500271,
    'ean': 3580304118,
    'price_to': 453.39,
    'price_from': 516.18,
    'qty': 463
},
{
    'description': 'Melophus lathami',
    'sku': 48329709,
    'ean': 9431512608,
    'price_to': 733.34,
    'price_from': 1066.41,
    'qty': 42
},
{
    'description': 'Ceryle rudis',
    'sku': 37311063,
    'ean': 6508542323,
    'price_to': 220.74,
    'price_from': 807.14,
    'qty': 111
},
{
    'description': 'Tachybaptus ruficollis',
    'sku': 67145133,
    'ean': 5751944283,
    'price_to': 315.69,
    'price_from': 733.17,
    'qty': 235
},
{
    'description': 'Canis aureus',
    'sku': 52446319,
    'ean': 7766015190,
    'price_to': 2.61,
    'price_from': 241.01,
    'qty': 31
},
{
    'description': 'Pelecans onocratalus',
    'sku': 63107081,
    'ean': 2555082581,
    'price_to': 758.42,
    'price_from': 1443.25,
    'qty': 125
},
{
    'description': 'Pitangus sulphuratus',
    'sku': 58933729,
    'ean': 9567200756,
    'price_to': 723.56,
    'price_from': 958.64,
    'qty': 272
},
{
    'description': 'Choloepus hoffmani',
    'sku': 67118514,
    'ean': 7204922836,
    'price_to': 548.39,
    'price_from': 933.47,
    'qty': 37
},
{
    'description': 'unavailable',
    'sku': 47561598,
    'ean': 1910330444,
    'price_to': 814.69,
    'price_from': 1664.29,
    'qty': 442
},
{
    'description': 'Sylvilagus floridanus',
    'sku': 74310320,
    'ean': 1633840454,
    'price_to': 427.46,
    'price_from': 692.65,
    'qty': 364
},
{
    'description': 'Eumetopias jubatus',
    'sku': 88673773,
    'ean': 6650869232,
    'price_to': 924.79,
    'price_from': 1508.03,
    'qty': 262
},
{
    'description': 'Acrobates pygmaeus',
    'sku': 99889311,
    'ean': 9213254116,
    'price_to': 160.59,
    'price_from': 523.31,
    'qty': 464
},
{
    'description': 'Macropus fuliginosus',
    'sku': 14370844,
    'ean': 3277274905,
    'price_to': 747.12,
    'price_from': 1590.82,
    'qty': 108
},
{
    'description': 'Hyaena brunnea',
    'sku': 62816283,
    'ean': 9466935567,
    'price_to': 147.39,
    'price_from': 530.63,
    'qty': 237
},
{
    'description': 'Macropus eugenii',
    'sku': 18846036,
    'ean': 8530782905,
    'price_to': 62.61,
    'price_from': 430.1,
    'qty': 145
},
{
    'description': 'Nyctea scandiaca',
    'sku': 88382500,
    'ean': 2863571161,
    'price_to': 137.83,
    'price_from': 1113.08,
    'qty': 401
},
{
    'description': 'Merops bullockoides',
    'sku': 67881589,
    'ean': 9123894847,
    'price_to': 804.58,
    'price_from': 1587.63,
    'qty': 255
},
{
    'description': 'Colobus guerza',
    'sku': 73909581,
    'ean': 6936412363,
    'price_to': 162.13,
    'price_from': 626.89,
    'qty': 316
},
{
    'description': 'Graspus graspus',
    'sku': 20272655,
    'ean': 5876652266,
    'price_to': 911.3,
    'price_from': 1482.75,
    'qty': 95
},
{
    'description': 'Cynomys ludovicianus',
    'sku': 48935191,
    'ean': 2155673812,
    'price_to': 900.43,
    'price_from': 1040.09,
    'qty': 15
},
{
    'description': 'Dusicyon thous',
    'sku': 26066573,
    'ean': 4397195348,
    'price_to': 811.04,
    'price_from': 946.65,
    'qty': 247
},
{
    'description': 'Tamiasciurus hudsonicus',
    'sku': 98452845,
    'ean': 9112136712,
    'price_to': 258.31,
    'price_from': 632.8,
    'qty': 387
},
{
    'description': 'Chauna torquata',
    'sku': 39521219,
    'ean': 8614756134,
    'price_to': 319.31,
    'price_from': 1247.15,
    'qty': 453
},
{
    'description': 'Choriotis kori',
    'sku': 48225068,
    'ean': 1640826400,
    'price_to': 228.68,
    'price_from': 911.15,
    'qty': 20
},
{
    'description': 'Corvus albicollis',
    'sku': 73355560,
    'ean': 5934032635,
    'price_to': 301.46,
    'price_from': 1046.33,
    'qty': 281
},
{
    'description': 'Neotoma sp.',
    'sku': 88794430,
    'ean': 7340885556,
    'price_to': 724.53,
    'price_from': 1153.02,
    'qty': 333
},
{
    'description': 'Tauraco porphyrelophus',
    'sku': 45463992,
    'ean': 5606173122,
    'price_to': 404.81,
    'price_from': 701.68,
    'qty': 166
},
{
    'description': 'Tenrec ecaudatus',
    'sku': 36839860,
    'ean': 5796354986,
    'price_to': 832.21,
    'price_from': 952.59,
    'qty': 199
},
{
    'description': 'Nesomimus trifasciatus',
    'sku': 89007998,
    'ean': 9183179617,
    'price_to': 108.11,
    'price_from': 931.68,
    'qty': 265
},
{
    'description': 'Centrocercus urophasianus',
    'sku': 33627265,
    'ean': 7711359006,
    'price_to': 108.62,
    'price_from': 856.05,
    'qty': 236
},
{
    'description': 'Cathartes aura',
    'sku': 91991196,
    'ean': 4279966855,
    'price_to': 554.23,
    'price_from': 1184.77,
    'qty': 477
},
{
    'description': 'Canis mesomelas',
    'sku': 59555446,
    'ean': 2317284590,
    'price_to': 113.97,
    'price_from': 891.03,
    'qty': 102
},
{
    'description': 'Phoeniconaias minor',
    'sku': 32248144,
    'ean': 5367313005,
    'price_to': 564.45,
    'price_from': 862.24,
    'qty': 419
},
{
    'description': 'Milvago chimachima',
    'sku': 48147865,
    'ean': 6104561316,
    'price_to': 275.31,
    'price_from': 317.92,
    'qty': 67
},
{
    'description': 'Mabuya spilogaster',
    'sku': 55418081,
    'ean': 7674448406,
    'price_to': 952.11,
    'price_from': 1503.12,
    'qty': 232
},
{
    'description': 'Martes pennanti',
    'sku': 28849124,
    'ean': 9180478590,
    'price_to': 876.04,
    'price_from': 1628.89,
    'qty': 200
},
{
    'description': 'Cervus elaphus',
    'sku': 85550807,
    'ean': 6781248539,
    'price_to': 430.42,
    'price_from': 1405.01,
    'qty': 29
},
{
    'description': 'Sula dactylatra',
    'sku': 48220227,
    'ean': 3312315570,
    'price_to': 330.43,
    'price_from': 1326.86,
    'qty': 82
},
{
    'description': 'Manouria emys',
    'sku': 28374648,
    'ean': 2988538211,
    'price_to': 858.33,
    'price_from': 1475.05,
    'qty': 44
},
{
    'description': 'Dacelo novaeguineae',
    'sku': 97667443,
    'ean': 7408498059,
    'price_to': 987.22,
    'price_from': 1416.16,
    'qty': 490
},
{
    'description': 'Papio cynocephalus',
    'sku': 57615567,
    'ean': 5102605143,
    'price_to': 814.55,
    'price_from': 1064.64,
    'qty': 197
},
{
    'description': 'Anastomus oscitans',
    'sku': 11138786,
    'ean': 5316815685,
    'price_to': 405.16,
    'price_from': 1286.85,
    'qty': 254
},
{
    'description': 'Connochaetus taurinus',
    'sku': 28047992,
    'ean': 2360976883,
    'price_to': 787.99,
    'price_from': 1737.75,
    'qty': 246
},
{
    'description': 'Bucephala clangula',
    'sku': 52143105,
    'ean': 2048745695,
    'price_to': 508.33,
    'price_from': 1451.88,
    'qty': 210
},
{
    'description': 'Eumetopias jubatus',
    'sku': 97145181,
    'ean': 9131164564,
    'price_to': 462.3,
    'price_from': 1456.36,
    'qty': 213
},
{
    'description': 'Aquila chrysaetos',
    'sku': 46881236,
    'ean': 7275112656,
    'price_to': 408.01,
    'price_from': 436.02,
    'qty': 430
},
{
    'description': 'Physignathus cocincinus',
    'sku': 52792789,
    'ean': 5717074985,
    'price_to': 710.63,
    'price_from': 997.7,
    'qty': 44
},
{
    'description': 'Uraeginthus angolensis',
    'sku': 72698212,
    'ean': 9105759454,
    'price_to': 631.88,
    'price_from': 1524.31,
    'qty': 254
},
{
    'description': 'Neotis denhami',
    'sku': 76274671,
    'ean': 3936669824,
    'price_to': 204.54,
    'price_from': 971.32,
    'qty': 411
},
{
    'description': 'Pteronura brasiliensis',
    'sku': 39874720,
    'ean': 7213106774,
    'price_to': 660.81,
    'price_from': 1486.87,
    'qty': 123
},
{
    'description': 'Ardea golieth',
    'sku': 45286847,
    'ean': 9340704267,
    'price_to': 422.57,
    'price_from': 1120.91,
    'qty': 57
},
{
    'description': 'Eudromia elegans',
    'sku': 64680102,
    'ean': 7190843915,
    'price_to': 79.85,
    'price_from': 518.1,
    'qty': 82
},
{
    'description': 'Coluber constrictor',
    'sku': 79951569,
    'ean': 6257596246,
    'price_to': 452.37,
    'price_from': 670.17,
    'qty': 260
},
{
    'description': 'Pteronura brasiliensis',
    'sku': 35971962,
    'ean': 2838829287,
    'price_to': 723.75,
    'price_from': 915.89,
    'qty': 169
},
{
    'description': 'Mirounga angustirostris',
    'sku': 97405050,
    'ean': 5539928398,
    'price_to': 110.2,
    'price_from': 684.03,
    'qty': 137
},
{
    'description': 'Paroaria gularis',
    'sku': 18386774,
    'ean': 7339911807,
    'price_to': 584.48,
    'price_from': 996.21,
    'qty': 288
},
{
    'description': 'Columba livia',
    'sku': 79154919,
    'ean': 1775653755,
    'price_to': 527.36,
    'price_from': 950.43,
    'qty': 323
},
{
    'description': 'Francolinus leucoscepus',
    'sku': 83735035,
    'ean': 5993747024,
    'price_to': 264.37,
    'price_from': 431.53,
    'qty': 410
},
{
    'description': 'Crotalus cerastes',
    'sku': 15844176,
    'ean': 9886044268,
    'price_to': 738.26,
    'price_from': 1409.01,
    'qty': 152
},
{
    'description': 'Upupa epops',
    'sku': 57823419,
    'ean': 6872876554,
    'price_to': 875.15,
    'price_from': 901.31,
    'qty': 354
},
{
    'description': 'Speothos vanaticus',
    'sku': 99624121,
    'ean': 5795332013,
    'price_to': 320.15,
    'price_from': 699.42,
    'qty': 403
},
{
    'description': 'Ara chloroptera',
    'sku': 43452036,
    'ean': 5924981922,
    'price_to': 314.21,
    'price_from': 1232.52,
    'qty': 414
},
{
    'description': 'Nucifraga columbiana',
    'sku': 58765299,
    'ean': 9037695760,
    'price_to': 61.93,
    'price_from': 723.64,
    'qty': 109
},
{
    'description': 'Funambulus pennati',
    'sku': 25747352,
    'ean': 1755744433,
    'price_to': 221.26,
    'price_from': 1022.67,
    'qty': 307
},
{
    'description': 'Canis aureus',
    'sku': 38393045,
    'ean': 2289083488,
    'price_to': 734.52,
    'price_from': 795.94,
    'qty': 89
},
{
    'description': 'Bucorvus leadbeateri',
    'sku': 60029030,
    'ean': 9627333897,
    'price_to': 441.75,
    'price_from': 528.55,
    'qty': 290
},
{
    'description': 'Coracias caudata',
    'sku': 35706984,
    'ean': 2337209485,
    'price_to': 439.39,
    'price_from': 820.87,
    'qty': 379
},
{
    'description': 'Eremophila alpestris',
    'sku': 66575088,
    'ean': 1262522707,
    'price_to': 883.28,
    'price_from': 1689.61,
    'qty': 452
},
{
    'description': 'Acridotheres tristis',
    'sku': 63262239,
    'ean': 5784327444,
    'price_to': 150.83,
    'price_from': 541.67,
    'qty': 453
},
{
    'description': 'Macaca fuscata',
    'sku': 69796916,
    'ean': 5332626498,
    'price_to': 875.27,
    'price_from': 1044.34,
    'qty': 475
},
{
    'description': 'Didelphis virginiana',
    'sku': 46473241,
    'ean': 8901221190,
    'price_to': 841.43,
    'price_from': 1582.47,
    'qty': 432
},
{
    'description': 'Scolopax minor',
    'sku': 60962550,
    'ean': 4730309323,
    'price_to': 811.01,
    'price_from': 1027.37,
    'qty': 101
},
{
    'description': 'Morelia spilotes variegata',
    'sku': 78249840,
    'ean': 8166238952,
    'price_to': 366.14,
    'price_from': 512.77,
    'qty': 33
},
{
    'description': 'Paradoxurus hermaphroditus',
    'sku': 48226907,
    'ean': 5991044633,
    'price_to': 490.94,
    'price_from': 922.01,
    'qty': 408
},
{
    'description': 'Pseudalopex gymnocercus',
    'sku': 21741084,
    'ean': 8826166115,
    'price_to': 874.7,
    'price_from': 1771.55,
    'qty': 362
},
{
    'description': 'Anitibyx armatus',
    'sku': 30572626,
    'ean': 3283440339,
    'price_to': 989.46,
    'price_from': 1746.81,
    'qty': 454
},
{
    'description': 'Odocoileus hemionus',
    'sku': 88872197,
    'ean': 4575956468,
    'price_to': 695.88,
    'price_from': 1491.65,
    'qty': 192
},
{
    'description': 'Cebus apella',
    'sku': 97161606,
    'ean': 9752602145,
    'price_to': 345.96,
    'price_from': 1243.99,
    'qty': 357
},
{
    'description': 'Tyto novaehollandiae',
    'sku': 37040313,
    'ean': 6006168324,
    'price_to': 780.44,
    'price_from': 1092.63,
    'qty': 177
},
{
    'description': 'Dasyurus viverrinus',
    'sku': 63772460,
    'ean': 6980394133,
    'price_to': 720.26,
    'price_from': 1280.51,
    'qty': 131
},
{
    'description': 'Manouria emys',
    'sku': 54343289,
    'ean': 9721323448,
    'price_to': 587.18,
    'price_from': 1452.42,
    'qty': 218
},
{
    'description': 'Lophoaetus occipitalis',
    'sku': 18734944,
    'ean': 5814350687,
    'price_to': 413.56,
    'price_from': 604.6,
    'qty': 386
},
{
    'description': 'Alopochen aegyptiacus',
    'sku': 85657999,
    'ean': 7389698719,
    'price_to': 166.03,
    'price_from': 195.15,
    'qty': 59
},
{
    'description': 'Cebus nigrivittatus',
    'sku': 46223690,
    'ean': 3978438587,
    'price_to': 263.43,
    'price_from': 1198.8,
    'qty': 58
},
{
    'description': 'Speothos vanaticus',
    'sku': 53656355,
    'ean': 5640880919,
    'price_to': 391.13,
    'price_from': 1355.47,
    'qty': 180
},
{
    'description': 'Ctenophorus ornatus',
    'sku': 23304394,
    'ean': 4493332817,
    'price_to': 657.5,
    'price_from': 787.21,
    'qty': 321
},
{
    'description': 'Oncorhynchus nerka',
    'sku': 95793803,
    'ean': 6412201560,
    'price_to': 190.79,
    'price_from': 549.4,
    'qty': 211
},
{
    'description': 'Canis mesomelas',
    'sku': 24814737,
    'ean': 1519415713,
    'price_to': 291.65,
    'price_from': 1030.84,
    'qty': 113
},
{
    'description': 'Butorides striatus',
    'sku': 49362409,
    'ean': 8919807097,
    'price_to': 811.53,
    'price_from': 1154.5,
    'qty': 435
},
{
    'description': 'Ramphastos tucanus',
    'sku': 84864706,
    'ean': 4143523597,
    'price_to': 394.74,
    'price_from': 679.33,
    'qty': 480
},
{
    'description': 'Felis silvestris lybica',
    'sku': 37341092,
    'ean': 4562958454,
    'price_to': 610.79,
    'price_from': 804.78,
    'qty': 103
},
{
    'description': 'Macropus agilis',
    'sku': 29998469,
    'ean': 4622761375,
    'price_to': 250.66,
    'price_from': 274.32,
    'qty': 111
},
{
    'description': 'Papio cynocephalus',
    'sku': 17788299,
    'ean': 9084612181,
    'price_to': 606.17,
    'price_from': 689.6,
    'qty': 469
},
{
    'description': 'Phalaropus fulicarius',
    'sku': 32748734,
    'ean': 5399462803,
    'price_to': 56.42,
    'price_from': 938.62,
    'qty': 48
},
{
    'description': 'Macropus robustus',
    'sku': 32934346,
    'ean': 4930271749,
    'price_to': 772.39,
    'price_from': 788.0,
    'qty': 275
},
{
    'description': 'Paradoxurus hermaphroditus',
    'sku': 24652839,
    'ean': 9823743055,
    'price_to': 24.22,
    'price_from': 243.47,
    'qty': 55
},
{
    'description': 'Francolinus swainsonii',
    'sku': 51071966,
    'ean': 3393059186,
    'price_to': 620.37,
    'price_from': 1546.37,
    'qty': 402
},
{
    'description': 'Rhea americana',
    'sku': 29054705,
    'ean': 7369793603,
    'price_to': 65.08,
    'price_from': 176.9,
    'qty': 483
},
{
    'description': 'Climacteris melanura',
    'sku': 60182361,
    'ean': 7332450563,
    'price_to': 885.58,
    'price_from': 1500.86,
    'qty': 13
},
{
    'description': 'Bos taurus',
    'sku': 99290209,
    'ean': 7579575333,
    'price_to': 410.25,
    'price_from': 1227.8,
    'qty': 421
},
{
    'description': 'Vanellus sp.',
    'sku': 86760537,
    'ean': 8117579727,
    'price_to': 644.26,
    'price_from': 846.2,
    'qty': 252
},
{
    'description': 'Melanerpes erythrocephalus',
    'sku': 14998691,
    'ean': 1247464255,
    'price_to': 188.99,
    'price_from': 339.0,
    'qty': 71
},
{
    'description': 'Macropus agilis',
    'sku': 19147063,
    'ean': 4736815381,
    'price_to': 268.89,
    'price_from': 612.46,
    'qty': 54
},
{
    'description': 'Streptopelia senegalensis',
    'sku': 66973107,
    'ean': 6922588919,
    'price_to': 290.3,
    'price_from': 581.96,
    'qty': 196
},
{
    'description': 'Coluber constrictor foxii',
    'sku': 22102587,
    'ean': 2052976584,
    'price_to': 808.17,
    'price_from': 1452.47,
    'qty': 161
},
{
    'description': 'Leipoa ocellata',
    'sku': 76518851,
    'ean': 7451809157,
    'price_to': 294.06,
    'price_from': 605.56,
    'qty': 200
},
{
    'description': 'Felis serval',
    'sku': 95215528,
    'ean': 4920443889,
    'price_to': 915.99,
    'price_from': 1349.8,
    'qty': 218
},
{
    'description': 'Lasiorhinus latifrons',
    'sku': 90849139,
    'ean': 2171852958,
    'price_to': 911.43,
    'price_from': 1005.31,
    'qty': 208
},
{
    'description': 'Smithopsis crassicaudata',
    'sku': 43706145,
    'ean': 2933444841,
    'price_to': 783.79,
    'price_from': 1670.12,
    'qty': 412
},
{
    'description': 'Sylvicapra grimma',
    'sku': 85582622,
    'ean': 5356838469,
    'price_to': 848.33,
    'price_from': 1014.85,
    'qty': 165
},
{
    'description': 'Gyps bengalensis',
    'sku': 63831590,
    'ean': 7431700570,
    'price_to': 930.87,
    'price_from': 1497.6,
    'qty': 53
},
{
    'description': 'Felis silvestris lybica',
    'sku': 56587767,
    'ean': 9886482814,
    'price_to': 540.54,
    'price_from': 1349.26,
    'qty': 171
},
{
    'description': 'Anhinga rufa',
    'sku': 42728040,
    'ean': 3778328462,
    'price_to': 811.12,
    'price_from': 1323.55,
    'qty': 355
},
{
    'description': 'Cordylus giganteus',
    'sku': 62661269,
    'ean': 3433087917,
    'price_to': 293.58,
    'price_from': 1152.8,
    'qty': 40
},
{
    'description': 'Graspus graspus',
    'sku': 88226799,
    'ean': 5071001990,
    'price_to': 614.02,
    'price_from': 1610.69,
    'qty': 260
},
{
    'description': 'Bettongia penicillata',
    'sku': 88389753,
    'ean': 3441831719,
    'price_to': 997.15,
    'price_from': 1006.03,
    'qty': 272
},
{
    'description': 'Hippotragus niger',
    'sku': 93949630,
    'ean': 1642752366,
    'price_to': 782.53,
    'price_from': 1564.77,
    'qty': 491
},
{
    'description': 'Globicephala melas',
    'sku': 13760432,
    'ean': 7387851266,
    'price_to': 389.35,
    'price_from': 633.84,
    'qty': 390
},
{
    'description': 'Mazama gouazoubira',
    'sku': 25238710,
    'ean': 3134313082,
    'price_to': 40.1,
    'price_from': 104.39,
    'qty': 386
},
{
    'description': 'Streptopelia senegalensis',
    'sku': 74486314,
    'ean': 8711956135,
    'price_to': 473.0,
    'price_from': 1020.88,
    'qty': 127
},
{
    'description': 'Lutra canadensis',
    'sku': 16476125,
    'ean': 6263016928,
    'price_to': 523.5,
    'price_from': 941.75,
    'qty': 94
},
{
    'description': 'Papilio canadensis',
    'sku': 17544475,
    'ean': 8102321478,
    'price_to': 470.28,
    'price_from': 1278.13,
    'qty': 456
},
{
    'description': 'Damaliscus dorcas',
    'sku': 14475732,
    'ean': 9063539349,
    'price_to': 721.72,
    'price_from': 1408.13,
    'qty': 30
},
{
    'description': 'Heloderma horridum',
    'sku': 17454927,
    'ean': 3750245945,
    'price_to': 875.09,
    'price_from': 1318.87,
    'qty': 475
},
{
    'description': 'Macropus agilis',
    'sku': 34449010,
    'ean': 2243497909,
    'price_to': 672.52,
    'price_from': 1274.37,
    'qty': 457
},
{
    'description': 'Cervus canadensis',
    'sku': 64764930,
    'ean': 4400527244,
    'price_to': 890.83,
    'price_from': 1301.03,
    'qty': 460
},
{
    'description': 'Neotis denhami',
    'sku': 88394429,
    'ean': 7349897887,
    'price_to': 630.48,
    'price_from': 1312.63,
    'qty': 285
},
{
    'description': 'Buteo jamaicensis',
    'sku': 24470889,
    'ean': 3655709862,
    'price_to': 504.51,
    'price_from': 605.69,
    'qty': 220
},
{
    'description': 'Calyptorhynchus magnificus',
    'sku': 32336648,
    'ean': 1209787188,
    'price_to': 528.99,
    'price_from': 1377.37,
    'qty': 331
},
{
    'description': 'Acanthaster planci',
    'sku': 48299161,
    'ean': 6412587248,
    'price_to': 439.19,
    'price_from': 506.5,
    'qty': 420
},
{
    'description': 'Dendrocygna viduata',
    'sku': 63839333,
    'ean': 7715560017,
    'price_to': 714.47,
    'price_from': 1053.18,
    'qty': 53
},
{
    'description': 'Uraeginthus angolensis',
    'sku': 56599108,
    'ean': 2208603657,
    'price_to': 199.01,
    'price_from': 622.33,
    'qty': 329
},
{
    'description': 'Vulpes vulpes',
    'sku': 94809627,
    'ean': 7000772660,
    'price_to': 761.85,
    'price_from': 1366.28,
    'qty': 442
},
{
    'description': 'Spermophilus richardsonii',
    'sku': 68915176,
    'ean': 7899286880,
    'price_to': 90.57,
    'price_from': 730.85,
    'qty': 178
},
{
    'description': 'Equus burchelli',
    'sku': 38022469,
    'ean': 2993905520,
    'price_to': 425.78,
    'price_from': 807.86,
    'qty': 162
},
{
    'description': 'Morelia spilotes variegata',
    'sku': 76544503,
    'ean': 3096307133,
    'price_to': 98.99,
    'price_from': 117.49,
    'qty': 158
},
{
    'description': 'Tadorna tadorna',
    'sku': 87946099,
    'ean': 6727229351,
    'price_to': 665.06,
    'price_from': 1443.83,
    'qty': 326
},
{
    'description': 'Echimys chrysurus',
    'sku': 94098584,
    'ean': 4176551693,
    'price_to': 635.62,
    'price_from': 843.78,
    'qty': 336
},
{
    'description': 'Manouria emys',
    'sku': 51670909,
    'ean': 1929208522,
    'price_to': 192.28,
    'price_from': 714.95,
    'qty': 258
},
{
    'description': 'Mirounga angustirostris',
    'sku': 20899262,
    'ean': 5287881335,
    'price_to': 144.62,
    'price_from': 997.66,
    'qty': 301
},
{
    'description': 'Spermophilus tridecemlineatus',
    'sku': 97157005,
    'ean': 6686680697,
    'price_to': 682.23,
    'price_from': 1608.82,
    'qty': 143
},
{
    'description': 'Trachyphonus vaillantii',
    'sku': 75190106,
    'ean': 9709606785,
    'price_to': 848.07,
    'price_from': 1371.55,
    'qty': 268
},
{
    'description': 'Bubalus arnee',
    'sku': 69623967,
    'ean': 1192818136,
    'price_to': 310.31,
    'price_from': 532.42,
    'qty': 227
},
{
    'description': 'Irania gutteralis',
    'sku': 79821608,
    'ean': 9061200793,
    'price_to': 832.23,
    'price_from': 1376.54,
    'qty': 1
},
{
    'description': 'Rhabdomys pumilio',
    'sku': 86191830,
    'ean': 9253397817,
    'price_to': 739.62,
    'price_from': 1583.74,
    'qty': 198
},
{
    'description': 'Aegypius tracheliotus',
    'sku': 61926831,
    'ean': 7943465038,
    'price_to': 852.86,
    'price_from': 1742.8,
    'qty': 497
},
{
    'description': 'Oreamnos americanus',
    'sku': 58583026,
    'ean': 6253147577,
    'price_to': 606.06,
    'price_from': 796.54,
    'qty': 129
},
{
    'description': 'Fulica cristata',
    'sku': 88507703,
    'ean': 5406025993,
    'price_to': 345.98,
    'price_from': 852.55,
    'qty': 333
},
{
    'description': 'Vulpes vulpes',
    'sku': 77184565,
    'ean': 2940206897,
    'price_to': 990.73,
    'price_from': 1572.99,
    'qty': 251
},
{
    'description': 'Macropus fuliginosus',
    'sku': 24795118,
    'ean': 5065475152,
    'price_to': 311.32,
    'price_from': 572.09,
    'qty': 392
},
{
    'description': 'Nasua narica',
    'sku': 42320030,
    'ean': 6398355721,
    'price_to': 171.62,
    'price_from': 961.85,
    'qty': 50
},
{
    'description': 'Coluber constrictor',
    'sku': 15894577,
    'ean': 5302949991,
    'price_to': 847.53,
    'price_from': 1068.62,
    'qty': 121
},
{
    'description': 'Francolinus coqui',
    'sku': 56209186,
    'ean': 9164469854,
    'price_to': 303.91,
    'price_from': 354.43,
    'qty': 202
},
{
    'description': 'Panthera pardus',
    'sku': 37937331,
    'ean': 1680865849,
    'price_to': 273.42,
    'price_from': 863.22,
    'qty': 15
},
{
    'description': 'Hippopotamus amphibius',
    'sku': 49207802,
    'ean': 1844529300,
    'price_to': 360.78,
    'price_from': 1171.61,
    'qty': 211
},
{
    'description': 'Vombatus ursinus',
    'sku': 31123710,
    'ean': 3881177364,
    'price_to': 641.9,
    'price_from': 1491.37,
    'qty': 460
},
{
    'description': 'Cyrtodactylus louisiadensis',
    'sku': 50605670,
    'ean': 9908068659,
    'price_to': 720.44,
    'price_from': 947.84,
    'qty': 139
},
{
    'description': 'Tetracerus quadricornis',
    'sku': 30354994,
    'ean': 8629496832,
    'price_to': 676.0,
    'price_from': 893.47,
    'qty': 418
},
{
    'description': 'Phoca vitulina',
    'sku': 44055653,
    'ean': 8144287726,
    'price_to': 236.44,
    'price_from': 920.13,
    'qty': 24
},
{
    'description': 'Phalaropus fulicarius',
    'sku': 49235353,
    'ean': 9589407172,
    'price_to': 365.51,
    'price_from': 1085.37,
    'qty': 208
},
{
    'description': 'Dicrostonyx groenlandicus',
    'sku': 85568167,
    'ean': 3734529935,
    'price_to': 939.72,
    'price_from': 1345.92,
    'qty': 194
},
{
    'description': 'Anthropoides paradisea',
    'sku': 99579614,
    'ean': 1786624472,
    'price_to': 663.37,
    'price_from': 1137.13,
    'qty': 16
},
{
    'description': 'unavailable',
    'sku': 64338526,
    'ean': 9023560164,
    'price_to': 522.1,
    'price_from': 1075.5,
    'qty': 289
},
{
    'description': 'Hippotragus niger',
    'sku': 82299253,
    'ean': 2159707456,
    'price_to': 930.9,
    'price_from': 1740.45,
    'qty': 438
},
{
    'description': 'Suricata suricatta',
    'sku': 40500706,
    'ean': 9093624502,
    'price_to': 454.9,
    'price_from': 615.26,
    'qty': 311
},
{
    'description': 'Alcelaphus buselaphus cokii',
    'sku': 35107419,
    'ean': 2174052595,
    'price_to': 31.09,
    'price_from': 851.95,
    'qty': 73
},
{
    'description': 'Erethizon dorsatum',
    'sku': 55449163,
    'ean': 3964715526,
    'price_to': 599.37,
    'price_from': 819.14,
    'qty': 372
},
{
    'description': 'Naja nivea',
    'sku': 59070651,
    'ean': 3656916150,
    'price_to': 552.43,
    'price_from': 749.96,
    'qty': 458
},
{
    'description': 'Gopherus agassizii',
    'sku': 58829846,
    'ean': 2709114095,
    'price_to': 659.27,
    'price_from': 961.13,
    'qty': 445
},
{
    'description': 'Microcebus murinus',
    'sku': 88940127,
    'ean': 2866103254,
    'price_to': 167.5,
    'price_from': 1116.54,
    'qty': 323
},
{
    'description': 'Corythornis cristata',
    'sku': 44834675,
    'ean': 7355902620,
    'price_to': 289.42,
    'price_from': 301.83,
    'qty': 492
},
{
    'description': 'Dasypus novemcinctus',
    'sku': 17699776,
    'ean': 7165333048,
    'price_to': 574.22,
    'price_from': 908.5,
    'qty': 251
},
{
    'description': 'Ephipplorhynchus senegalensis',
    'sku': 63988012,
    'ean': 9489060192,
    'price_to': 695.69,
    'price_from': 1107.37,
    'qty': 409
},
{
    'description': 'Crotaphytus collaris',
    'sku': 76470283,
    'ean': 8999583876,
    'price_to': 580.9,
    'price_from': 1512.99,
    'qty': 394
},
{
    'description': 'Spizaetus coronatus',
    'sku': 44883381,
    'ean': 6141331789,
    'price_to': 479.15,
    'price_from': 732.75,
    'qty': 433
},
{
    'description': 'Laniarius ferrugineus',
    'sku': 29755463,
    'ean': 6256555680,
    'price_to': 447.62,
    'price_from': 1131.39,
    'qty': 398
},
{
    'description': 'Panthera tigris',
    'sku': 33384796,
    'ean': 6258783312,
    'price_to': 997.19,
    'price_from': 1782.81,
    'qty': 290
},
{
    'description': 'Diomedea irrorata',
    'sku': 43666562,
    'ean': 7103909295,
    'price_to': 208.95,
    'price_from': 434.3,
    'qty': 35
},
{
    'description': 'Felis wiedi or Leopardus weidi',
    'sku': 12514231,
    'ean': 3808256468,
    'price_to': 644.5,
    'price_from': 1367.39,
    'qty': 422
},
{
    'description': 'Colobus guerza',
    'sku': 66352177,
    'ean': 8703183756,
    'price_to': 295.39,
    'price_from': 1253.0,
    'qty': 97
},
{
    'description': 'Marmota monax',
    'sku': 58247187,
    'ean': 2505513988,
    'price_to': 454.15,
    'price_from': 1178.41,
    'qty': 83
},
{
    'description': 'Microcavia australis',
    'sku': 32942056,
    'ean': 4319216762,
    'price_to': 605.75,
    'price_from': 859.96,
    'qty': 460
},
{
    'description': 'Tetracerus quadricornis',
    'sku': 17905460,
    'ean': 3536570771,
    'price_to': 774.61,
    'price_from': 1081.11,
    'qty': 197
},
{
    'description': 'Iguana iguana',
    'sku': 74104051,
    'ean': 6833685758,
    'price_to': 700.41,
    'price_from': 1113.51,
    'qty': 85
},
{
    'description': 'Nucifraga columbiana',
    'sku': 51902001,
    'ean': 3863135163,
    'price_to': 278.11,
    'price_from': 347.57,
    'qty': 453
},
{
    'description': 'Ovis ammon',
    'sku': 22295371,
    'ean': 2844922957,
    'price_to': 285.25,
    'price_from': 909.2,
    'qty': 94
},
{
    'description': 'Ara ararauna',
    'sku': 14332028,
    'ean': 9116129993,
    'price_to': 58.96,
    'price_from': 118.42,
    'qty': 167
},
{
    'description': 'Lamprotornis superbus',
    'sku': 46827716,
    'ean': 3297549357,
    'price_to': 150.58,
    'price_from': 460.88,
    'qty': 187
},
{
    'description': 'Lama guanicoe',
    'sku': 87399409,
    'ean': 6380729402,
    'price_to': 416.53,
    'price_from': 679.28,
    'qty': 242
},
{
    'description': 'Pycnonotus nigricans',
    'sku': 48444801,
    'ean': 2044655597,
    'price_to': 677.41,
    'price_from': 853.77,
    'qty': 270
},
{
    'description': 'Macropus fuliginosus',
    'sku': 41779558,
    'ean': 6524175150,
    'price_to': 626.25,
    'price_from': 1597.88,
    'qty': 49
},
{
    'description': 'Agkistrodon piscivorus',
    'sku': 40449435,
    'ean': 3451875733,
    'price_to': 764.66,
    'price_from': 875.23,
    'qty': 287
},
{
    'description': 'Ictonyx striatus',
    'sku': 31898871,
    'ean': 1463996267,
    'price_to': 936.28,
    'price_from': 1236.54,
    'qty': 356
},
{
    'description': 'Priodontes maximus',
    'sku': 19008189,
    'ean': 9228761936,
    'price_to': 290.88,
    'price_from': 490.11,
    'qty': 154
},
{
    'description': 'Procyon lotor',
    'sku': 41579246,
    'ean': 4761842842,
    'price_to': 15.88,
    'price_from': 995.81,
    'qty': 118
},
{
    'description': 'Crotalus cerastes',
    'sku': 74651595,
    'ean': 3172088959,
    'price_to': 165.5,
    'price_from': 396.27,
    'qty': 309
},
{
    'description': 'Colobus guerza',
    'sku': 25772786,
    'ean': 5829539505,
    'price_to': 57.52,
    'price_from': 593.8,
    'qty': 259
},
{
    'description': 'Phalacrocorax carbo',
    'sku': 63610031,
    'ean': 8744859592,
    'price_to': 604.43,
    'price_from': 887.03,
    'qty': 152
},
{
    'description': 'Mazama americana',
    'sku': 76374345,
    'ean': 7683889211,
    'price_to': 807.23,
    'price_from': 1604.82,
    'qty': 451
},
{
    'description': 'Odocoilenaus virginianus',
    'sku': 87145837,
    'ean': 1251709565,
    'price_to': 541.74,
    'price_from': 831.54,
    'qty': 231
},
{
    'description': 'Aonyx cinerea',
    'sku': 35589206,
    'ean': 4016574566,
    'price_to': 722.14,
    'price_from': 1536.94,
    'qty': 217
},
{
    'description': 'Capra ibex',
    'sku': 67930676,
    'ean': 2160527633,
    'price_to': 238.69,
    'price_from': 1063.26,
    'qty': 373
},
{
    'description': 'Aegypius tracheliotus',
    'sku': 47389601,
    'ean': 5290597549,
    'price_to': 30.01,
    'price_from': 653.05,
    'qty': 258
},
{
    'description': 'Sylvicapra grimma',
    'sku': 94738306,
    'ean': 5232700442,
    'price_to': 563.0,
    'price_from': 1286.32,
    'qty': 297
},
{
    'description': 'Parus atricapillus',
    'sku': 74621956,
    'ean': 4379838389,
    'price_to': 759.36,
    'price_from': 1462.5,
    'qty': 344
},
{
    'description': 'Varanus salvator',
    'sku': 32563172,
    'ean': 6602542884,
    'price_to': 517.63,
    'price_from': 735.05,
    'qty': 310
},
{
    'description': 'Meleagris gallopavo',
    'sku': 36212614,
    'ean': 9011861426,
    'price_to': 70.16,
    'price_from': 555.91,
    'qty': 477
},
{
    'description': 'Haliaetus vocifer',
    'sku': 52509036,
    'ean': 8729075541,
    'price_to': 606.8,
    'price_from': 1001.89,
    'qty': 380
},
{
    'description': 'Cervus duvauceli',
    'sku': 36121470,
    'ean': 4161983621,
    'price_to': 799.17,
    'price_from': 1029.88,
    'qty': 456
},
{
    'description': 'Phalacrocorax varius',
    'sku': 85437610,
    'ean': 7985495480,
    'price_to': 895.34,
    'price_from': 1888.05,
    'qty': 266
},
{
    'description': 'Aegypius tracheliotus',
    'sku': 53521131,
    'ean': 5079958426,
    'price_to': 235.6,
    'price_from': 843.28,
    'qty': 50
},
{
    'description': 'Paroaria gularis',
    'sku': 11376594,
    'ean': 7515828428,
    'price_to': 132.13,
    'price_from': 786.65,
    'qty': 429
},
{
    'description': 'Casmerodius albus',
    'sku': 97819790,
    'ean': 6190897086,
    'price_to': 549.76,
    'price_from': 1302.66,
    'qty': 164
},
{
    'description': 'Dasyurus viverrinus',
    'sku': 88225210,
    'ean': 2363249188,
    'price_to': 478.02,
    'price_from': 726.85,
    'qty': 14
},
{
    'description': 'Haliaetus vocifer',
    'sku': 70071632,
    'ean': 4160628877,
    'price_to': 915.26,
    'price_from': 1671.53,
    'qty': 80
},
{
    'description': 'Anthropoides paradisea',
    'sku': 58607240,
    'ean': 1278845608,
    'price_to': 378.02,
    'price_from': 1044.21,
    'qty': 339
},
{
    'description': 'Francolinus coqui',
    'sku': 30893271,
    'ean': 9946722530,
    'price_to': 56.81,
    'price_from': 1018.49,
    'qty': 497
},
{
    'description': 'Macropus agilis',
    'sku': 16698122,
    'ean': 6842173752,
    'price_to': 491.0,
    'price_from': 1422.49,
    'qty': 119
},
{
    'description': 'Snycerus caffer',
    'sku': 15150484,
    'ean': 3746033622,
    'price_to': 246.33,
    'price_from': 817.5,
    'qty': 225
},
{
    'description': 'Chauna torquata',
    'sku': 21509839,
    'ean': 3141152632,
    'price_to': 389.85,
    'price_from': 898.51,
    'qty': 270
},
{
    'description': 'unavailable',
    'sku': 59250833,
    'ean': 5242219854,
    'price_to': 674.9,
    'price_from': 799.7,
    'qty': 111
},
{
    'description': 'unavailable',
    'sku': 60698844,
    'ean': 7503239512,
    'price_to': 969.46,
    'price_from': 1083.79,
    'qty': 289
},
{
    'description': 'Felis concolor',
    'sku': 32939160,
    'ean': 4952782906,
    'price_to': 205.19,
    'price_from': 379.62,
    'qty': 101
},
{
    'description': 'Macaca radiata',
    'sku': 78189771,
    'ean': 3177161786,
    'price_to': 109.29,
    'price_from': 666.35,
    'qty': 252
},
{
    'description': 'Anastomus oscitans',
    'sku': 89148352,
    'ean': 9648356478,
    'price_to': 313.03,
    'price_from': 601.89,
    'qty': 465
},
{
    'description': 'Cereopsis novaehollandiae',
    'sku': 86006679,
    'ean': 4475953276,
    'price_to': 229.04,
    'price_from': 987.26,
    'qty': 13
},
{
    'description': 'Castor canadensis',
    'sku': 22034829,
    'ean': 6197200975,
    'price_to': 749.31,
    'price_from': 1655.89,
    'qty': 283
},
{
    'description': 'Haliaetus vocifer',
    'sku': 42456380,
    'ean': 5157604268,
    'price_to': 945.67,
    'price_from': 1628.89,
    'qty': 461
},
{
    'description': 'Amphibolurus barbatus',
    'sku': 66882429,
    'ean': 2152884027,
    'price_to': 753.33,
    'price_from': 1418.46,
    'qty': 103
},
{
    'description': 'Pteronura brasiliensis',
    'sku': 62967512,
    'ean': 3710437563,
    'price_to': 106.87,
    'price_from': 664.83,
    'qty': 494
},
{
    'description': 'Melophus lathami',
    'sku': 69142381,
    'ean': 9207522748,
    'price_to': 110.61,
    'price_from': 673.05,
    'qty': 231
},
{
    'description': 'Orcinus orca',
    'sku': 21519062,
    'ean': 3065207330,
    'price_to': 964.15,
    'price_from': 1190.14,
    'qty': 133
},
{
    'description': 'Bison bison',
    'sku': 73899481,
    'ean': 8359094370,
    'price_to': 21.06,
    'price_from': 142.52,
    'qty': 226
},
{
    'description': 'Aonyx capensis',
    'sku': 62302191,
    'ean': 7442272362,
    'price_to': 15.06,
    'price_from': 813.11,
    'qty': 132
},
{
    'description': 'Phalacrocorax carbo',
    'sku': 50540253,
    'ean': 1161778929,
    'price_to': 852.4,
    'price_from': 1130.36,
    'qty': 358
},
{
    'description': 'Pteropus rufus',
    'sku': 88956047,
    'ean': 9559780279,
    'price_to': 667.43,
    'price_from': 726.88,
    'qty': 61
},
{
    'description': 'Lepus townsendii',
    'sku': 39761572,
    'ean': 3224990553,
    'price_to': 366.16,
    'price_from': 543.43,
    'qty': 161
},
{
    'description': 'Castor canadensis',
    'sku': 57624816,
    'ean': 2933835715,
    'price_to': 587.38,
    'price_from': 641.44,
    'qty': 52
},
{
    'description': 'unavailable',
    'sku': 57502601,
    'ean': 7939995345,
    'price_to': 549.85,
    'price_from': 839.56,
    'qty': 204
},
{
    'description': 'Ninox superciliaris',
    'sku': 45807760,
    'ean': 9364250880,
    'price_to': 245.14,
    'price_from': 675.3,
    'qty': 12
},
{
    'description': 'Macaca nemestrina',
    'sku': 36551973,
    'ean': 4709657731,
    'price_to': 891.88,
    'price_from': 1364.02,
    'qty': 16
},
{
    'description': 'Trichosurus vulpecula',
    'sku': 32013599,
    'ean': 1590734356,
    'price_to': 916.06,
    'price_from': 1552.34,
    'qty': 11
},
{
    'description': 'Gyps bengalensis',
    'sku': 26209903,
    'ean': 8018547490,
    'price_to': 592.89,
    'price_from': 1327.75,
    'qty': 428
},
{
    'description': 'Creagrus furcatus',
    'sku': 77669848,
    'ean': 8080975815,
    'price_to': 472.61,
    'price_from': 718.32,
    'qty': 68
},
{
    'description': 'Bassariscus astutus',
    'sku': 56393824,
    'ean': 1560946414,
    'price_to': 843.37,
    'price_from': 1252.98,
    'qty': 40
},
{
    'description': 'Geochelone elegans',
    'sku': 98014058,
    'ean': 3288919521,
    'price_to': 783.08,
    'price_from': 1474.51,
    'qty': 299
},
{
    'description': 'Laniaurius atrococcineus',
    'sku': 79738627,
    'ean': 8455860546,
    'price_to': 31.27,
    'price_from': 648.94,
    'qty': 419
},
{
    'description': 'Gorilla gorilla',
    'sku': 63199816,
    'ean': 9506832040,
    'price_to': 512.41,
    'price_from': 935.46,
    'qty': 12
},
{
    'description': 'Bubo virginianus',
    'sku': 57734001,
    'ean': 4728531377,
    'price_to': 942.95,
    'price_from': 1598.63,
    'qty': 186
},
{
    'description': 'Varanus salvator',
    'sku': 88889842,
    'ean': 2959690554,
    'price_to': 446.74,
    'price_from': 1366.84,
    'qty': 409
},
{
    'description': 'Pteropus rufus',
    'sku': 92152666,
    'ean': 6946680159,
    'price_to': 60.1,
    'price_from': 955.8,
    'qty': 1
},
{
    'description': 'Genetta genetta',
    'sku': 55306705,
    'ean': 9880503913,
    'price_to': 99.74,
    'price_from': 802.71,
    'qty': 456
},
{
    'description': 'Haliaetus vocifer',
    'sku': 57471929,
    'ean': 5394407987,
    'price_to': 123.75,
    'price_from': 857.46,
    'qty': 335
},
{
    'description': 'Ursus americanus',
    'sku': 16121283,
    'ean': 2368688029,
    'price_to': 226.38,
    'price_from': 568.15,
    'qty': 70
},
{
    'description': 'Bos taurus',
    'sku': 45976031,
    'ean': 9835884817,
    'price_to': 271.86,
    'price_from': 286.18,
    'qty': 230
},
{
    'description': 'Lamprotornis nitens',
    'sku': 55079063,
    'ean': 5054469836,
    'price_to': 113.14,
    'price_from': 866.65,
    'qty': 273
},
{
    'description': 'Choloepus hoffmani',
    'sku': 24216637,
    'ean': 1604618210,
    'price_to': 557.96,
    'price_from': 1412.5,
    'qty': 310
},
{
    'description': 'Francolinus leucoscepus',
    'sku': 50456818,
    'ean': 8857474173,
    'price_to': 303.47,
    'price_from': 335.73,
    'qty': 368
},
{
    'description': 'Felis concolor',
    'sku': 64176179,
    'ean': 5660527380,
    'price_to': 100.86,
    'price_from': 392.48,
    'qty': 327
},
{
    'description': 'Lorythaixoides concolor',
    'sku': 47132384,
    'ean': 7603112867,
    'price_to': 293.27,
    'price_from': 1257.88,
    'qty': 46
},
{
    'description': 'Felis silvestris lybica',
    'sku': 64457164,
    'ean': 8861306475,
    'price_to': 889.52,
    'price_from': 1569.97,
    'qty': 385
},
{
    'description': 'Scolopax minor',
    'sku': 72281830,
    'ean': 2415241042,
    'price_to': 126.82,
    'price_from': 130.91,
    'qty': 216
},
{
    'description': 'Castor fiber',
    'sku': 33616653,
    'ean': 9780727966,
    'price_to': 327.24,
    'price_from': 336.93,
    'qty': 321
},
{
    'description': 'Kobus leche robertsi',
    'sku': 22784272,
    'ean': 7126517404,
    'price_to': 890.81,
    'price_from': 1297.14,
    'qty': 109
},
{
    'description': 'Anser caerulescens',
    'sku': 76724045,
    'ean': 4820004790,
    'price_to': 530.52,
    'price_from': 904.51,
    'qty': 331
},
{
    'description': 'Macaca mulatta',
    'sku': 26602557,
    'ean': 9946943029,
    'price_to': 461.81,
    'price_from': 800.21,
    'qty': 319
},
{
    'description': 'Corvus brachyrhynchos',
    'sku': 82709463,
    'ean': 8501731252,
    'price_to': 61.23,
    'price_from': 546.53,
    'qty': 364
},
{
    'description': 'Felis concolor',
    'sku': 35641551,
    'ean': 7760446317,
    'price_to': 564.07,
    'price_from': 669.58,
    'qty': 308
},
{
    'description': 'Branta canadensis',
    'sku': 89013316,
    'ean': 6817043125,
    'price_to': 599.73,
    'price_from': 634.1,
    'qty': 342
},
{
    'description': 'Zalophus californicus',
    'sku': 75396140,
    'ean': 4951187900,
    'price_to': 643.73,
    'price_from': 1594.65,
    'qty': 114
},
{
    'description': 'Mabuya spilogaster',
    'sku': 68080452,
    'ean': 3846955918,
    'price_to': 762.42,
    'price_from': 1344.03,
    'qty': 124
},
{
    'description': 'Canis mesomelas',
    'sku': 52926513,
    'ean': 3608062442,
    'price_to': 461.62,
    'price_from': 1091.57,
    'qty': 387
},
{
    'description': 'Haliaeetus leucocephalus',
    'sku': 80303056,
    'ean': 7275500119,
    'price_to': 232.57,
    'price_from': 792.02,
    'qty': 181
},
{
    'description': 'Tockus erythrorhyncus',
    'sku': 31328956,
    'ean': 1630981137,
    'price_to': 504.31,
    'price_from': 1450.75,
    'qty': 467
},
{
    'description': 'Hystrix cristata',
    'sku': 43378938,
    'ean': 7793933394,
    'price_to': 967.24,
    'price_from': 1505.33,
    'qty': 257
},
{
    'description': 'Vombatus ursinus',
    'sku': 29685247,
    'ean': 7513057280,
    'price_to': 756.78,
    'price_from': 1747.8,
    'qty': 347
},
{
    'description': 'Isoodon obesulus',
    'sku': 91710864,
    'ean': 4253683392,
    'price_to': 19.5,
    'price_from': 263.58,
    'qty': 108
},
{
    'description': 'Phascogale calura',
    'sku': 54894123,
    'ean': 3696370449,
    'price_to': 968.25,
    'price_from': 1090.0,
    'qty': 301
},
{
    'description': 'Anastomus oscitans',
    'sku': 90400186,
    'ean': 3866480901,
    'price_to': 560.88,
    'price_from': 1064.68,
    'qty': 300
},
{
    'description': 'Zosterops pallidus',
    'sku': 37999017,
    'ean': 5811425471,
    'price_to': 143.5,
    'price_from': 488.66,
    'qty': 21
},
{
    'description': 'Oxybelis fulgidus',
    'sku': 25590388,
    'ean': 9975273096,
    'price_to': 598.71,
    'price_from': 1477.85,
    'qty': 185
},
{
    'description': 'Hippopotamus amphibius',
    'sku': 15658976,
    'ean': 6124794298,
    'price_to': 705.26,
    'price_from': 877.19,
    'qty': 219
},
{
    'description': 'Odocoilenaus virginianus',
    'sku': 67135737,
    'ean': 4586288920,
    'price_to': 599.28,
    'price_from': 1421.16,
    'qty': 445
},
{
    'description': 'Lasiorhinus latifrons',
    'sku': 27879405,
    'ean': 4290061250,
    'price_to': 819.34,
    'price_from': 1128.88,
    'qty': 438
},
{
    'description': 'Dromaeus novaehollandiae',
    'sku': 43866789,
    'ean': 2797388777,
    'price_to': 328.45,
    'price_from': 1275.74,
    'qty': 278
},
{
    'description': 'Dicrurus adsimilis',
    'sku': 31807471,
    'ean': 2666648134,
    'price_to': 88.11,
    'price_from': 477.86,
    'qty': 71
},
{
    'description': 'Turtur chalcospilos',
    'sku': 65642966,
    'ean': 3828077936,
    'price_to': 426.9,
    'price_from': 711.71,
    'qty': 360
},
{
    'description': 'Eremophila alpestris',
    'sku': 30356641,
    'ean': 5770636222,
    'price_to': 248.11,
    'price_from': 517.33,
    'qty': 416
},
{
    'description': 'Varanus salvator',
    'sku': 78116502,
    'ean': 6659246114,
    'price_to': 674.74,
    'price_from': 1132.15,
    'qty': 323
},
{
    'description': 'Colobus guerza',
    'sku': 51528709,
    'ean': 9721033421,
    'price_to': 280.15,
    'price_from': 679.12,
    'qty': 259
}
];

export default data;